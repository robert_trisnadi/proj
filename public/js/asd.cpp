#include <bits/stdc++.h>

using namespace std;

struct point
{
    point(int a,int b):x(a),y(b){}
    int x;
    int y;
};

int twoPluses(vector <string> grid) {
    // Complete this function    
    
    int maxval = 0;
    int allowedmax = 5;
    int w1 = grid[0].size();
    int w2 = grid.size();
    int maxoffset = w1*w2;    
    
    for(int k=1;k<w2;k++)
    {
        for(int l=1;l<w1;l++)
        {
            vector<string> cgrid = grid;
            vector<int> crosses;
            allowedmax = 5;
            while(allowedmax < maxoffset)
            {
                if(cgrid[k][l] == 'G')//loop if 'G'
                {
                    int size = 0;
                    while(true)
                    {
                        vector<point> vp;
                        if((k-1)>=0 && cgrid[k-1][l] != 'B' && cgrid[k-1][l]!='1')
                        {
                            if((k-2) >= 0 && cgrid[k-2][l]=='1')
                                break;
                            else                            
                                vp.push_back(point(k-1,l));
                        }                            
                        else break;
                        if((k+1)<w2 && cgrid[k+1][l] != 'B' && cgrid[k+1][l]!='1')
                        {
                            if((k+2) < w2 && cgrid[k+2][l]=='1')
                                break;  
                            else
                                vp.push_back(point(k+1,l));
                        }                            
                        else break;
                        if((l-1)>=0 && cgrid[k][l-1] != 'B' && cgrid[k][l-1]!='1')
                        {
                            if((l-2) >= 0 && cgrid[k][l-2]=='1')
                                break;
                            else
                                vp.push_back(point(k,l-1));
                        }                            
                        else break;
                        if((l+1)<w1 && cgrid[k][l+1] != 'B' && cgrid[k][l+1]!='1')
                        {
                            if((l+2) < w1 && cgrid[k][l+2]=='1')
                                break;
                            else
                                vp.push_back(point(k,l+1));
                        }
                        else break;
                        if(size==0)
                        {
                            size = 1;
                            cgrid[k][l] = '1';
                        }
                        for(auto a : vp)
                            cgrid[a.x][a.y] = '1';
                        size+=4;
                        if(size == allowedmax)                                                    
                            break;
                    }
                    if(size != 0)
                        crosses.push_back(size);
                }
            
                for(int i=1;i<w2;i++)
                {
                    for(int j=1;j<w1;j++)
                    {
                        if(cgrid[i][j] == 'G')//loop if 'G'
                        {
                            int size = 0;
                            while(true)
                            {
                                vector<point> vp;
                                if((i-1)>=0 && cgrid[i-1][j] != 'B' && cgrid[i-1][j]!='1')
                                {
                                    if((i-2) >= 0 && cgrid[i-2][j]=='1')
                                        break;
                                    else                            
                                        vp.push_back(point(i-1,j));
                                }                            
                                else break;
                                if((i+1)<w2 && cgrid[i+1][j] != 'B' && cgrid[i+1][j]!='1')
                                {
                                    if((i+2) < w2 && cgrid[i+2][j]=='1')                        
                                        break;  
                                    else
                                        vp.push_back(point(i+1,j));
                                }                            
                                else break;
                                if((j-1)>=0 && cgrid[i][j-1] != 'B' && cgrid[i][j-1]!='1')
                                {
                                    if((j-2) >= 0 && cgrid[i][j-2]=='1')
                                        break;
                                    else
                                        vp.push_back(point(i,j-1));                                
                                }                            
                                else break;
                                if((j+1)<w1 && cgrid[i][j+1] != 'B' && cgrid[i][j+1]!='1')
                                {
                                    if((j+2) < w1 && cgrid[i][j+2]=='1')
                                        break;
                                    else
                                        vp.push_back(point(i,j+1));                                
                                }
                                else break;
                                if(size==0)
                                {
                                    size = 1;
                                    cgrid[i][j] = '1';
                                }
                                for(auto a : vp)
                                    cgrid[a.x][a.y] = '1';
                                size+=4;
                                if(size == allowedmax)                                                    
                                    break;
                            }
                            if(size != 0)
                                crosses.push_back(size);
                        }
                    }
                }
                if(crosses.size()!=0)
                {
                    int total = 1;
                    for(auto a : crosses)                
                        total*=a;
                    if(maxval < total)
                        maxval = total;
                }
                allowedmax+=4;
            }
        }
    }
    return maxval;
}

int main() {
    int n;
    int m;
    cin >> n >> m;
    vector<string> grid(n);
    for(int grid_i = 0; grid_i < n; grid_i++){
       cin >> grid[grid_i];
    }
    int result = twoPluses(grid);
    cout << result << endl;
    return 0;
}
