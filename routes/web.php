<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
ini_set('display_errors', 1);
Route::get('/', function () {
    return view('home');
});

Route::view('/faq', 'faq');

Route::get('/panduan', function () {
    return view('panduan');
});

Route::get('/memoOperation/{get_param}',
	function ($get_param){
		try {
			header('Content-type: application/json');
		    if($get_param == 0)
		    	Request::session()->get('id')->isMinimized = 1;
		    else if($get_param == 1)
		    	Request::session()->get('id')->isClosed = 1;
		} catch (Exception $e) {
			
		}
});

Route::get('/checkusername/{get_param}',
	function ($get_param){
		try {
			header('Content-type: application/json');
		    $data = DB::table('LoginInformation')->where('Username',$get_param)->first();
		    $obj = (object) [
			    'string' => 'no'		    
			];
		    if($data != null){
		    	$obj->string = 'yes';
		    	return json_encode($obj);
		    }
		    else{
		    	$obj->string = 'no';
		    	return json_encode($obj);
		    }	
		} catch (Exception $e) {
			
		}		
});

Route::get('/moredatanews/{lastid}/{categoryid}/{subcategoryid}',
	function ($lastid, $categoryid,$subcategoryid){
		try {
			header('Content-type: application/json');			
			if('-' == $categoryid)
			{
				$news = DB::table('ArticleCategorySubView')
					->where('Status',4)
					->where(function($q)
					{
						$q->where('StatusCategory',4)
						  ->orWhere('StatusCategory',NULL);
					})
					->where(function($q)
					{
						$q->where('StatusSub',4)
						  ->orWhere('StatusSub',NULL);
					})
					->orderBy('PublishDate', 'desc')
					->offset($lastid)
					->limit(5)
					->get();				
			}
			else
			{
				if('-' == $subcategoryid || null == $subcategoryid)
				{	

					$news = DB::table('ArticleCategorySubView')
						->orderBy('PublishDate', 'desc')
						->where('Status',4)
						->where(function($q)
						{
							$q->where('StatusCategory',4)
							  ->orWhere('StatusCategory',NULL);
						})
						->where(function($q)
						{
							$q->where('StatusSub',4)
							  ->orWhere('StatusSub',NULL);
						})
						->where('CategoryName',$categoryid)
						->offset($lastid)
						->limit(5)
						->get();				
				}
				else
				{			
					$news = DB::table('ArticleCategorySubView')
						->orderBy('PublishDate', 'desc')								
						->where('Status',4)
						->where(function($q)
						{
							$q->where('StatusCategory',4)
							  ->orWhere('StatusCategory',NULL);
						})
						->where(function($q)
						{
							$q->where('StatusSub',4)
							  ->orWhere('StatusSub',NULL);
						})
						->where('CategoryName','=',$categoryid)
						->where('SubcategoryName','=',$subcategoryid)
						->offset($lastid)
						->limit(5)
						->get();
				}
			}

			foreach ($news as $n) {
				$n->Content = strip_tags($n->Content);
			}

		    }catch(Exception $e)
		    {
		    	return json_encode($e);
		    }
		    return json_encode($news);
});

Route::get('/moredata/{lastid}/{pagename}',
	function ($lastid, $pagename){
		//try {
			header('Content-type: application/json');
			$columnName = '';
			$tablename = '';
			if($pagename == 'deposit'){
				$tablename = "depositview";
				$columnName = 'DepositlogID';		
			}
			else if($pagename == 'withdraw'){
				$tablename = "withdrawview";
				$columnName = 'WithdrawID';
			}
			else if($pagename == 'transfer'){
				$tablename = "transferlogview";
				$columnName = 'TransferlogID';
			}

		    $data = DB::table($tablename)
		    		->where('CustomerID', Request::session()->get('id')->CustomerID)	
	    			->orderBy('RequestDate','DESC')
	    			->offset($lastid-1)
					->limit(5)
	    			->get();
	    	foreach ($data as $dat) {
	    		$dat->Amount = number_format($dat->Amount,0,',','.');
	    	}
		    $data[0]->statuss = Request::session()->get('id')->statuss;
		    return json_encode($data);
});

Route::get('/moredatabonus/{lastnumber}/{pagename}',
	function ($lastnumber,$pagename){
		header('Content-type: application/json');	
		$data ='';		
		if($pagename == 'cashbackbonus'){
			$data = DB::table('CashbackBonusRequestView')
				->where('CustomerID',Request::session()->get('id')->CustomerID)				
				->where(function($q)
				{
					$q->where('BonusTypeID',3)
					  ->orWhere('BonusTypeID','-');
				})
				->orderBy('RequestDate','DESC')
				->offset($lastnumber-1)
				->limit(5)
				->get();

			foreach ($data as $dat) {
	    		$dat->Total = number_format($dat->Total,0,',','.');
	    	}
		}
		else if($pagename == 'referralbonus'){
			$data = DB::table('ReferralBonusView2')
				->where('CustomerID',
					Request::session()->get('id')->CustomerID)		
				->orderBy('RequestDate','DESC')	
				->offset($lastnumber-1)
				->limit(5)
				->get();
			foreach ($data as $dat) {
	    		$dat->Total = number_format($dat->Total,0,',','.');
	    	}
		}
		else if($pagename == 'firstdeposit'){
			$data = DB::table('FirstDepositBonusView')
			->where('CustomerID',Request::session()->get('id')->CustomerID)			
			->where(function($q)
			{
				$q->where('BonusTypeID',1)
				  ->orWhere('BonusTypeID','-');
			})
			->orderBy('RequestDate','DESC')				
			->offset($lastnumber-1)
			->limit(5)	
			->get();
			foreach ($data as $dat) {
	    		$dat->Total = number_format($dat->Total,0,',','.');
	    	}
		}
		$data[0]->statuss = Request::session()->get('id')->statuss;
	    return json_encode($data);		
});

Route::get('/peraturan', function () {
    return view('peraturan');
});

Route::get('/promo', function () {
    return view('promo');
});

Route::get('/jadwal', function () {
    return view('jadwal');
});

Route::get('/livescore', function () {
    return view('livescore');
});

Route::view('/registration', 'registration');
Route::get('/profil', function()
{
	try {
		$data = DB::table('viewcustomerproductproduct')->where('CustomerID', 
			Request::session()->get('id')->CustomerID)->get();
		return view('profil', ['data' => $data]);
	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke profile gagal!');
		return view('home');
	}	
});

Route::get('/deposit', function()
{
	try {
		$data = DB::table('depositview')
			->where('CustomerID', Request::session()->get('id')->CustomerID)
			->orderBy('RequestDate','DESC')
			->take(5)
			->get();
		return view('deposit', ['data' => $data]);
	} catch (Exception $e) {		
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke deposit gagal!');
		return view('home');
	}
});

Route::get('/downline', function()
{
	try {
		$data = DB::table('uplinedownlineview')
			->where('UplineID',Request::session()->get('id')->CustomerID)
			->join('LoginInformation','uplinedownlineview.UplineID','=','LoginInformation.CustomerID')
			->join('LoginInformation as li','uplinedownlineview.DownlineID','=','li.CustomerID')
			->select(DB::raw("li.Username as downline"))
			->get();
		return view('downlineview', ['data' => $data]);	
	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke downline gagal!');
		return view('home');
	}		
});

Route::get('/referalbonus', function()
{
	// $data = DB::table('referralbonusview')->where('Username', 
	// 	Request::session()->get('id')->Username)->where('Total','>',0)->where('Status','<>','Pending')->get();
	try {

		// $data = DB::table('uplinedownlineview')
		// 	->where('uplinedownlineview.UplineID',
		// 		Request::session()->get('id')->CustomerID)
		// 	->join('CustomerProductCashbackReferralView','CustomerProductCashbackReferralView.CustomerID','=','uplinedownlineview.DownlineID')
		// 	->where('CustomerProductCashbackReferralView.Status','<>','0')
		// 	//->join('uplinedownlineview','CustomerProductCashbackReferralView.CustomerID','=','uplinedownlineview.DownlineID')
		// 	//->where('uplinedownlineview.UplineID','CustomerProductCashbackReferralView.CustomerID')
		// 	->join('logininformation','uplinedownlineview.DownlineID','=','logininformation.CustomerID')
		// 	->select(
		// 		DB::raw('TimeInput as RequestDate'),
		// 		DB::raw("logininformation.Username as Keterangan"),
		// 		DB::raw("ReferralBonus as Jumlah"),
		// 		DB::raw('ReferralBonus as Total'),
		// 		'CustomerProductCashbackReferralView.Status')
		// 	->take(5)
		// 	->get();
		$data = DB::table('ReferralBonusView2')
			->where('CustomerID',
				Request::session()->get('id')->CustomerID)
			->orderBy('RequestDate','DESC')
			->take(5)
			->get();
		return view('referalbonus', ['data' => $data]);	
	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke referral bonus gagal!');
		return view('home');
	}	
});

Route::get('/cashbackbonus', function()
{
	try {
		$data = DB::table('CashbackBonusRequestView')
			->where('CustomerID',Request::session()->get('id')->CustomerID)
			//->where('Status','<>','0')
			->take(5)
			->where(function($q)
			{
				$q->where('BonusTypeID',3)
				  ->orWhere('BonusTypeID','-');
			})
			->orderBy('RequestDate','DESC')
			->get();

		return view('cashback', ['data' => $data]);
	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke cashback bonus gagal!');
		return view('home');
	}	
});

Route::get('/transfer', function()
{
	try {
		$data = DB::table('transferlogview')
			->where('CustomerID', Request::session()->get('id')->CustomerID)
			->orderBy('RequestDate','DESC')
			->take(5)
			->get();
		return view('transfer', ['data' => $data]);
	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke transfer gagal!');
		return view('home');		
	}	
});

Route::get('/daftargame', function()
{
	try {
		$products = DB::table('Product')->where('Status',4)->get();
		$first = DB::table('RequestGameProductView')
			->where('CustomerID',
				Request::session()->get('id')->CustomerID)
			->select('RequestDate','ProductName',DB::raw("'-' as Username"),DB::raw("'-' as Password"),'Status')
			->get();

		$data = DB::table('viewcustomerproductproduct')
            ->where('CustomerID',
				Request::session()->get('id')->CustomerID)
            ->select('CreatedDate','ProductName','Username','Password','Status')            
            ->get();

        $t = $first->merge($data);

		return view('daftargame', ['data' => $t,'products'=>$products]);

	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke daftar game gagal!');
		return view('home');
	}	
});

Route::post('/daftargame/kirim','DaftarGameController@daftargame');

Route::get('/withdraw', function()
{
	try {
		$data = DB::table('withdrawview')
			->where('CustomerID', Request::session()->get('id')->CustomerID)
			->orderBy('RequestDate','DESC')
			->take(5)
			->get();
		return view('withdraw', ['data' => $data]);		
	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke withdraw gagal!');
		return view('home');
	}
});

Route::get('/transaksi', function()
{
	try {
		$data = DB::table('transaksi')->where('CustomerID', 
			Request::session()->get('id')->CustomerID)->get();
		return view('transaksi', ['data' => $data]);	
	} catch (Exception $e) {
		echo '<script>console.log("'.$e.'")</script>';
		Request::session()->flash('errlogin','Route ke transaksi gagal!');
		return view('home');
	}	
});

Route::get('/firstdeposit', function()
{
	//try {
		$data = DB::table('FirstDepositBonusView')
			->where('CustomerID',Request::session()->get('id')->CustomerID)
			//->where('Status','<>','0')
			->where(function($q)
			{
				$q->where('BonusTypeID',1)
				  ->orWhere('BonusTypeID','-');
			})
			->orderBy('RequestDate','DESC')
			->take(5)
			->get();
			return view('firstdeposit', ['data' => $data]);
	//} catch (Exception $e) {
		//echo '<script>console.log("'.$e.'")</script>';
		//Request::session()->flash('errlogin','Route ke firstdeposit bonus gagal!');
		//return view("home");
	//}	
});

Route::get('/logout','Auth\\LoginController@deauthenticate' );
Route::view('/forgot', 'forgotpassword');
Route::view('/contact', 'contact');
Route::get('/newss/{imax}/{categoryid}/{subcategory}', [
	'uses' => function($imax,$categoryid=null,$subcategoryid=null)
	{
		if( null === $imax)
			$imax = 10;
		if( null == $categoryid)
			$categoryid = '-';
		else if(null != $categoryid && strlen($categoryid)>1)
			$categoryid = str_replace("-"," ",$categoryid);
		if( null == $subcategoryid)
			$subcategoryid = '-';
		else if(null != $subcategoryid && strlen($subcategoryid)>1)
			$subcategoryid = str_replace("-"," ",$subcategoryid);
		return view('news', ['imax' => $imax,'categoryid'=>$categoryid,'subcategoryid'=>$subcategoryid]);
	},
	'as' => 'newss'
]);

Route::get('/berita/{imax}/{categoryid}/{subcategory}', [
	'uses' => function($imax,$categoryid=null,$subcategoryid=null)
	{
		if( null === $imax)
			$imax = 10;
		if( null == $categoryid)
			$categoryid = '-';
		else if(null != $categoryid && strlen($categoryid)>1)
			$categoryid = str_replace("-"," ",$categoryid);
		if( null == $subcategoryid)
			$subcategoryid = '-';
		else if(null != $subcategoryid && strlen($subcategoryid)>1)
			$subcategoryid = str_replace("-"," ",$subcategoryid);
		return view('news', ['imax' => $imax,'categoryid'=>$categoryid,'subcategoryid'=>$subcategoryid]);
	},
	'as' => 'berita'
]);

Route::get('/newssinglee/{idd}',[
	'uses' => function($idd)
	{
		try {
			$data = DB::table('article')->where('Title',str_replace("-"," ",$idd))->first();
			DB::table('article')->where('Title',str_replace("-"," ",$idd))->update(['viewcount'=>($data->ViewCount+1)]);	
		} catch (Exception $e) {
			Request::session()->flash('errlogin','Route ke detail baca berita gagal!');
			return view("home");
		}		
		$subname = '-';
		try {
			if($data->SubcategoryID != null)
			$subname = DB::table('subcategory')->where('SubcategoryID',$data->SubcategoryID)->first()->SubcategoryName;
		} catch (Exception $e) {
			$subname = '/n Error subname : '.$e;
		}
		$can = '-';
		try {
			$can = DB::table('category')->where('CategoryID',$data->CategoryID)->first()->CategoryName;
		} catch (Exception $e) {
			$can = 'cat id : '.var_dump($data);
		}					
		$data->category = $can;
		$data->subcategory = $subname;
		return view('newssingle', ['data' => $data]);
	},
	'as' => 'newssinglee'
]);

Route::get('/detailberita/{idd}',[
	'uses' => function($idd)
	{
		try {
			$data = DB::table('article')->where('Title',str_replace("-"," ",$idd))->first();
			DB::table('article')->where('Title',str_replace("-"," ",$idd))->update(['viewcount'=>($data->ViewCount+1)]);	
		} catch (Exception $e) {
			Request::session()->flash('errlogin','Route ke detail berita gagal!');
			return view('home');
		}		
		$subname = '-';
		try {
			if($data->SubcategoryID != null)
			$subname = DB::table('subcategory')->where('SubcategoryID',$data->SubcategoryID)->first()->SubcategoryName;
		} catch (Exception $e) {
			Request::session()->flash('errlogin','Route ke detail berita gagal!');
			return view('home');
			//$subname = '/n Error subname : '.$e;
		}
		$can = '-';
		try {
			if($data->CategoryID != null)
			$can = DB::table('category')->where('CategoryID',$data->CategoryID)->first()->CategoryName;
		} catch (Exception $e) {
			Request::session()->flash('errlogin','Route ke detail berita gagal!');
			return view('home');
			//$can = 'cat id : '.var_dump($data);
		}					
		$data->category = $can;
		$data->subcategory = $subname;
		return view('newssingle', ['data' => $data]);
	},
	'as' => 'detailberita'
]);
// 	function()
// {		
// 	$data = DB::table('article')->where('ID',$idd)->first();
// 	$category = DB::table('category')->where('ID',$date->CategoryID);
// 	$data->category = $category;
// 	return view('newssingle', ['data' => $data]);
// });
Route::post('/forgot/kirim','Auth\\ForgotPasswordController@passreset');
Route::post('/deposit/kirim','DepositController@deposit');
Route::post('/transfer/transfer','TransferController@transfer');
Route::post('/profil/gantipassword', 'Auth\\LoginController@changepassword');
Route::post('/registration/submit','RegistrationController@submit');
Route::post('/login1','Auth\\LoginController@authenticate');
Route::post('/loginJSON','Auth\\LoginController@authenticateJSON');
Route::post('/withdraw/kirim','WithdrawController@kirim');
Route::post('/komplain','ComplainController@kirim');
//Route::post('/referalbonus/kirim', 'ReferalBonusController@kirim');
Route::post('/referalbonus/kirim', 'TransferBonusController@kirim');
//Route::post('/cashbackbonus/kirim', 'CashbackBonusController@kirim');
Route::post('/cashbackbonus/kirim', 'TransferBonusController@kirim');
//Route::post('/firstdeposit/kirim', 'FirstDepositBonusController@kirim');
Route::post('/firstdeposit/kirim', 'TransferBonusController@kirim');
Route::post('/cashback/claim', 'ClaimBonusLogController@kirim');
Route::post('/referalbonus/claim', 'ClaimBonusLogController@kirim');
Route::post('/firstdeposit/claim', 'ClaimBonusLogController@kirim');

Route::get('/home', 'HomeController@index')->name('home');
Route::view('/panduanbola', 'panduanbola');
Route::view('/panduankasino', 'panduankasino');
Route::get('/msgpopupOperation',
	function (){
		try {
		    Request::session()->get('id')->msgpopup = 1;
		} catch (Exception $e) {
			
		}
});