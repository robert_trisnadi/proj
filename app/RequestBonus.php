<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestBonus extends Model
{	
    protected $table = 'RequestBonus';
    public $timestamps = false;
}
