<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestGame extends Model
{
    protected $table = 'RequestGame';
    public $timestamps = false;
}
