<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class viewcustomerproductproduct extends Model
{
    protected $table = 'viewcustomerproductproduct';
    public $timestamps = false;
}
