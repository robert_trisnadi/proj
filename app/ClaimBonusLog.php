<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimBonusLog extends Model
{
    protected $table = 'ClaimBonusLog';
    public $timestamps = false;
}
