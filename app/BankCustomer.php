<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankCustomer extends Model
{	
    protected $table = 'BankCustomer';
    public $timestamps = false;
}
