<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transferbonuslog extends Model
{
    protected $table = 'transferbonuslog';
    public $timestamps = false;
}
