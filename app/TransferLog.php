<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferLog extends Model
{
    protected $table = 'Transferlog';
    public $timestamps = false;
}
