<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerQuestions extends Model
{	
    protected $table = 'CustomerQuestions';
    public $timestamps = false;
}
