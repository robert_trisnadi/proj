<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{	
    protected $table = 'Bank';
    public $timestamps = false;
}
