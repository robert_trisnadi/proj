<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerQuestionsCustomerView extends Model
{
    protected $table = 'CustomerQuestionsCustomerView';
    public $timestamps = false;
}
