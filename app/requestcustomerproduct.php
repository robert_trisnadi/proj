<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class requestcustomerproduct extends Model
{	
    protected $table = 'requestcustomerproduct';
    public $timestamps = false;
}
