<?php

namespace App\Http\Controllers;

use App\ClaimBonusLog;
use App\RequestBonus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class ClaimBonusLogController extends Controller
{
    public function kirim(Request $request){

    	try {
    		if(!Session::has('id'))
			{
			   Redirect::to('/')->send();
			}
			// 1 2 3 f r c
			//RequestBonusClaimBonusView
			$data = null;
			$total = 0;
			$btype = 0;
			$ermsg = '';

			switch($request->input('type'))
			{
				case 'firstdeposit': $btype = 1;$ermsg = 'first deposit bonus'; break;
				case 'cashback': $btype = 3; $ermsg = 'cashback bonus'; break;
				case 'referral': $btype = 2; $ermsg = 'referral bonus'; break;
			}
			$customerid = $request->session()->get('id')->CustomerID;
			
			$exists = DB::table('RequestBonusClaimBonusView')
				->where('CustomerID',$customerid)
				->where('BonusTypeID',$btype)
				->where('Status',0)
				->first();
	        if(null != $exists)
	            return Redirect::back()->with('err','Anda sedang memiliki request claim '.$ermsg.' yang sedang diproses.');

	        $rb = new RequestBonus;
	        $statement = DB::select("SHOW TABLE STATUS LIKE 'RequestBonus'");
            $RequestBonusID = $statement[0]->Auto_increment;
	        $rb->CustomerID = $customerid;
	        $rb->RequestTypeID = 5;
	        $rb->BonusTypeID = $btype;
	        $rb->RequestDate = date('Y-m-d H:i:s');
	        $rb->Status = 0;
	        $rb->save();


    		$cp = new ClaimBonusLog;
    		$cp->RequestBonusID = $RequestBonusID;
    		$total = 0;
    		$data = '';
    		switch($btype)
    		{
    			case 1:
	    			$data = DB::table('CustomerProduct')
						->where('CustomerID', $customerid)
						->join('firstdepositbonus','firstdepositbonus.customerproductid','=','customerproduct.CustomerProductID')
						->select('CustomerProduct.CustomerProductID',DB::raw('firstdepositbonus.TimeInput as Tanggal'),DB::raw("'bonus first deposit' as Keterangan" ),DB::raw('(VarFirstDepo * Transaction / 100) as Jumlah'),'firstdepositbonus.Status')
						->get();

					DB::table('firstdepositbonus')
						->where('Status',1)
						->whereIn('CustomerProductID',$data->pluck('CustomerProductID')->all())
						->update(['RequestBonusID'=>$RequestBonusID,'Status'=>3]);

					foreach ($data as $var) {
						if($var->Status == 1)
						{
							$total+=floatval($var->Jumlah);
						}
					}
    			break;//f

    			case 3:
	    			$data = DB::table('cashbackbonusview')
						->where('CustomerID',$customerid)
						->where('Status' , 1)
						->get();
					DB::table('cashbackbonus')
						->whereIn('CustomerProductID',$data->pluck('CustomerProductID')->all())
						->where('Status',1)
						->update(['RequestBonusID'=>$RequestBonusID,'Status'=>3]);

					$total = 0;
					foreach ($data as $var) {
						if($var->status == 1)
						{		
							$total+=floatval($var->BonusCashback);
						}
					}
    			break;//c

    			case 2:
    				$data = DB::table('ReferralBonusView')
						->where('UplineID',$customerid)
						->where('Status' , 1)
						->get();
					$cashbackids = $data->pluck('CashbackBonusID')->all();
					DB::table('referralbonus')
						->whereIn('CashbackBonusID',$cashbackids)
						->update(['RequestBonusID'=>$RequestBonusID,'Status'=>3]);
						
					$total = 0;
					foreach ($data as $var) {
						if($var->Status == 1)
						{
							$total+=floatval($var->ReferralBonus);
						}
					}
    			break;//r
    		}
			
			
			$cp->Amount = $total;	    	
	    	$cp->save();

	    	$request->session()->flash('message', 'Request claim '.$ermsg.' berhasil!');
	    	return Redirect::back();
    	} catch (Exception $e) {
    		$request->session()->flash('error', 'Gagal request claim '.$ermsg.'. Kesalahan pada input : '.$e);
    		return Redirect::back();
    	}

    }
}
