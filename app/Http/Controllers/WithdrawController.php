<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Withdraw;
use App\RequestCustomerProduct;

class WithdrawController extends Controller
{
    public function kirim(Request $request)
    {
    	$this->validate($request,[
			'bankname'=>'required',
			'norekbank'=>'required',
			'pemilikbank'=>'required',
			'productname'=>'required',
			'jumlah'=>'required',
			'membertime'=>'required'
		]);
        if(preg_match('/\W/',''.($request->input('productname')),$match))
            return Redirect::back()->with('err','Game yang diinput tidak valid.');
		if(!is_numeric($request->input('jumlah')) || doubleval($request->input('jumlah'))<50000)
            return Redirect::back()->with('err','Jumlah harus angka dan lebih besar dari Rp. 50.000,00.');
        if($request->input('jumlah') > 999999999999)
            return Redirect::back()->with('err','Jumlah yang diinput maksimal 12 digit.');
        if(preg_match('/^[0-9]+$/',$request->input('jumlah'),$match) == 0)
            return Redirect::back()->with('err','Jumlah yang dimasukkan harus angka dan tidak mengandung desimal.');
    	try {
            $exist = DB::table('withdrawview')
                ->where('CustomerID',$request->session()->get('id')->CustomerID)
                ->where('Status',0)
                ->first();
            if($exist != null)
                return Redirect::back()->with('err','Anda sudah memiliki request withdraw! Silahkan tunggu response dari admin.');
            $existcp = DB::table('customerproduct')
                ->where('CustomerID',$request->session()->get('id')->CustomerID)
                ->where('CustomerProductID',$request->input('productname'))
                ->where('Status',4)
                ->first();
            if($existcp == null)
                return Redirect::back()->with('err','Anda tidak terdaftar pada game spesifik.');

            $rq = new RequestCustomerProduct;
            $statement = DB::select("SHOW TABLE STATUS LIKE 'requestcustomerproduct'");
            $RequestCustomerProductID = $statement[0]->Auto_increment;
            $rq->RequestTypeID = 3;
            $rq->CustomerProductID = $request->input('productname');
            $rq->RequestDate = date('Y-m-d H:i:s');
            $rq->Operator = '-';
            $rq->Note = '-';            
            $rq->Status = 0;
            $rq->save();

    		$c = new Withdraw;
            $c->RequestCustomerProductID = $RequestCustomerProductID;
            $c->Amount = $request->input('jumlah');            
    		$c->save();
    	} catch (Exception $e) {
    		$request->session()->flash('err', ''.$e);
    		return Redirect::back();
    	}
    	$request->session()->flash('message', 'Permohonan withdraw berhasil dilakukan.');
    	return Redirect::back();
    }
}