<?php

namespace App\Http\Controllers;

use App\cashbackbonus;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Deposit;
use App\requestcustomerproduct;
use Illuminate\Support\Facades\DB;

class DepositController extends Controller
{
    public function deposit(Request $request)
    {
    	$this->validate($request,[			
			'productname'=>'required',
			'depositdate'=>'required',
			'banktujuan'=>'required',
			'jumlah'=>'required'
		]);
        if(preg_match('/\W/',''.($request->input('productname')),$match))
            return Redirect::back()->with('err','Game yang diinput tidak valid.');
        if(strtotime($request->input('depositdate'))=='')
            return Redirect::back()->with('err','Input pada tanggal deposit tidak valid.');
        if(preg_match('/\W/',''.($request->input('banktujuan')),$match))
            return Redirect::back()->with('err','Bank tujuan tidak valid.');        
    	if(!is_numeric($request->input('jumlah')) || doubleval($request->input('jumlah'))<50000)
    		return Redirect::back()->with('err','Jumlah harus angka dan lebih besar dari Rp. 50.000,00.');
        if(preg_match('/^[0-9]+$/',$request->input('jumlah'),$match) == 0)
            return Redirect::back()->with('err','Jumlah yang dimasukkan harus angka dan tidak mengandung desimal.');
    	if($request->input('jumlah') > 999999999999)
    		return Redirect::back()->with('err','Jumlah yang dideposit maksimal 12 digit.');
    	
    	//create new Deposit
    	try {
    		$exist = DB::table('requestcustomerview')
    			->where('CustomerID',$request->session()->get('id')->CustomerID)
    			->where('Status',0)
    			->first();
    		if($exist != null)
    			return Redirect::back()->with('err','Anda sudah memiliki request deposit! Silahkan tunggu response dari admin.');
    		//game,tanggal,bank tujuan,amount
            $existcp = DB::table('customerproduct')
                ->where('CustomerID',$request->session()->get('id')->CustomerID)
                ->where('CustomerProductID',$request->input('productname'))
                ->where('Status',4)
                ->first();
            if($existcp == null)
                return Redirect::back()->with('err','Anda tidak terdaftar pada game spesifik.');

            $existBank = DB::table('Bank')->where('BankID',$request->input('banktujuan'))->first();
            if($existBank == null)
                return Redirect::back()->with('err','Bank tujuan tidak valid.');

    		$rq = new requestcustomerproduct;
    		$statement = DB::select("SHOW TABLE STATUS LIKE 'requestcustomerproduct'");
            $RequestCustomerProductID = $statement[0]->Auto_increment;
    		$rq->RequestTypeID = 1;
    		$rq->CustomerProductID = $request->input('productname');
    		$rq->RequestDate = date('Y-m-d H:i:s');
    		$rq->Operator = '-';
	    	$rq->Note = '-';	    	
	    	$rq->Status = 0;
	    	$rq->save();

    		$c = new Deposit;
            $c->RequestCustomerProductID = $RequestCustomerProductID;
            $c->Amount = $request->input('jumlah');
            $c->BankID = $request->input('banktujuan');
            $c->DepositDate = date('Y-m-d',strtotime($request->input('depositdate')));
	    	$c->save();	    	

    	} catch (Exception $e) {
    		$request->session()->flash('err', ''.$e);
    		return Redirect::to('deposit');
    	}    	    	
    	$request->session()->flash('message', 'Request deposit berhail dilakukan.');
    	return Redirect::to('deposit');
    }
}
