<?php

namespace App\Http\Controllers;

use App\transferbonuslog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class CashBackBonusController extends Controller
{
	public function kirim(Request $request){

    	try {
    		if(!Session::has('id'))
			{
			   Redirect::to('/')->send();
			}
			//requestbonustransferbonusview
			if(null==$request->input('productname'))
			{
				$request->session()->flash('err', 'Game belum terdaftar atau tidak boleh kosong.');
    			return Redirect::back();
			}
			$data = DB::table('cashbackbonusview')
				->where('Username',request()->session()->get('id')['Username'])
				->where(function ($query) {
		    			$query->where('Status', 'Approved')
		          			  ->orWhere('Status','Claimed Referral');})
				->get();
			$total = 0;
			foreach ($data as $var) {
				if(($var->Status =='Approved'||$var->Status =='Claimed Referral') && $var->inputbonuslogid != '-')
				{
					if($var->Operator == '-')
						$total-=floatval($var->Total);
					else
						$total+=floatval($var->Total);
				}	
			}
			if($total <= 0)
			{
				$request->session()->flash('err', 'Anda tidak memiliki bonus.');
    			return Redirect::back();
			}

			$exists = DB::table('transferbonuslog')
				->where('Username',$request->session()->get('id')['Username'])
				->where('Status','Pending')
				->where('Type','CASHBACK BONUS')				
				->first();
	        if(null != $exists)
	            return Redirect::back()->with('err','Anda sedang memiliki request transfer bonus cashback yang sedang diproses.');

	        $exists = DB::table('claimbonuslog')
				->where('Username',$request->session()->get('id')['Username'])
				->where('Status','Pending')
				->where('Type','CASHBACK BONUS')//bonus bank requestdate
				->first();
	        if(null != $exists)
	            return Redirect::back()->with('err','Anda sedang memiliki request claim bonus cashback yang sedang diproses.');
	  

    		$cp = new transferbonuslog;
	    	$cp->Username = $request->session()->get('id')['Username'];
	    	$cp->Type = 'CASHBACK BONUS';
	    	$cp->Bonus = $total;
	    	$cp->Product = $request->input('productname');
	    	$cp->RequestDate = date('Y-m-d H:i:s');
	    	$cp->Operator = '';
	    	$cp->Status = 'Pending';
	    	$cp->updated_at = date('Y-m-d H:i:s');
	    	$cp->save();

	    	$request->session()->flash('message', 'Request transfer cashback bonus berhasil!');
	    	return Redirect::back();
    	} catch (Exception $e) {
    		$request->session()->flash('error', 'Gagal request transfer cashback bonus. Kesalahan pada input.');
    		return Redirect::back();
    	}

    }
}
