<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\TransferLog;
use App\RequestCustomerProduct;

class TransferController extends Controller
{
    public function transfer(Request $request)
    {
    	$this->validate($request,[
			'transferdari'=>'required',
			'transferke'=>'required',
			'jumlah'=>'required'			
		]);
        if(preg_match('/\W/',''.($request->input('transferdari')),$match))
            return Redirect::back()->with('err','Game yang akan ditransfer tidak valid.');
        if(preg_match('/\W/',''.($request->input('transferke')),$match))
            return Redirect::back()->with('err','Game tujuan transfer tidak valid.');
		if(!is_numeric($request->input('jumlah')) || doubleval($request->input('jumlah'))<50000)
            return Redirect::back()->with('err','Jumlah harus angka dan lebih besar dari Rp. 50.000,00.');
        if($request->input('jumlah') > 999999999999)
            return Redirect::back()->with('err','Jumlah yang diinput maksimal 12 digit.');
        if(preg_match('/^[0-9]+$/',$request->input('jumlah'),$match) == 0)
            return Redirect::back()->with('err','Jumlah yang dimasukkan harus angka dan tidak mengandung desimal.');
        else if($request->input('transferdari') == $request->input('transferke'))
            return Redirect::back()->with('err','Transfer hanya boleh dilakukan pada 2 akun permainan yang berbeda.'); 

         $exist = DB::table('transferlogview')
                ->where('CustomerID',$request->session()->get('id')->CustomerID)
                ->where('Status',0)
                ->first();
            if($exist != null)
                return Redirect::back()->with('err','Anda sudah memiliki request transfer! Silahkan tunggu response dari admin.');

            $existcp = DB::table('customerproduct')
                ->where('CustomerID',$request->session()->get('id')->CustomerID)
                ->where('CustomerProductID',$request->input('transferdari'))
                ->where('Status',4)
                ->first();
            if($existcp == null)
                return Redirect::back()->with('err','Anda tidak terdaftar pada permainan akun pertama.');

            $existcp = DB::table('customerproduct')
                ->where('CustomerID',$request->session()->get('id')->CustomerID)
                ->where('CustomerProductID',$request->input('transferke'))
                ->where('Status',4)
                ->first();
            if($existcp == null)
                return Redirect::back()->with('err','Anda tidak terdaftar pada permainan akun tujuan.');

    	try {

            $rq = new RequestCustomerProduct;
            $statement = DB::select("SHOW TABLE STATUS LIKE 'requestcustomerproduct'");
            $RequestCustomerProductID = $statement[0]->Auto_increment;
            $rq->RequestTypeID = 4;
            $rq->CustomerProductID = $request->input('transferdari');
            $rq->RequestDate = date('Y-m-d H:i:s');
            $rq->Operator = '-';
            $rq->Note = '-';
            $rq->Status = 0;
            $rq->save();

    		$c = new TransferLog;    		
    		$c->RequestCustomerProductID = $RequestCustomerProductID;    		
    		$c->CustomerProductID = $request->input('transferke');//transfer to
    		$c->Amount = $request->input('jumlah');    		
    		$c->save();

    	} catch (Exception $e) {
    		$request->session()->flash('err', ''.$e);
    		return Redirect::back();
    	}
    	$request->session()->flash('message', 'Request Transfer berhail dilakukan.');
    	return Redirect::back();
    }
}
