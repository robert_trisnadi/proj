<?php

namespace App\Http\Controllers;

use App\transferbonuslog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class FirstDepositBonusController extends Controller
{
    public function kirim(Request $request)
    {
    	try {
    		if(!Session::has('id'))
			{
			   Redirect::to('/')->send();
			}

			$data = DB::table('firstdepositview')->where('Username', 
				request()->session()->get('id')['Username'])->where('Jumlah','>',0)->get();
			$total = 0;
			foreach ($data as $var) {
				if($var->Status =='Approved' && $var->inputbonuslogid != '-')
				{
					if($var->Operator == '-')
						$total-=floatval($var->Jumlah);
					else
						$total+=floatval($var->Jumlah);
				}
			}
			if($total <= 0)
			{
				$request->session()->flash('err', 'Anda tidak memiliki bonus.');
    			return Redirect::back();
			}			

			if(null==$request->input('productname'))
			{
				$request->session()->flash('err', 'Game belum terdaftar atau tidak boleh kosong.');
    			return Redirect::back();
			}
			$exists = DB::table('transferbonuslog')
				->where('Username',$request->session()->get('id')['Username'])
				->where('Status','Pending')
				->where('Type','FIRST DEPO BONUS')				
				->first();
	        if(null != $exists)
	            return Redirect::back()->with('err','Anda sedang memiliki request transfer bonus first deposit yang sedang diproses.');
	        $exists = DB::table('claimbonuslog')
				->where('Username',$request->session()->get('id')['Username'])
				->where('Status','Pending')
				->where('Type','FIRST DEPO BONUS')//bonus bank requestdate
				->first();
	        if(null != $exists)
	            return Redirect::back()->with('err','Anda sedang memiliki request claim bonus first deposit yang sedang diproses.');

    		$cp = new transferbonuslog;
	    	$cp->Username = $request->session()->get('id')['Username'];
	    	$cp->Type = 'FIRST DEPO BONUS';
	    	$cp->Bonus = $total;
	    	$cp->Product = $request->input('productname');
	    	//$cp->RequestDate = $request->input('membertime');
	    	$cp->RequestDate = date('Y-m-d H:i:s');
	    	$cp->Operator = '';
	    	//$cp->OfficeDate = date('Y-m-d H:i:s');
	    	$cp->Status = 'Pending';
	    	$cp->updated_at = date('Y-m-d H:i:s');
	    	$cp->save();

	    	$request->session()->flash('message', 'Request transfer first deposit bonus berhasil!');
	    	return Redirect::back();
    	} catch (Exception $e) {
    		$request->session()->flash('error', 'Gagal request transfer first deposit bonus. Kesalahan pada input.');
    		return Redirect::back();
    	}
    }
}
