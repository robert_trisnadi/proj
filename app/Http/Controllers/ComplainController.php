<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Complain;
use App\LoginInformation;

class ComplainController extends Controller
{
    public function kirim(Request $request)
    {
        try {
            if(
                $request->input('username') == null ||
                $request->input('handphone') == null||
                $request->input('emailkomplain') == null ||
                $request->input('komplain') == null
            )
            {
                $request->session()->flash('errkomplain', 'Semua field harus diisi kecuali file upload.');
                return Redirect::back();
            }            
            if(preg_match('/\W/',$request->input('username'),$match))
                return Redirect::back()->with('errkomplain','Username tidak boleh mengandung karakter non-kata.');            
            if(!is_numeric($request->input('handphone')) || intval($request->input('handphone'))<=0){
                $request->session()->flash('errkomplain', 'Nomor handphone harus numerik.');
                return Redirect::back();       
            }
            else if(!is_numeric($request->input('handphone')))
            {
                $request->session()->flash('errkomplain', 'Nomor handphone harus terdiri dari angka.');
                return Redirect::back();
            }
            if(preg_match('/^[a-zA-Z0-9 .\-]+$/',$request->input('komplain'),$match) == 0)
                return Redirect::back()->with('errkomplain','Komplain tidak boleh mengandung karakter non-kata.');
            $li = LoginInformation::where('Username',$request->input('username'))->first();
            if(!isset($li))
            {
                $request->session()->flash('errkomplain', 'Username tidak terdaftar.');
                return Redirect::back();
            }
            $complain = new Complain;
            $complain->LoginID = $li->LoginID;            
            $complain->handphone = $request->input('handphone');            
            $complain->Content = $request->input('komplain');
            $complain->CreatedDate = date('Y-m-d H:i:s');
            $complain->Status = 0;
            if($request->file('uploadfile'))
               $request->file('uploadfile')->store('files');
            $complain->save();

            $request->session()->flash('message', 'Komplain anda telah dilaporkan.');
            return Redirect::back();
        } catch (Exception $e) {
            return Redirect::to('/')->with('err','Komplain gagal pada sistem.');
        }
        
    }
}
