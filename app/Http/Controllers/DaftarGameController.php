<?php

namespace App\Http\Controllers;
use App\CustomerProduct;
use App\RequestGame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class DaftarGameController extends Controller
{
    public function daftargame(Request $request)
    {
    	try {
    		if(!Session::has('id'))
			{
			   Redirect::to('/')->send();
			}

			if(preg_match('/\W/',''.($request->input('productid')),$match))
                return Redirect::back()->with('err','Game yang diinput tidak valid.');
        	$existProduct = DB::table('Product')->where('ProductID',$request->input('productid'))->where('Status',4)->first();
        	if(null == $existProduct)
        	{
        		$request->session()->flash('err', 'Game yang diinput tidak valid.');
				return Redirect::back();
        	}

			$exist = RequestGame::where('CustomerID', '=', $request->session()->get('id')->CustomerID)
				->where('ProductID', '=', 
            		intval($request->input('productid')))->first();
			
			if($exist != null)
			{
				if($exist->Status == 2)
				{
					$request->session()->flash('err', 'Anda sudah memiliki request pembuatan akun permainan ini.');
					return Redirect::back();
				}
				$request->session()->flash('err', 'Anda sudah memiliki request pembuatan akun permainan ini.');
				return Redirect::back();
			}

			$exist = CustomerProduct::where('CustomerID', '=', $request->session()->get('id')->CustomerID)
				->where('ProductID', '=', 
            		intval($request->input('productid')))->first();

			if($exist != null)
			{
				if($exist->Status == 4)
				{
					$request->session()->flash('err', 'Anda sudah memiliki akun permainan ini.');
    				return Redirect::back();
				}
				else if($exist->Status == 5)
				{
					$request->session()->flash('err', 'Akun permainan anda berstaus Non-Active. Silahkan kontak Customer Service kami untuk info lebih lanjut.');
    				return Redirect::back();
				}
			}

    		$cp = new RequestGame;
	    	$cp->CustomerID = $request->session()->get('id')->CustomerID;
	    	$cp->ProductID = intval($request->input('productid'));	    	
	    	$cp->Status = 0;	    	
	    	$cp->save();

	    	$request->session()->flash('message', 'Request daftar game berhasil dilakukan.');
	    	return Redirect::back();
    	} catch (Exception $e) {
    		$request->session()->flash('err', 'Gagal melakukan registrasi.');
    		return Redirect::back();
    	}
    }
}