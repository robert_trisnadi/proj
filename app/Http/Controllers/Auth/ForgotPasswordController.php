<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\LoginInformation;
use App\CustomerQuestionsCustomerView;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    public function passreset(Request $request)
    {
        $this->validate($request,[
            'username'=>'required|string|max:30|min:3',
            'plupapassword'=>'required',
            'jlupapassword'=>'required|min:4|max:30',
            'passwordbaru'=>'required|min:4|max:20',
            'kpassword'=>'required|min:4|max:20',
        ]);
         if(preg_match('/\W/',$request->input('username'),$match))
                return Redirect::back()->with('err','Username tidak boleh mengandung karakter non-kata.');
        // if(preg_match('/\W/',$request->input('passwordbaru'),$match))
        //     return Redirect::back()->with('err','Password baru tidak boleh mengandung karakter non-kata.');
        // if(preg_match('/\W/',$request->input('kpassword'),$match))
        //     return Redirect::back()->with('err','Konfirmasi password tidak boleh mengandung karakter non-kata.');        
            if(preg_match('/^[a-zA-Z0-9 .\-]+$/',$request->input('jlupapassword'),$match) == 0)
                return Redirect::back()->with('err','Jawaban lupa password tidak boleh mengandung karakter non-kata.');
        if(preg_match('/\W/',''.($request->input('plupapassword')),$match))
            return Redirect::back()->with('err','Pertanyaan lupa password tidak valid.');

        $plupa = DB::table('ForgotQuestions')->where('forgotquestionsid',$request->input('plupapassword'))->where('Status',1)->first();
        if(null == $plupa)
            return Redirect::back()->with('err','Pertanyaan lupa password tidak bisa dipakai.');
        if($request->input('passwordbaru') != $request->input('kpassword'))
        {
            return Redirect::back()->withErrors(['Konfirmasi password tidak sesuai.']);
        }
        $data = DB::table('CustomerQuestionsCustomerView')
            ->where('Username',$request->input('username'))
            ->where('ForgotQuestionsID',$request->input('plupapassword'))
            ->where('Answer',$request->input('jlupapassword'))
            ->get();
        if(count($data)>0)
        {            
            DB::table('LoginInformation')
                ->where('CustomerID',$data[0]->CustomerID)
                ->update(['Password'=>password_hash($request->input('passwordbaru'),PASSWORD_BCRYPT)]);
            return Redirect::to('/')->with('msg','Reset password berhasil! Anda bisa log-in dengan password baru.');
        }
        else
        {
            return Redirect::back()->withErrors(['Reset password tidak berhasil. Pastikan informasi yang anda masukkan benar.']);            
        }

        
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
