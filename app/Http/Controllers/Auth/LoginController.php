<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\LoginInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function changepassword(Request $request)
    {
        $this->validate($request,[
                'password'=>'required|min:4|max:20',
                'kpassword'=>'required|min:4|max:20',
                'passwordbaru'=>'required|min:4|max:20'
            ]);

        // if(preg_match('/\W/',$request->input('password'),$match))
        //     return Redirect::back()->with('err','Password tidak boleh mengandung karakter non-kata.');
        // if(preg_match('/\W/',$request->input('kpassword'),$match))
        //     return Redirect::back()->with('err','Konfirmasi password tidak boleh mengandung karakter non-kata.');
        // if(preg_match('/\W/',$request->input('passwordbaru'),$match))
        //     return Redirect::back()->with('err','Password baru tidak boleh mengandung karakter non-kata.');
        if($request->input('kpassword') != $request->input('passwordbaru'))
        {
            return Redirect::to('/profil')->with('err','Password baru tidak sama dengan konfirmasi password.');
        }
        else
        {
             $data = DB::table('LoginInformation')
                ->where('Username',$request->session()->get('id')->Username)
                ->get();            
            if(count($data)>0)
            { 
                if(password_verify($request->input('password'),$data[0]->Password) == true)
                {
                    DB::table('LoginInformation')
                        ->where('CustomerID',$request->session()->get('id')->CustomerID)
                        ->update(['Password'=>password_hash($request->input('passwordbaru'),PASSWORD_BCRYPT)]);
                    return Redirect::to('/profil')->with('message','Password reset sukses!');
                }
                else
                    return Redirect::to('/profil')->with('err','Password lama salah.');
            }
            else
            {
                return Redirect::to('/profil')->with('err','Password lama salah.');
            }
        }
    }

    public function deauthenticate(Request $request)
    {
        $request->session()->pull('id', 'default');         
        $request->session()->pull('bonuscashback', 'default');
        $request->session()->pull('bonusreferral', 'default');
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    public function authenticate(Request $request)
    {
        try {
            if($request->input('username') == "" || $request->input('password') == "")
            {
                $request->session()->flash('errlogin','Username atau password harus diisi!');
                return Redirect::back();
            }        

            $auth = LoginInformation::where('username', '=', $request->username)->first();
            if($auth){
                if($auth->Status != 4)//tidak aktif
                {
                    $request->session()->flash('errlogin','Status login anda tidak aktif. Harap menghubungi Customer service.');
                    return Redirect::back();
                }
                else if(password_verify($request->input('password'),$auth->Password) == true)
                {
                    DB::table('LoginInformation')->where('Username', $request->username)->update(['LastLoginTime'=>date('Y-m-d H:i:s')]);            
                    $lcb = DB::table('LoginCustomerBankView')->where('username', '=', $request->username)->first();
                    $lcb->statuss = DB::table('StatusTable')->get();
                    Auth::login($auth);
                    $request->session()->put('id', $lcb);
                    return redirect('profil');
                }
                else
                {
                    $request->session()->flash('errlogin','Username atau password salah!');
                    return Redirect::back();
                }       
            }
            else
            {
                $request->session()->flash('errlogin','Username atau password salah!');
                return Redirect::back();
            }
        } catch (Exception $e) {
            return Redirect::to('/')->with('err','Proses login gagal pada sistem.');
        }
        
    }

    public function authenticateJSON(Request $request)
    {
        try {
            if($request->input('username') == "" || $request->input('password') == "")
            {
                $obj = 'Username atau password harus diisi!';
                return json_encode($obj);
                // $request->session()->flash('errlogin','Username atau password harus diisi!');
                // return Redirect::back();
            }        

            $auth = LoginInformation::where('username', '=', $request->username)->first();
            if($auth){
                if(password_verify($request->input('password'),$auth->Password) == true)
                {
                    DB::table('LoginInformation')->where('Username', $request->username)->update(['LastLoginTime'=>date('Y-m-d H:i:s')]);            
                    $lcb = DB::table('LoginCustomerBankView')->where('username', '=', $request->username)->first();
                    $lcb->statuss = DB::table('StatusTable')->get();
                    Auth::login($auth);
                    $request->session()->put('id', $lcb);
                    return redirect('profil');
                }
                else
                {
                     $obj = 'Username atau password salah!';
                    return json_encode($obj);
                    // $request->session()->flash('errlogin','Username atau password salah!');
                    // return Redirect::back();
                }       
            }
            else
            {
                $obj = 'Username atau password salah!';
                return json_encode($obj);
                //$request->session()->flash('errlogin','Username atau password salah!');                
                //return Redirect::back();
            }
        } catch (Exception $e) {
            $obj = 'Proses login gagal pada sistem.';
            return json_encode($obj);
            //return Redirect::to('/')->with('err','Proses login gagal pada sistem.');
        }
        
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
