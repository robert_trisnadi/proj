<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Customer;
use App\LoginInformation;
use App\BankCustomer;
use App\CustomerQuestions;
use Illuminate\Support\Facades\DB;

class RegistrationController extends Controller
{
    public function submit(Request $request)
    {
    	try {
           $this->validate($request,[
                'username'=>'required|max:30|min:3|unique:LoginInformation,Username',
                'password'=>'required|min:4|max:20',
                'bankname'=>'required',
                'norekbank'=>'required|min:9|max:20',
                'pemilikbank'=>'required|max:50',
                'kpassword'=>'required|min:4|max:20',
                'email'=>'required|email|max:255',
                'plupapassword'=>'required',
                'jlupapassword'=>'required|min:4|max:30',
                'cbkonfirmasi'=>'required',                
            ]);
            if(preg_match('/\W/',$request->input('username'),$match))
                return Redirect::back()->with('err','Username tidak boleh mengandung karakter non-kata.');
            if ($request->input('username') == trim($request->input('username')) && strpos($request->input('username'), ' ') !== false) {
                return Redirect::back()->with('err','Username tidak boleh mengandung spasi');
            }
            if($request->input('password') != $request->input('kpassword'))
                return Redirect::back()->with('err','Konfirmasi password tidak sesuai dengan password.');

            $exists = DB::table('BankCustomer')->where('BankAccountNumber',$request->input('norekbank'))->first();
            if($exists != null)
                return Redirect::back()->with('err','Nomor rekening ini telah terdaftar!');
            if(!is_numeric($request->input('norekbank')))
                return Redirect::back()->with('err','Nomor rekening harus numerik.');
            if(preg_match('/^[0-9]+$/',$request->input('norekbank'),$match) == 0)
                return Redirect::back()->with('err','Nomor rekening harus numerik.');
            if(preg_match('/^[a-zA-Z0-9 .\-]+$/',$request->input('pemilikbank'),$match) == 0)
                return Redirect::back()->with('err','Nama pemilik bank tidak boleh mengandung karakter non-kata.');            
            if(preg_match('/[[:digit:]]/',$request->input('pemilikbank'),$match))
                return Redirect::back()->with('err','Nama pemilik rekening tidak boleh mengandung angka.');            
            if(preg_match('/^[a-zA-Z0-9 .\-]+$/',$request->input('jlupapassword'),$match) == 0)
                return Redirect::back()->with('err','Jawaban lupa password tidak boleh mengandung karakter non-kata.');
            if(preg_match('/\W/',''.($request->input('plupapassword')),$match))
                return Redirect::back()->with('err','Pertanyaan lupa password tidak valid.');

            $plupa = DB::table('ForgotQuestions')->where('ForgotQuestionsID',$request->input('plupapassword'))->where('Status',1)->first();
            if(null == $plupa)
                return Redirect::back()->with('err','Pertanyaan lupa password tidak valid.');
            //create new customer    
            $c = new Customer;      
            $c->email = strtolower($request->input('email'));
            $c->CreatedDate = date('Y-m-d H:i:s');
            $c->referralcode = md5($request->input('username'));
            $c->Status = 4;
            if(null!=$request->input('upline'))
            {
                $cis = DB::table("customer")->where('referralcode',$request->input('upline'))->get();            
                $c->UplineID = $cis[0]->CustomerID;
            }
            $statement = DB::select("SHOW TABLE STATUS LIKE 'customer'");
            $nextId = $statement[0]->Auto_increment;
            $c->save();

            $li = new LoginInformation;
            $li->Username = strtolower($request->input('username'));
            $li->Password = password_hash($request->input('password'),PASSWORD_BCRYPT);
            $li->CustomerID = $nextId;
            $li->Status = 4;
            $li->save();

            $b = new BankCustomer;
            $b->BankID = $request->input('bankname');
            $b->CustomerID = $li->CustomerID;
            $b->BankAccountNumber = $request->input('norekbank');
            $b->BankAccountName = ucwords(strtolower($request->input('pemilikbank')));
            $b->save();

            $fp = new CustomerQuestions;
            $fp->CustomerID = $li->CustomerID;
            $fp->ForgotQuestionsID = $request->input('plupapassword');
            $fp->Answer = $request->input('jlupapassword');
            $fp->save();

            // //redirect
            $request->session()->flash('msg', 'Registrasi berhasil!');        
            return redirect('/'); 
        } catch (Exception $e) {
            return Redirect::to('/')->with('err','Proses registrasi gagal pada sistem.');
        }
    }
}
