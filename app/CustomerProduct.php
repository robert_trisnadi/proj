<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerProduct extends Model
{
    protected $table = "customerproduct";
    public $timestamps = false;
}
