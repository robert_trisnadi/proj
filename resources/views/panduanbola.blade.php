@extends('layouts.app')
@section('contentdescription')
	<?php echo "Panduan bola di ".$_SERVER['HTTP_HOST']."! Website agen betting terpercaya.";?>	
@endsection
@section('content')
<link rel="stylesheet" href="assets/css/gamehelp.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Panduan Bola</h1>
				</header>
				<div class="page-single-content game-guide">					
					<h2 class="title">Panduan Cara Main Judi Bola T4SPORT.info</h2>
					<hr />
					<p>Disini kami akan memberikan panduan untuk cara bermain judi bola online, ada beberapa permainan dalam judi bola seperti taruhan untuk pertandingan penuh <i>Full Time</i> atau hanya pertandingan awal <i>First Half</i> dengan taruhan HDP <small class="text-muted">(<i>Handicap</i>)</small>, <i>1x2</i>, OU <small class="text-muted">(<i>Over/Under</i>)</small>, OE <small class="text-muted">(<i>Odd/Even</i>)</small>, <i>Correct Score</i> dan lainnya.</p>

					<div class="web-game-guide">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-penjelasan-tab" data-toggle="tab" href="#nav-penjelasan" role="tab" aria-controls="nav-penjelasan" aria-selected="true">Penjelasan</a>
								<a class="nav-item nav-link" id="nav-hdp-tab" data-toggle="tab" href="#nav-hdp" role="tab" aria-controls="nav-hdp" aria-selected="false">Handicap</small></a>
								<a class="nav-item nav-link" id="nav-ou-tab" data-toggle="tab" href="#nav-ou" role="tab" aria-controls="nav-ou" aria-selected="false">Over/Under</a>
								<a class="nav-item nav-link" id="nav-1x2-tab" data-toggle="tab" href="#nav-1x2" role="tab" aria-controls="nav-1x2" aria-selected="false">1X2</a>
								<a class="nav-item nav-link" id="nav-oe-tab" data-toggle="tab" href="#nav-oe" role="tab" aria-controls="nav-oe" aria-selected="false">Odd/Even</a>
								<a class="nav-item nav-link" id="nav-mp-tab" data-toggle="tab" href="#nav-mp" role="tab" aria-controls="nav-mp" aria-selected="false">Mix Parlay</a>
								<a class="nav-item nav-link" id="nav-cs-tab" data-toggle="tab" href="#nav-cs" role="tab" aria-controls="nav-cs" aria-selected="false">Correct Score</a>
							</div>
						</nav>
						<div class="tab-content" id="nav-tabContent">
							<div class="tab-pane fade show active" id="nav-penjelasan" role="tabpanel" aria-labelledby="nav-penjelasan-tab">
								<div class="content-guide">
									<p>Apabila kamu belum mengerti tentang apa itu HDP <small class="text-muted">(<i>Handicap</i>)</small>, <i>1x2</i>, OU <small class="text-muted">(<i>Over/Under</i>)</small>, OE <small class="text-muted">(<i>Odd/Even</i>)</small>, <i>Correct Score</i> kami sarankan untuk membaca penjelasan nya dibawah ini.</p>
									<figure>
										<img src="assets/images/soccer/tabel-bola.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Tabel Judi Bola Online">
										<figcaption>Gambar 1.0 - Contoh Gambar Tabel Lengkap Judi Bola Online</figcaption>
									</figure>
									<p>Gambar di atas merupakan contoh gambar tampilan tabel lengkap untuk taruhan judi bola. Untuk contoh gambar berikutnya, kami hanya akan menampilkan gambar hingga <i>Full Time</i> saja agar tidak membingungkan.</p>
									<dl class="row">
										<dt class="col-sm-3 col-md-2">Time <small class="text-muted">(GMT +8)</small></dt>
										<dd class="col-sm-9 col-md-10">
											Waktu jalan nya pertandingan, untuk jam Indonesia WIB <small class="text-muted">(GMT +7)</small> maka harus dikurangi 1 jam terlebih dahulu. Contoh untuk pertandingan di atas, maka di Indonesia dengan jam WIB akan mulai pada jam 17:00.
										</dd>

										<dt class="col-sm-3 col-md-2">Event</dt>
										<dd class="col-sm-9 col-md-10">
											Tim yang akan bertanding, jika dilihat ada kolom Event ada 2 warna pada tim yang bertanding yaitu <span class="text-red">Merah</span> dan <span class="text-blue">Biru</span>. Perbedaan nya adalah tim warna <span class="text-red">Merah</span> memberikan nilai pasaran atau fur an yang ditentukan oleh masing - masing provider.<br />
										</dd>

										<dt class="col-sm-3 col-md-2">Full Time</dt>
										<dd class="col-sm-9 col-md-10">
											Taruhan yang kamu pasang akan ditentukan menang atau kalah nya setelah pertandingan babak pertama dan babak terakhir selesai. <small class="text-muted">( Waktu Pertandingan : 2 x 45 menit+ )</small>
										</dd>

										<dt class="col-sm-3 col-md-2">Half Time</dt>
										<dd class="col-sm-9 col-md-10">
											Berbeda dengan Full Time, taruhan yang kamu pasang hanya akan dihitung setelah babak pertama pertandingan selesai. <small class="text-muted">( Waktu Pertandingan : 1 x 45 menit+ )</small>
										</dd>

										<dt class="col-sm-3 col-md-2">Pasaran <small class="text-muted">(Fur)</small></dt>
										<dd class="col-sm-9 col-md-10">
											Nilai fur an atau mengalah berapa skor, biasa nya disebut dengan juga dengan pasaran. Fur an hanya ada di HDP <small class="text-muted">(<i>Handicap</i>)</small> atau OU <small class="text-muted">(<i>Over/Under</i>)</small>. Contoh fur an bisa dilihat ada gambar dibawah yang ditandai dengan kotak merah.
											<figure>
												<img src="assets/images/soccer/tabel-contoh-pasaran.png" alt="apa itu pasaran bola, pasaran bola, panduan pasaran, fur an, apa itu leg leg an" title="Contoh Pasaran Bola atau Fur an Bola">
												<figcaption>Gambar 1.1 - Contoh Gambar Pasaran atau Fur an Bola</figcaption>
											</figure>
											<div class="card">
												<div class="card-header">
													Nilai lain Pasaran atau Fur an
												</div>
												<div class="card-body">
													<ol>
														<li>Pasaran <span class="text-fur">0-0</span> disebut leg - leg an atau tidak ada fur.</li>
														<li>Pasaran <span class="text-fur">0-0.5</span> disebut fur seperempat (0.25) bola.</li>
														<li>Pasaran <span class="text-fur">0.5</span> disebut fur setengah bola.</li>
														<li>Pasaran <span class="text-fur">0.5-1</span> disebut fur tiga per empat (0.75) bola.</li>
														<li>Pasaran <span class="text-fur">1</span> disebut fur 1 bola.</li>
														<li>Pasaran <span class="text-fur">1-1.5</span> disebut fur 1 seperempat (1.25) bola.</li>
														<li class="list-unstyled">dan seterusnya..</li>
													</ol>
												</div>
											</div>
										</dd>

										<dt class="col-sm-3 col-md-2">Odds <small class="text-muted">(Key/Uang Air)</small></dt>
										<dd class="col-sm-9 col-md-10">
											<p><i>Odds</i> biasa disebut juga dengan Key atau Uang Air merupakan nilai taruhan kamu. Ada 2 nilai <i>Odds</i> dengan warna yang berbeda, <span class="text-red">Merah</span> <small class="text-muted">(<i>Minus</i>)</small> atau Hitam <small class="text-muted">(<i>Plus</i>)</small>. Untuk memasang taruhan, kamu hanya perlu klik <i>Odds</i> yang ingin kamu pilih setelah itu memasukkan jumlah taruhan.</p>
											<p>Perbedaan nya, apabila kamu pasang <i>Odds</i> <span class="text-red">Merah</span> <small class="text-muted">(<i>Minus</i>)</small> maka ketika kalah kamu harus membayar lebih besar. Sedangkan, apabila kamu pasang <i>Odds</i> Hitam <small class="text-muted">(<i>Plus</i>)</small> maka ketika menang kamu akan mendapat hasil lebih besar.</p>
											<figure>
												<img src="assets/images/soccer/tabel-contoh-odds.png" alt="apa itu odds, odds adalah, taruhan odds, key, uang air" title="Contoh Gambar Odds, Key, Uang Air">
												<figcaption>Gambar 1.2 - Contoh Gambar Odds / Key / Uang Air</figcaption>
											</figure>
											<div class="card">
												<div class="card-header">
													Cara Hitung <i>Odds</i> / Key / Uang Air
												</div>
												<div class="card-body">
													<p>Contoh Taruhan : 100.000</p>
													<dl>
														<dt>Pasang <span class="text-red">France</span> <i>Odds</i> <span class="text-red">-1.13</span> <small class="text-muted">(<i>Minus</i>)</small>  - Kotak Merah 1</dt>
														<dd>
															<ul>
																<li>Menang : Dapat hasil taruhan 1 x 100.000</li>
																<li>Kalah : Bayar taruhan 1.13 x 100.000 = 113.000</li>
															</ul>
														</dd>

														<dt>Pasang <span class="text-blue">Australia</span> <i>Odds</i> 1.07 <small class="text-muted">(<i>Plus</i>)</small>  - Kotak Merah 1</dt>
														<dd>
															<ul>
																<li>Menang : Dapat hasil taruhan 1.07 x 107.000</li>
																<li>Kalah : Bayar taruhan 1 x 100.000 = 100.000</li>
															</ul>
														</dd>
													</dl>
												</div>
											</div>
										</dd>

										<dt class="col-sm-3 col-md-2">HDP <small class="text-muted">(<i>Handicap</i>)</small></dt>
										<dd class="col-sm-9 col-md-10">Taruhan <i>handicap</i> merupakan taruhan yang ditentukan dari fur an atau ada salah satu tim yang lebih kuat.</dd>

										<dt class="col-sm-3 col-md-2">OU <small class="text-muted">(<i>Over/Under</i>)</small></dt>
										<dd class="col-sm-9 col-md-10">
											Taruhan untuk menebak jumlah skor Tim A dan Tim B yang dijumlahkan dari pertandingan yang kamu pasang. Apakah total nilai skor lebih <small class="text-muted">(<i>Over</i>)</small> atau kurang <small class="text-muted">(<i>Under</i>)</small> dari pasaran yang sudah ditentukan. Tidak peduli tim mana pun yang menang maupun kalah, karena kamu hanya bermain tebak total skor.
											<figure>
												<img src="assets/images/soccer/tabel-over-under.png" alt="apa itu over under, cara hitung over under, taruhan over under, over under adalah, judi bola over under" title="Contoh Gambar Tabel Over Under">
												<figcaption>Gambar 1.3 - Contoh Gambar Over Under</figcaption>
											</figure>
											<p>Dilihat pada pasaran OU <small class="text-muted">(<i>Over/Under</i>)</small> diatas, pasaran nya adalah <span class="text-fur">2.5-3</span> atau fur 2 tiga per empat bola (2.75). Untuk menang penuh <small class="text-muted">(100%)</small> taruhan <i>Over</i>, maka total skor yang dijumlahkan harus minimal 4. Sedangkan untuk taruhan <i>Under</i>, total skor tidak boleh lebih dari 2.</p>
											<div class="card">
												<div class="card-header">
													Panduan Taruhan Lebih / Diatas <small class="text-muted">(Over)</small>
												</div>
												<div class="card-body">
													<p>Pasaran : <span class="text-fur">2.5-3</span> atau fur 2.75 bola.</p>
													<ul>
														<li>Total Skor 0 sampai 2 : Kalah Taruhan.</li>
														<li>Total Skor 3 : Menang 50% Taruhan.</li>
														<li>Total Skor 4+ : Menang <i>Full</i> <small class="text-muted">(100%)</small>.</li>
													</ul>
													<small class="text-muted">Total Skor = Skor Tim A + Skor Tim B</small>
												</div>
											</div>
											<div class="card">
												<div class="card-header">
													Panduan Taruhan Kurang / Dibawah <small class="text-muted">(Under)</small>
												</div>
												<div class="card-body">
													<p>Pasaran : <span class="text-fur">2.5-3</span> atau fur 2.75 bola.</p>
													<ul>
														<li>Total Skor 0 sampai 2 : Menang <i>Full</i> <small class="text-muted">(100%)</small>.</li>
														<li>Total Skor 3 : Kalah 50% Taruhan.</li>
														<li>Total Skor 4+ : Kalah Taruhan.</li>
													</ul>
													<small class="text-muted">Total Skor = Skor Tim A + Skor Tim B</small>
												</div>
											</div>
										</dd>

										<dt class="col-sm-3 col-md-2">1X2</dt>
										<dd class="col-sm-9 col-md-10">Taruhan untuk menebak tim yang menang atau skor seri <small class="text-muted">(Draw)</small>.</dd>

										<dt class="col-sm-3 col-md-2">OE <small class="text-muted">(<i>Odd/Even</i>)</small></dt>
										<dd class="col-sm-9 col-md-10">Taruhan untuk menebak total skor pertandingan Tim A ditambah skor Tim B apakah hasilnya Ganjil <small class="text-muted">(<i>Odd</i>)</small> atau Genap <small class="text-muted">(<i>Even</i>).</small></dd>
									</dl>
									<p>Apabila kamu masih belum mengerti, kamu bisa lihat penjelasan lebih lengkap dan cara hitungnya di halaman lain yang sudah kami sediakan.</p>
								</div>
							</div>

							<div class="tab-pane fade" id="nav-hdp" role="tabpanel" aria-labelledby="nav-hdp-tab">
								<div class="content-guide">
									<figure>
										<img src="assets/images/soccer/tabel-hdp.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Tabel Handicap Judi Bola Online">
										<figcaption>Gambar 2.0 - Contoh Gambar Tabel Handicap</figcaption>
									</figure>
									<p><strong>Taruhan <i>Handicap</i></strong> merupakan taruhan yang ditentukan dari fur an atau ada salah satu tim yang lebih kuat. Untuk tim yang memberikan fur, biasanya nama tim nya pun berwarna merah. <i>Handicap</i> ini biasanya ditentukan dari <i>provider</i> tempat kamu bermain.</p>
									<h3>Cara Bermain Judi Bola Handicap</h3>
									<p>Untuk bermain judi bola handicap, kamu perlu tahu syarat - syarat untuk menang dan lain nya. Untuk bermain HDP, walaupun skor sudah 2-0 atau lainya tetap akan dianggap kamu mulai dari skor 0-0.</p>
									<p>Jika kamu memasang <span class="text-red">France</span>, minimal <span class="text-red">France</span> harus mencetak gol 3-0 untuk menang 100%. Sebaliknya, apabila kamu memasang <span class="text-blue">Australia</span> dan jika <span class="text-blue">Australia</span> bisa menahan <span class="text-red">France</span> dengan skor maksimal 1-0, kamu sudah menang taruhan kamu.</p>
									<p>Dalam taruhan HDP <small class="text-muted">(<i>Handicap</i>)</small> untuk skor 2-1 maka akan dianggap seperti skor 1-0. Kami akan memberikan contoh untuk pertandingan <span class="text-red">France</span> vs <span class="text-blue">Australia</span>.</p>
									<div class="card">
										<div class="card-header">
											Contoh Pertama : Kamu Taruhan <span class="text-red">France</span> - HDP <i>Full Time</i> ( Kotak Merah Pertama )
										</div>
										<div class="card-body">
											<ul>
												<li>Pasaran : <span class="text-fur">1.5-2</span> (1.75)</li>
												<li>Odds : <span class="text-red">-1.13</span></li>
											</ul>
											<p>Untuk pasaran fur 1.75, syarat untuk menang sepenuh nya atau 100% adalah minimal <small class="text-red">France</small> mempunyai skor lebih besar 3-0 bola atau lebih dari <span class="text-blue">Australia</span> dibawah ini.</p>
											<ul>
												<li>Skor <span class="text-red">France</span> 0 - 1 <span class="text-blue">Australia</span> : Kalah Taruhan.</li>
												<li>Skor <span class="text-red">France</span> 1 - 0 <span class="text-blue">Australia</span> : Kalah Taruhan.</li>
												<li>Skor <span class="text-red">France</span> 2 - 0 <span class="text-blue">Australia</span> : Hanya Menang 50% Taruhan.</li>
												<li>Skor <span class="text-red">France</span> 3 - 0 <span class="text-blue">Australia</span> : Menang <i>Full</i>.</li>
											</ul>
										</div>
									</div>
									<div class="card mg-bot-16">
										<div class="card-header">
											Contoh Kedua : Kamu Taruhan <span class="text-blue">Australia</span> - HDP <i>Full Time</i> ( Kotak Merah Pertama )
										</div>
										<div class="card-body">
											<ul>
												<li>Pasaran : <span class="text-fur">1.5-2</span> (1.75)</li>
												<li>Odds : 1.07</li>
											</ul>
											<p>Untuk pasaran fur 1.75, syarat untuk menang sepenuh nya atau 100% apabila <small class="text-red">France</small> hanya mempunyai skor 1-0 bola atau kurang dari <span class="text-blue">Australia</span>.</p>
											<ul>
												<li>Skor <span class="text-red">France</span> 0 - 1 <span class="text-blue">Australia</span> : Menang <i>Full</i>.</li>
												<li>Skor <span class="text-red">France</span> 0 - 0 <span class="text-blue">Australia</span> : Menang <i>Full</i>.</li>
												<li>Skor <span class="text-red">France</span> 1 - 0 <span class="text-blue">Australia</span> : Menang <i>Full</i>.</li>
												<li>Skor <span class="text-red">France</span> 2 - 0 <span class="text-blue">Australia</span> : Kalah 50% Taruhan.</li>
												<li>Skor <span class="text-red">France</span> 3 - 0 <span class="text-blue">Australia</span> : Kalah Taruhan.</li>
											</ul>
										</div>
									</div>
									<p>Untuk setiap pasaran atau fur an, maka skor penentu untuk kemenangan akan berbeda juga.</p>
									<h3>Cara Hitung Kemenangan Taruhan Handicap</h3>
									<p>Untuk setiap nilai Odds, ada angka yang <i>minus</i> dan ada angka yang <i>plus</i>. Berikut ini cara untuk menghitung nilai kemenangan taruhan kamu.</p>
									<p>Contoh apabila pasang taruhan 100.000 di setiap tim yang berbeda.</p>
									<div class="card">
										<div class="card-header">Taruhan <span class="text-red">France</span> - Odds <span class="text-red">-1.13</span></div>
										<div class="card-body">
											<dl>
												<dt>Menang = 1 x Nilai Taruhan</dt>
												<dd>
													= 1 x 100.000<br />
													= Menang 100.000
												</dd>

												<dt>Kalah = <span class="text-red">1.13</span> x Nilai Taruhan</dt>
												<dd>= 1.13 x 100.000<br />
													= Kalah 113.000
												</dd>
											</dl>
										</div>
									</div>
									<div class="card">
										<div class="card-header">Taruhan <span class="text-blue">Australia</span> - Odds 1.07</div>
										<div class="card-body">
											<dl>
												<dt>Menang = 1.07 x Nilai Taruhan</dt>
												<dd>
													= 1.07 x 100.000<br />
													= Menang 107.000
												</dd>

												<dt>Kalah = <span class="text-red">-1</span> x Nilai Taruhan</dt>
												<dd>
													= 1 x 100.000<br />
													= Kalah 100.000
												</dd>
											</dl>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="nav-ou" role="tabpanel" aria-labelledby="nav-ou-tab">
								<div class="content-guide">
									<figure>
										<img src="assets/images/soccer/tabel-ou2.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola, info judi bola" title="Tabel Over atau Under Judi Bola Online">
										<figcaption>Gambar 3.0 - Contoh Gambar Tabel Over / Under</figcaption>
									</figure>
									<p><strong>Taruhan OU (<i>Over/Under</i>)</strong> merupakan taruhan untuk menebak total skor Tim A ditambah dengan Tim B, ketika dijumlahkan apakah nilainya lebih <small class="text-muted">(Over)</small> atau kurang <small class="text-muted">(Under)</small> dari pasaran yang ada. Sebutan lain untuk <i>Over/Under</i> adalah atas/bawah.</p>
									<p>Contoh apabila hasil pertandingan <span class="text-red">France</span> vs <span class="text-blue">Australia</span> adalah 3-1, maka untuk total skor <span class="text-red">France</span> ditambah skor <span class="text-blue">Australia</span> adalah 3 + 1 = 4. Untuk hasil taruhan karena setiap pasaran berbeda, berikut ini kami berikan contoh untuk <i>Over</i> dan <i>Under</i> dalam 3 pasaran yang berbeda.</p>
									<div class="card mg-bot-16">
										<div class="card-header">
											Beberapa Contoh Taruhan <i>Over</i> atau skor lebih besar dari pasaran.
										</div>
										<div class="card-body">
											<dl>
												<dt>Pasaran <span class="text-fur">1-1.5</span> atau Pasaran <span class="text-fur">1.25</span></dt>
												<dd>
													<ul>
														<li>Total Skor 0 : Kalah Taruhan</li>
														<li>Total Skor 1 : Kalah 50% Taruhan</li>
														<li>Total Skor 2 atau lebih : Menang <i>Full</i></li>
													</ul>
												</dd>
											</dl>
											<dl>
												<dt>Pasaran <span class="text-fur">1.5</span></dt>
												<dd>
													<ul>
														<li>Total Skor 0 sampai 1 : Kalah Taruhan</li>
														<li>Total Skor 2 atau lebih : Menang <i>Full</i></li>
													</ul>
												</dd>
											</dl>
											<dl>
												<dt>Pasaran <span class="text-fur">2.5-3</span> atau Pasaran <span class="text-fur">2.75</span></dt>
												<dd>
													<ul>
														<li>Total Skor 0 sampai 2 : Kalah Taruhan</li>
														<li>Total Skor 3 : Menang 50% Taruhan</li>
														<li>Total Skor 4 atau lebih : Menang <i>Full</i></li>
													</ul>
												</dd>
											</dl>
										</div>
									</div>
									<p>Untuk taruhan <i>Under</i> biasanya adalah kebalikan dari taruhan <i>Over</i>, untuk lebih jelasnya kamu tetap bisa lihat contoh dibawah ini.</p>
									<div class="card mg-bot-16">
										<div class="card-header">
											Beberapa Contoh Taruhan <i>Under</i> atau skor kurang dari pasaran.
										</div>
										<div class="card-body">
											<dl>
												<dt>Pasaran <span class="text-fur">1-1.5</span> atau Pasaran <span class="text-fur">1.25</span></dt>
												<dd>
													<ul>
														<li>Total Skor 0 : Menang <i>Full</i></li>
														<li>Total Skor 1 : Menang 50% Taruhan</li>
														<li>Total Skor 2 atau lebih : Kalah Taruhan</li>
													</ul>
												</dd>
											</dl>
											<dl>
												<dt>Pasaran <span class="text-fur">1.5</span></dt>
												<dd>
													<ul>
														<li>Total Skor 0 sampai 1 : Menang <i>Full</i></li>
														<li>Total Skor 2 atau lebih : Kalah Taruhan</li>
													</ul>
												</dd>
											</dl>
											<dl>
												<dt>Pasaran <span class="text-fur">2.5-3</span> atau Pasaran <span class="text-fur">2.75</span></dt>
												<dd>
													<ul>
														<li>Total Skor 0 sampai 2 : Menang <i>Full</i></li>
														<li>Total Skor 3 : Kalah 50% Taruhan</li>
														<li>Total Skor 4 atau lebih : Kalah Taruhan</li>
													</ul>
												</dd>
											</dl>
										</div>
									</div>
									<p>Cara untuk menghitung taruhan <i>Over/Under</i> sama seperti dengan taruhan HDP <small class="text-muted">(Handicap)</small>.</p>
								</div>
							</div>

							<div class="tab-pane fade" id="nav-1x2" role="tabpanel" aria-labelledby="nav-1x2-tab">
								<div class="content-guide">
									<figure>
										<img src="assets/images/soccer/tabel-1x2-2.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola, info judi bola" title="Tabel 1X2 Judi Bola Online">
										<figcaption>Gambar 4.0 - Contoh Gambar Tabel 1X2</figcaption>
									</figure>
									<p><strong>Taruhan 1X2</strong> merupakan taruhan untuk menebak hasil pertandingan antara tim mana yang menang atau hasil pertandingan berakhir seri <small class="text-muted">(<i>Draw</i>)</small>. Untuk menentukan tim mana yang menang, sudah pasti tim tersebut harus memiliki minimal 1 angka lebih banyak dari lawan nya.</p>
									<h3>Cara Main Taruhan Bola 1X2</h3>
									<p>Untuk cara bermain taruhan 1X2 ini sangat sederhana, kamu tidak perlu melihat pasaran lagi. Untuk bermain 1X2 kamu hanya perlu menebak tim mana yang menang atau menebak apakah skor pertandingan akan berakhir seri atau <i>draw</i>.</p>
									<p>1X2 memiliki kepanjangan dan arti lain yang bisa kamu lihat dibawah ini</p>
									<ul>
										<li>1 <small class="text-muted">(<i>Home</i>)</small> : Bertaruh untuk Tuan Rumah (<span class="text-red">France</span>) yang menang.</li>
										<li>X : Bertaruh untuk hasil pertandingan seri <small class="text-muted">(<i>Draw</i>)</small>.</li>
										<li>2 <small class="text-muted">(<i>Away</i>)</small> : Bertaruh untuk Tim Tamu (<span class="text-blue">Australia</span>) yang menang.</li>
									</ul>
									<h3>Cara Menghitung Taruhan Judi 1X2</h3>
									<p>Untuk cara menghitung taruhan 1X2 sedikit berbeda dengan perhitungan HDP, <small class="text-muted">(<i>Over/Under</i>)</small> dan <small class="text-muted">(<i>Odd/Even</i>)</small>. Perhitungan <i>Odds</i> pada taruhan 1X2 harus dikurangi 1 terlebih dahulu sebelum dikali dengan Nilai Taruhan kamu.</p>
									<pre>Rumus 1X2 : (Odds - 1) x Nilai Taruhan</pre>
									<p>Berikut ini beberapa contoh perhitungan apabila kamu Menang pasang di <span class="text-red">France</span>, <span class="text-blue">Australia</span> dan <i>Draw</i> dengan nilai taruhan 1.000.000. Apabila kamu kalah, kamu hanya perlu membayar taruhan sesuai dengan taruhan yang kamu pasang yaitu 1.000.000</p>
									<div class="card">
										<div class="card-header">Hitung Menang Taruhan 1X2 <span class="text-red">France</span> - Odds 1.24</div>
										<div class="card-body">
											<p>
											= ( 1.24 - 1 ) x 1.000.000<br />
											= 0.24 x 1.000.000<br />
											= 240.000</p>
											Kamu Menang Taruhan 240.000
										</div>
									</div>
									<div class="card">
										<div class="card-header">Hitung Menang Taruhan 1X2 <span class="text-blue">Australia</span> - Odds 10.00</div>
										<div class="card-body">
											<p>
											= ( 10.00 - 1 ) x 1.000.000<br />
											= 9 x 1.000.000<br />
											= 9.000.000</p>
											Kamu Menang Taruhan 9.000.000
										</div>
									</div>
									<div class="card">
										<div class="card-header">Hitung Menang Taruhan 1X2 Draw - Odds 7.00</div>
										<div class="card-body">
											<p>
											= ( 7.00 - 1 ) x 1.000.000<br />
											= 6 x 1.000.000<br />
											= 6.000.000</p>
											Kamu Menang Taruhan 9.000.000
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="nav-oe" role="tabpanel" aria-labelledby="nav-oe-tab">
								<div class="content-guide">
									<figure>
										<img src="assets/images/soccer/tabel-odd-even.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola, info judi bola" title="Tabel Odd/Even Judi Bola Online">
										<figcaption>Gambar 5.0 - Contoh Gambar Tabel Odd / Even</figcaption>
									</figure>
									<p><strong>Taruhan <i>Odd/Even</i></strong> merupakan taruhan untuk total skor yang hampir sama dengan taruhan <i>Over/Under</i>, perbedaan nya untuk <i>Odd/Even</i> adalah menebak total skor Ganjil <small class="text-muted">(Odd)</small> atau Genap <small class="text-muted">(Even)</small>. Taruhan ini tidak ada pengaruh dari tim mana yang menang.</p>
									<p>Contoh apabila hasil pertandingan <span class="text-red">France</span> vs <span class="text-blue">Australia</span> adalah 3-1, maka untuk total skor <span class="text-red">France</span> ditambah skor <span class="text-blue">Australia</span> adalah 3 + 1 = 4, maka hasil total skor adalah Genap <small class="text-muted">(Even)</small>.</p>
									<ul>
										<li>Angka Ganjil <small class="text-muted">(Odd)</small> : 1, 3, 5, 7, 9, dan seterusnya.</li>
										<li>Angka Genap <small class="text-muted">(Even)</small> : 0, 2, 4, 6, 8, dan seterusnya.</li>
									</ul>
									<div class="card mg-bot-16">
										<div class="card-header">Total Skor = Skor <span class="text-red">France</span> + Skor <span class="text-blue">Australia</span></div>
										<div class="card-body">
											<ul>
												<li>Total Skor 0 : Genap</li>
												<li>Total Skor 1 : Ganjil</li>
												<li>Total Skor 2 : Genap</li>
												<li>Total Skor 3 : Ganjil</li>
												<li>Total Skor 4 : Genap</li>
												<li class="list-unstyled">dan seterusnya..</li>
											</ul>
										</div>
									</div>
									<p>Untuk cara perhitungan hasil taruhan sama seperti HDP, tidak ada perbedaan sama sekali.</p>
								</div>
							</div>

							<div class="tab-pane fade" id="nav-mp" role="tabpanel" aria-labelledby="nav-mp-tab">
								<div class="content-guide">
									<figure>
										<img src="assets/images/soccer/tabel-parlay.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola, info judi bola" title="Tabel Mix Parlay Judi Bola Online">
										<figcaption>Gambar 6.0 - Contoh Gambar Tabel Mix Parlay</figcaption>
									</figure>
									<p><strong>Taruhan <i>Mix Parlay</i></strong> merupakan taruhan untuk memasang beberapa pertandingan sekaligus menjadi 1 paket taruhan. Keuntungan nya adalah nilai <i>Odds</i> dari tim - tim yang bertanding akan menjadi lebih tinggi bisa mencapai hingga berkali - kali lipat tergantung jumlah pertandingan yang dipasang.</p>
									<h3>Cara Main Judi Bola Mix Parlay</h3>
									<p>Dalam bertaruh <i>Mix Parlay</i> kamu hanya bisa memilih salah satu jenis taruhan yang ada pada pertandingan tersebut. Apabila kamu sudah memasang taruhan pada HDP <i>Full Time</i> dan jika kamu klik taruhan lain seperti HDP yang ada pada <i>First Half</i>, maka taruhan kamu pada HDP <i>Full Time</i> dan akan dipindahkan ke HDP <i>First Half</i>.</p>
									<p>Untuk taruhan <i>Mix Parlay</i> ini berbeda dengan taruhan lain nya karena dalam 1 paket maka apabila ada 1 taruhan kamu yang kalah sepenuhnya dalam pertandingan, maka taruhan kamu sudah di anggap kalah. Sedangkan apabila hanya kalah 50% atau menang 50%, maka untuk perhitungan nilai taruhan akan dihitung ulang.</p>
									<h3>Cara Menghitung Taruhan <i>Mix Parlay</i></h3>
									<p>Untuk menghitung taruhan <i>Mix Parlay</i>, kami menggunakan contoh yang ada pada gambar 6.0 diatas.</p>
									<div class="card mg-bot-16">
										<div class="card-header">
											Paket Yang Dipasang Pada Mix Parlay
										</div>
										<div class="card-body">
											<p>Ada 3 partai yang di pasang pada paket <i>Mix Parlay</i> pada gambar diatas.</p>
											<ol>
												<li><span class="text-red">Sweden</span> vs <span class="text-blue">Korea Republic</span> - HDP ( <span class="text-red">Sweden</span> Fur <span class="text-fur">0.5</span> & <i>Odds</i> 2.09 )</li>
												<li><span class="text-red">Belgium</span> vs <span class="text-blue">Panama</span> - <i>Over</i> ( Pasaran <span class="text-fur">2.5-3</span> & <i>Odds</i> 1.89 )</li>
												<li><span class="text-red">England</span> vs <span class="text-blue">Tunisia</span> - <i>1X2</i> ( Taruhan <span class="text-red">England</span> Menang & <i>Odds</i> 1.44 )</li>
											</ol>
										</div>
									</div>
									<p>Ada 3 cara untuk menghitung <i>Mix Parlay</i></p>
									<ol>
										<li>Paket taruhan <i>Mix Parlay</i> menang sepenuhnya atau menang <i>full</i>.</i></li>
										<li>Paket taruhan <i>Mix Parlay</i>, ada tim yang menang 50%.</i></li>
										<li>Paket taruhan <i>Mix Parlay</i>, ada tim yang kalah 50%.</i></li>
									</ol>
									<h5>1. Cara Hitung Taruhan <i>Mix Parlay</i> Menang <i>Full</i></h5>
									<p>Untuk menghitung <i>Mix Parlay</i> dengan kemenangan sepenuhnya cukup mudah, kamu hanya perlu mengkali kan semua <i>Odds</i> yang kamu pasang, kemudian dikurangi dengan 1 lalu baru dikali dengan Nilai Taruhan kamu.</p>
									<pre>(( Odds Partai 1 x Semua Odds Partai Lain ) - 1 ) x Nilai Taruhan</pre>
									<p>Dengan menggunakan rumus yang ada diatas, kita akan menghitung total kemenangan <i>Mix Parlay</i> kamu dengan contoh taruhan 1.000.000</p>
									<div class="card mg-bot-16">
										<div class="card-body">
											<p>
											= (( Odds Partai 1 x Odds Partai 2 x Odds Partai 3 ) - 1 ) x Nilai Taruhan<br />
											= (( 2.09 x 1.89 x 1.44 ) - 1 ) x 1.000.000<br />
											= ( 5.688 - 1 ) x 1.000.000<br />
											= 4.688 x 1.000.000<br />
											= 4.688.000
											</p>
											Total kemenangan kamu sebesar 4.688.000
										</div>
									</div>
									<h5>2. Cara Hitung Taruhan <i>Mix Parlay</i> Menang 50%</h5>
									<p>Untuk menghitung <i>Mix Parlay</i> dengan ada tim yang menang setengah atau menang 50%, maka perhitungan nya berbeda dengan yang sebelumnya.</p>
									<p><i>Odds</i> awal tim yang menang 50% harus dikurangi dengan 1 terlebih dahulu kemudian dibagi 2 dan ditambahkan kembali dengan 1 sebelum dikali kan dengan partai lainnya.</p>
									<pre>Odds Partai Menang 50% = (( Odds Awal - 1 ) / 2 ) + 1</pre>
									<p>Jika kita anggap partai yang menang 50% adalah Partai 1 dengan <i>Odds</i> 2.09, maka berikut ini contoh perhitungan Odds Baru Partai 1</p>
									<div class="card mg-bot-16">
										<div class="card-body">
											<p>
												<i>Odds</i> Baru Partai 1 :<br />
												= (( Odds Awal Partai 1 - 1 ) / 2 ) + 1<br />
												= (( 2.09 - 1 ) / 2 ) + 1<br />
												= ( 1.09 / 2 ) + 1<br />
												= 0.545 + 1<br />
												= 1.545
											</p>
											Maka <i>Odds</i> baru partai 1 adalah 1.545
										</div>
									</div>
									<p>Kemudian dari hasil <i>Odds</i> baru diatas, barulah kita hitung kembali ke dalam perhitungan <i>Mix Parlay</i> yang sama seperti contoh nomor 1 dan nilai taruhan kamu adalah 1.000.000</p>
									<div class="card mg-bot-16">
										<div class="card-body">
											<p>
											= (( <b>Odds Baru Partai 1</b> x Odds Partai 2 x Odds Partai 3 ) - 1 ) x Nilai Taruhan<br />
											= (( 1.545 x 1.89 x 1.44 ) - 1 ) x 1.000.000<br />
											= ( 4.204 - 1 ) x 1.000.000<br />
											= 3.204 x 1.000.000<br />
											= 3.204.000
											</p>
											Total kemenangan kamu sebesar 3.204.000
										</div>
									</div>
									<h5>3. Cara Hitung Taruhan <i>Mix Parlay</i> Kalah 50%</h5>
									<p>Untuk menghitung taruhan <i>Mix Parlay</i> yang kalah 50% tentunya cara ini akan berbeda dari yang menang <i>full</i> dan menang 50%. Pada hitungan ini, <i>Odds</i> partai yang menang akan dibagi dengan <i>Odds</i> partai yang kalah 50% setelah <i>Odds</i> partai kalah 50% tersebut di kali dengan 2.</p>
									<p>Pertama, kita akan menghitung Odds Baru untuk Partai kalah 50%, rumusnya adalah</p>
									<pre>Odds Partai Kalah 50% = ( Odds Awal x 2 )</pre>
									<p>Kali ini kami akan memakai contoh apabila partai yang kalah tersebut adalah Partai 3 dengan <i>Odds</i> 1.44. Maka perhitungan untuk nilai <i>Odds</i> baru Partai 3 adalah</p>
									<div class="card mg-bot-16">
										<div class="card-body">
											<p>
											<i>Odds</i> Baru Partai 3 :<br />
											= Odds Awal Partai 3 x 2<br />
											= 1.44 x 2
											= 2.88
											</p>
											Maka <i>Odds</i> baru partai 3 adalah 2.88
										</div>
									</div>
									<p>Kali ini kita akan menghitung hasil kemenangan kita yang dimana ada salah satu partai dengan taruhan kalah 50%, berikut ini adalah rumus perhitungan nya.</p>
									<pre>(( Odds Partai Menang / Odds Baru Partai Kalah 50% ) - 1 ) x Nilai Taruhan</pre>
									<p>Maka jika dalam perhitungan nya adalah seperti berikut dengan <i>Odds</i> Baru Partai 3 adalah 2.88 dan Nilai Taruhan kamu adalah 1.000.000</p>
									<div class="card">
										<div class="card-body">
											<p>
											= ((( Odds Partai 1 x Odds Partai 2 ) / <b>Odds Baru Partai 3</b> ) - 1 ) x Nilai Taruhan<br />
											= ((( 1.545 x 1.89 ) / 2.88 ) - 1 ) x 1.000.000<br />
											= (( 3.95 / 2.88 ) - 1 ) x 1.000.000<br />
											= ( 1.37 - 1 ) x 1.000.000<br />
											= 0.37 x 1.000.000<br />
											= 370.000
											</p>
											Total kemenangan kamu sebesar 3.204.000
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="nav-cs" role="tabpanel" aria-labelledby="nav-cs-tab">
								<div class="content-guide">
									<figure>
										<img src="assets/images/soccer/tabel-correct-score.png" alt="cara main judi bola, prediksi skor, tebak skor, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Tabel Judi Bola Online Correct Score">
										<figcaption>Gambar 7.0 - Contoh Gambar Tabel Correct Score</figcaption>
									</figure>
									<p><strong>Taruhan <i>Correct Score</i></strong> merupakan taruhan tebak skor pertandingan dengan tepat <i>Full Time</i> atau hanya menebak skor pertandingan babak pertama saja di tabel <i>FH Correct Score</i>.</p>
									<h3>Cara Bermain <i>Correct Score</i> Judi bola</h3>
									<p>Untuk bermain <i>Correct Score</i> ini cukup mudah, skor yang kamu tebak harus tepat sesuai dengan akhir skor pertandingan. Untuk skor tersebut tidak bisa di bolak - balik, misal skor 0-1 akan tetap dianggap skor 0-1 bukan 1-0.</p>
									<p>Contoh apabila kita bermain sesuai dengan tebakan skor yang dipasang pada gambar 7.0 diatas, apabila kita anggap pertandingan ini adalah pertandingan antara <span class="text-red">Tim A</span> vs <span class="text-blue">Tim B</span> maka taruhan yang kita pasang adalah sebagai berikut</p>
									<div class="card">
										<div class="card-header">
											Taruhan <i>Correct Score Full Time</i> ( Kotak Pertama )
										</div>
										<div class="card-body">
											<p>Untuk contoh taruhan <i>Correct Score</i>, ada 3 skor yang ditebak dan ditandai dengan kotak merah pada gambar diatas.</p>
											<ol>
												<li><span class="text-red">Tim A</span> [ 1 : 0 ] <span class="text-blue">Tim B</span> ( <i>Odds</i> 4.7 )</li>
												<li><span class="text-red">Tim A</span> [ 3 : 2 ] <span class="text-blue">Tim B</span> ( <i>Odds</i> 46 )</li>
												<li><span class="text-red">Tim A</span> [ 4 : 3 ] <span class="text-blue">Tim B</span> ( <i>Odds</i> 220 )</li>
											</ol>
										</div>
									</div>
									<div class="card mg-bot-16">
										<div class="card-header">
											Taruhan <i>Correct Score First Half</i> ( Kotak Kedua )
										</div>
										<div class="card-body">
											<p>Untuk contoh taruhan <i>Correct Score</i>, ada 3 skor yang ditebak dan ditandai dengan kotak biru pada gambar diatas.</p>
											<ol>
												<li><span class="text-red">Tim A</span> [ 0 : 2 ] <span class="text-blue">Tim B</span> ( <i>Odds</i> 60 )</li>
												<li><span class="text-red">Tim A</span> [ 3 : 0 ] <span class="text-blue">Tim B</span> ( <i>Odds</i> 40 )</li>
												<li><span class="text-red">Tim A</span> [ 3 : 2 ] <span class="text-blue">Tim B</span> ( <i>Odds</i> 225 )</li>
											</ol>
										</div>
									</div>
									<h3>Cara Menghitung Judi Bola <i>Correct Score</i></h3>
									<p>Cara untuk menghitung taruhan <i>Correct Score</i> hampir sama dengan perhitungan taruhan 1X2 dimana nilai <i>Odds</i> akan dikurangi 1 terlebih dahulu sebelum dikali dengan Nilai Taruhan kamu. Berikut simulasi skor hasil pertandingan antara <span class="text-red">Tim A</span> vs <span class="text-blue">Tim B</span>.</p>
									<ul>
										<li>Babak Pertama (<i>First Half</i>) : <span class="text-red">Tim A</span> [ 0 : 2 ] <span class="text-blue">Tim B</span></li>
										<li>Pertandingan Penuh (<i>Full Time</i>) : <span class="text-red">Tim A</span> [ 3 : 2 ] <span class="text-blue">Tim B</span></li>
									</ul>
									<p>Contoh apabila hasil skor pertandingan seperti diatas dengan taruhan 1.000.000, maka perhitungan kemenangan taruhan kamu adalah</p>
									<div class="card">
										<div class="card-header">
											Hitungan Taruhan Babak Pertama (<i>First Half</i>) atau 45 menit pertama.
										</div>
										<div class="card-body">
											<p><i>Odds FH Correct Score</i> untuk skor 0-2 adalah 60</p>
											<p>
												= ( Odds - 1 ) x Nilai Taruhan<br />
												= ( 60 - 1 ) x 1.000.000<br />
												= 59 x 1.000.000<br />
												= 59.000.000
											</p>
											Total kemenangan kamu di tebak skor babak pertama adalah 59.000.000
										</div>
									</div>
									<div class="card mg-bot-16">
										<div class="card-header">
											Hitungan Taruhan Pertandingan Penuh (<i>First Half</i>) atau 90 menit pertandingan.
										</div>
										<div class="card-body">
											<p><i>Odds Correct Score</i> untuk skor 3-2 adalah 46</p>
											<p>
												= ( Odds - 1 ) x Nilai Taruhan<br />
												= ( 46 - 1 ) x 1.000.000<br />
												= 45 x 1.000.000<br />
												= 45.000.000
											</p>
											Total kemenangan kamu di tebak skor babak pertama adalah 45.000.000
										</div>
									</div>
									<p>Sesuai dengan taruhan diatas, apabila kamu pasang kedua tebakan skor tersebut maka total kemenangan kamu adalah 104.000.000 atau 104 juta</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection