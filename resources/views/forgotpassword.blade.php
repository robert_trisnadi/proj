@extends('layouts.app')
@section('contentdescription')
	<?php echo "Halaman lupa password dari ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya.";?>
@endsection
<?php 
use App\SecurityQuestions;
$SecurityQuestions = SecurityQuestions::where('Status',1)->get();
$SecurityQuestions = $SecurityQuestions->pluck('Question','ForgotQuestionsID');
?>

@section('content')	
<div id="site-content" class="site-content">	
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Lupa Password</h1>
				</header>				
				<div class="page-single-content">
				@if(count($errors)>0)	
					<div class="alert">
					@foreach($errors->all() as $error)		
						{{$error}}<?php echo '</br>';?>
					@endforeach
					</div>
				@endif
				<?php 
					$msg = Request::session()->pull('message', 'default');
					if($msg !='default')
						echo '<div class="success">'.$msg.'</div>';
					else
					{
						$msg = Request::session()->pull('err', 'default');
						if($msg !='default')
							echo '<div class="alert">'.$msg.'</div>';
					}
				?>	
				{!! Form::open(['url'=> '/forgot/kirim']) !!}
					<div class="user-form">
						<div class="form-text form-inline">
							<label for="user-forgot-username" class="label">Username</label>
							{{Form::text('username','',['id'=>"user-forgot-username",'class'=>'input', 'placeholder'=>'Username'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-forgot-question" class="label">Pertanyaan Lupa Password</label>
							{{Form::select('plupapassword',$SecurityQuestions,null,['id'=>'user-forgot-question','class'=>'input'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-forgot-answer" class="label">Jawaban Lupa Password</label>
							{{Form::text('jlupapassword','',['id'=>"user-forgot-answer",'class'=>'input', 'placeholder'=>'Jawaban lupa password'])}}    
						</div>
						<div class="form-text form-inline">
							<label for="user-forgot-password" class="label">Password Baru</label>
							{{Form::password('passwordbaru',['id'=>'user-forgot-password','class'=>'input'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-forgot-password-2" class="label">Konfirmasi Password Baru</label>
							{{Form::password('kpassword',['id'=>"user-forgot-password-2",'class'=>'input'])}}
						</div>
						<div class="form-submit form-inline">
							{{Form::submit('Ubah Password',['class'=>'button button-full'])}}
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection