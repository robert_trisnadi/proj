@extends('layouts.app')
@section('contentdescription')
	<?php echo "Berita seputar FIFA world cup dari ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya.";?>
@endsection
@section('content')
<?php
	$articles = DB::table('category')->where('Status',4)->get();
	$subc = DB::table('ArticleCategorySubView')
		->where('Status',4)
		->where(function($q)
		{
			$q->where('StatusCategory',4)
			  ->orWhere('StatusCategory',NULL);
		})
		->where(function($q)
		{
			$q->where('StatusSub',4)
			  ->orWhere('StatusSub',NULL);
		})
		->groupBy("SubcategoryID")
		->select("SubcategoryName","subcategoryid as SubcategoryID","CategoryID","StatusSub as Status")->get();

	if('-' == $categoryid)
	{
		$news = DB::table('ArticleCategorySubView')
			->where('Status',4)
			->where(function($q)
			{
				$q->where('StatusCategory',4)
				  ->orWhere('StatusCategory',NULL);
			})
			->where(function($q)
			{
				$q->where('StatusSub',4)
				  ->orWhere('StatusSub',NULL);
			})
			->orderBy('PublishDate', 'desc')			
			->take($imax)
			->get();
		echo '
			<script>
				document.title = "Semua Kategori - Berita | T4SPORT";
			</script>
		';
	}
	else
	{
		echo '
			<script>
				document.title = "Kategori '.str_replace("-"," ",$categoryid).' | T4SPORT";
			</script>
		';
		if('-' == $subcategoryid || null == $subcategoryid)
		{	

			$news = DB::table('ArticleCategorySubView')
				->orderBy('PublishDate', 'desc')
				->where('Status',4)
				->where(function($q)
				{
					$q->where('StatusCategory',4)
					  ->orWhere('StatusCategory',NULL);
				})
				->where(function($q)
				{
					$q->where('StatusSub',4)
					  ->orWhere('StatusSub',NULL);
				})
				->where('CategoryName',$categoryid)
				->take($imax)
				->get();				
		}
		else
		{			
			$news = DB::table('ArticleCategorySubView')
				->orderBy('PublishDate', 'desc')
				->where('Status',4)
				->where(function($q)
				{
					$q->where('StatusCategory',4)
					  ->orWhere('StatusCategory',NULL);
				})
				->where(function($q)
				{
					$q->where('StatusSub',4)
					  ->orWhere('StatusSub',NULL);
				})
				->where('CategoryName','=',$categoryid)
				->where('SubcategoryName','=',$subcategoryid)
				->take($imax)
				->get();
		}
		
	}		
?>
<div id="site-content" class="site-content" id="page-news-site-content">
	<div class="inner wrapper" id="page-news-inner-wrapper">
		<div class="main-content" id="page-news-main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container" id="page-news-container">
				<header class="page-single-header">
					<h1 class="heading">Berita</h1>
				</header>
				<div class="page-single-content" id="page-news-content">
					<div class="news-overviews" id="newsContainer">
						<?php
						$counter = 0;
						foreach ($news as $n) {							
							if($counter < $imax){
								$counter++;
						?>
							<div class="news-overview">
								<a href="{{route('detailberita', ['idd'=>str_replace(" ","-",$n->Title)])}}" class="news-overview-link">
									<div class="news-overview-thumbnail news-overview-thumbnail-custom">
										<!-- <img src={{URL::asset('../../poseidon-admin/uploads/')}}<?php //echo '/'.$n->Image;?>> -->										
										<img title="{{$n->TitleImage}}"  alt="{{$n->AlternateImage}}" src="http://{{ $_SERVER['HTTP_HOST'].'/uploads/' . "{$n->Image}"}}"/>
									</div>
									<div class="news-overview-detail">
										<p class="meta"><?php echo $n->PublishDate;?></p>
										<p class="categories"><span class="category"><?php echo $n->CategoryName;?></span></p>
										<h2 class="title"><?php echo $n->Title;?></h2>
										<p class="excerpt"><?php echo substr(strip_tags($n->Content),0,100).'...';?></p>
									</div>
								</a>
							</div>	
						<?php
							}
						}						
						?>
					</div>

					<div class="news-overviews-more" id="page-news-overviews-more">
					<?php
						if($counter == 0)
							echo '<h2>Belum ada berita di kategori ini.</h2>';
					?>
						<a id="loadmorenewsid" onclick="LoadMoreNews({{$counter}},'{{str_replace(' ','-',$categoryid)}}','{{str_replace(' ','-',$subcategoryid)}}','newsContainer','{{route('detailberita',['idd'=>'x'])}}','{{ env('BACKEND_URL')}}')" class="button button-gray">Load More</a>
					</div>
				</div>
			</div>
		</div>
		@include('include.newsfeed')
	</div>
</div>
@endsection