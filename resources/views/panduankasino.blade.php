@extends('layouts.app')
@section('contentdescription')
	<?php echo "Panduan bola di ".$_SERVER['HTTP_HOST']."! Website agen betting terpercaya.";?>	
@endsection
@section('content')
<link rel="stylesheet" href="assets/css/gamehelp.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<div id="site-content" class="site-content">
				<div class="inner wrapper">
					<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
						<div class="page-single-container">
							<header class="page-single-header">
								<h1 class="heading">Panduan Casino</h1>
							</header>

							<div class="page-single-content game-guide">
								<h2 class="title">Panduan Cara Main Judi Casino <?php echo $_SERVER['HTTP_HOST'];?></h2>
								<hr />
								<p>Disini kami akan memberikan panduan untuk cara bermain judi casino online, ada beberapa permainan dalam judi casino seperti <i>Baccarat</i>, <i>Black Jack</i>, <i>Roullete</i>, <i>Sicbo</i> (Dadu) dan lainnya.</p>

								<div class="web-game-guide">
									<nav>
										<div class="nav nav-tabs" id="nav-tab" role="tablist">
											<a class="nav-item nav-link active" id="nav-baccarat-tab" data-toggle="tab" href="#nav-baccarat" role="tab" aria-controls="nav-baccarat" aria-selected="true">Baccarat</a>
											<a class="nav-item nav-link" id="nav-blackjack-tab" data-toggle="tab" href="#nav-blackjack" role="tab" aria-controls="nav-blackjack" aria-selected="false">Black Jack</small></a>
											<a class="nav-item nav-link" id="nav-roullete-tab" data-toggle="tab" href="#nav-roullete" role="tab" aria-controls="nav-roullete" aria-selected="false">Roullete</a>
											<a class="nav-item nav-link" id="nav-sicbo-tab" data-toggle="tab" href="#nav-sicbo" role="tab" aria-controls="nav-sicbo" aria-selected="false">Sicbo (Dadu)</a>
										</div>
									</nav>
									<div class="tab-content" id="nav-tabContent">
										<div class="tab-pane fade show active" id="nav-baccarat" role="tabpanel" aria-labelledby="nav-baccarat-tab">
											<div class="content-guide">
												<figure>
													<img src="assets/images/casino/baccarat.jpg" alt="cara main baccarat, judi baccarat, panduan baccarat, belajar judi baccarat, situs casino, casino online, player, banker, judi banker player, judi player banker" title="Contoh Gambar Meja Baccarat">
													<figcaption>Gambar 1.0 - Contoh Gambar Meja Baccarat</figcaption>
												</figure>
												<p><strong>Baccarat</strong> adalah permainan kartu untuk membandingkan kartu antara sisi <i>Player</i> dan sisi <i>Banker</i>, tujuan nya adalah untuk menebak sisi mana yang mempunyai kartu lebih besar. Kamu juga bisa memilih untuk bermain taruhan <i>Tie</i> atau seri.</p>
												<h3>Cara Bermain Baccarat Casino Online</h3>
												<p>Kamu akan diberikan waktu untuk memilih sisi mana yang akan kamu pasang sebelum pembagian kartu dimulai, setelah waktu tersebut habis kamu tidak dapat menambah taruhan kamu lagi. Bandar akan membagikan masing - masing 2 kartu untuk <i>Player</i> dan <i>Banker</i>. Maksimal kartu yang dibagikan oleh bandar untuk sisi <i>Player</i> atau <i>Banker</i> adalah 3 kartu tergantung dengan besaran nilai kartu tersebut. Pemain dengan taruhan terbesar bisa mendapatkan kesempatan untuk membuka kartu sisi yang dipasang nya.</p>
												<figure>
													<img src="assets/images/casino/kartu-baccarat.png" alt="cara main baccarat, judi baccarat, panduan baccarat, belajar judi baccarat, situs casino, casino online, player, banker, judi banker player, judi player banker" title="Contoh Gambar Perhitungan Kartu di Baccarat">
													<figcaption>Gambar 1.1 - Contoh Gambar Perhitungan Kartu di Baccarat</figcaption>
												</figure>
												<p>Untuk kartu pada <i>Baccarat</i>, nilai kartu yang paling kecil adalah 1 dan yang paling besar adalah 9 sesuai pada gambar diatas. Untuk 2 atau 3 kartu yang dijumlahkan apabila bernilai 10, maka nilai kartu tersebut adalah 0. Apabila kartu yang dijumlahkan nilai nya lebih dari 10, maka kartu tersebut akan dikurangi dengan nilai 10. Berikut ini contoh perhitungan kartu baccarat.</p>
												<p>Perlu diperhatikan lagi, dalam permainan <i>Baccarat</i> apabila jumlah nilai pada 2 kartu milik <i>Player</i> atau <i>Banker</i> ketika dijumlahkan mencapai angka 8 ataupun 9, maka tidak ada penambahan kartu ke 3. Apabila kartu <i>Player</i> dan <i>Banker</i> sama - sama bernilai 8 atau 9 maka dianggap <i>Tie</i> atau Seri. Jika tidak, maka sisi yang memiliki kartu 8 atau 9 lah yang akan dianggap menang atau biasa disebut dengan <i>Natural Win</i> atau Menang Murni karena tidak ada kartu tambahan.</p>
												<p>Untuk penambahan kartu ke 3, ada peraturan untuk <i>Player</i> dan <i>Banker</i> yang peraturan nya berbeda.</p>
												<h5>1. Peraturan Penambahan Kartu ke 3 Untuk <i>Player</i></h5>
												<p>Untuk penambahan kartu ke 3 pada <i>Player</i> hanya ada 1 peraturan yang harus dilihat, yaitu</p>
												<div class="card mg-bot-16">
													<div class="card-body">
														Jumlah total 2 kartu <i>Player</i> sama dengan 5 atau kurang dari angka 5
													</div>
												</div>
												<h5>2. Peraturan Penambahan Kartu ke 3 Untuk <i>Banker</i></h5>
												<figure>
													<img src="assets/images/casino/tabel-baccarat.png" alt="cara main baccarat, judi baccarat, panduan baccarat, belajar judi baccarat, situs casino, casino online, player, banker, judi banker player, judi player banker" title="Contoh Gambar Tabel Syarat Kartu Ke 3 Banker di Baccarat">
													<figcaption>Gambar 1.2 - Contoh Gambar Tabel Syarat Kartu Ke 3 Banker di Baccarat</figcaption>
												</figure>
												<p>Untuk penambahan kartu ke 3 pada <i>Banker</i>, ada beberapa syarat yang harus diperhatikan. Seperti pada gambar di atas, untuk H artinya <i>Hit</i> maka <i>Banker</i> akan mendapatkan kartu ke 3 sesuai dengan syarat kartu ke 3 <i>Player</i>. Sedangkan untuk S arti nya <i>Stand</i>, artinya <i>Banker</i> tidak akan mendapatkan kartu ke 3.</p>
												<p>Apabila masih bingung, kamu bisa melihat syarat dibawah ini.</p>
												<div class="card mg-bot-16">
													<div class="card-body">
														<p>Syarat ini berlaku hanya dihitung dari total 2 kartu <i>Banker</i>, untuk contoh kita sebut total 2 kartu banker sebagai Total <i>Banker</i>.</p>
														<ol>
															<li>Total <i>Banker</i> hanya mencapai 0 sampai 2.</li>
															<li>Total <i>Banker</i> adalah 3 <b>&</b> Kartu ke 3 <i>Player</i> bukan angka 8.</li>
															<li>Total <i>Banker</i> adalah 4 <b>&</b> Kartu ke 3 <i>Player</i> bukan angka 0, 1, 8 dan 9.</li>
															<li>Total <i>Banker</i> adalah 5 <b>&</b> Kartu ke 3 <i>Player</i> bukan angka 4, 5, 6 dan 7.</li>
															<li>Total <i>Banker</i> adalah 6 <b>&</b> Kartu ke 3 <i>Player</i> bukan angka 6 dan 7.</li>
														</ol>
													</div>
												</div>
												<p>Ingat, untuk total angka 8 atau 9 tidak ada pembagian kartu ke 3 lagi. Sehingga diluar peraturan yang sudah dijelaskan diatas maka tidak ada lagi penambahan kartu yang diberikan.</p>
											</div>
										</div>

										<div class="tab-pane fade" id="nav-blackjack" role="tabpanel" aria-labelledby="nav-blackjack-tab">
											<div class="content-guide">
												<figure>
													<img src="assets/images/casino/blackjack.jpg" alt="cara main blackjack, judi blackjack, panduan blackjack, belajar judi blackjack, situs casino, casino online, player, banker, judi banker player, judi player banker" title="Contoh Gambar Meja Baccarat">
													<figcaption>Gambar 2.0 - Contoh Gambar Black Jack</figcaption>
												</figure>
												<p><strong>Black Jack</strong> merupakan permainan kartu untuk melawan Bandar, permainan ini dimulai dari 2 kartu yang dibagikan oleh Bandar dimana salah 1 kartu akan langsung dibuka terlebih dahulu dan 1 kartu lain nya dibagikan tertutup.</p>
												<h3>Cara Main Casino Black Jack</h3>
												<p>Untuk bermain <i>Casino Black Jack</i> dan memenangkan taruhan <i>Black Jack</i> maka kamu harus mempunyai kartu lebih besar dari Bandar maksimal nilai total kartu adalah 21. Apabila nilai kartu kamu atau bandar lebih dari 21 maka dianggap <i>Bust</i> atau kalah.</p>
												<p>Apabila 2 kartu kamu yang pertama dibagikan oleh Bandar bernilai 21 biasanya disebut juga dengan <i>Black Jack</i>. Nilai taruhan kamu pun akan dibayar 1.5x dari taruhan yang kamu pasang, tapi apabila 2 kartu Bandar juga bernilai 21 atau <i>Black Jack</i> maka Bandar dianggap menang.</p>
												<p>Berikut ini nilai - nilai kartu yang ada di Black Jack</p>
												<figure>
													<img src="assets/images/casino/kartu-blackjack.jpg" alt="cara main blackjack, judi blackjack, panduan blackjack, belajar judi blackjack, situs casino, casino online, player, banker, judi banker player, judi player banker" title="Contoh Gambar Meja Baccarat">
													<figcaption>Gambar 2.1 - Contoh Gambar Kartu Black Jack</figcaption>
												</figure>
												<div class="card mg-bot-16">
													<div class="card-body">
														<ul class="no-margin">
															<li><span class="text-fur">2 - 9</span> : Sesuai dengan angka pada kartu.</li>
															<li><span class="text-fur">10, J, Q, K</span> : Nilai kartu 10.</li>
															<li><span class="text-fur">AS <small class="text-muted">(Ace)</small></span> : Nilai kartu 1 atau 11.</li>
														</ul>
													</div>
												</div>
												<p>Untuk kartu <span class="text-fur">AS</span> merupakan kartu spesial karena ada 2 nilai berbeda yang diberikan. Untuk mendapatkan <i>Black Jack</i>, maka pemain harus mempunyai 1 buah kartu bernilai 10 & 1 buah kartu AS.</p>
												<p>Kasus kedua, apabila jumlah 2 kartu kamu sudah mencapai angka 12 dan kartu ke 3 kamu adalah kartu AS. Maka kartu AS tersebut akan dianggap bernilai 1 agar tidak <i>Bust</i>. Jadi total kartu kamu adalah 13 sehingga kamu tetap dapat menambah kartu lagi.</p>
												<h3>Istilah - Istilah Dalam Permainan Black Jack</h3>
												<p>Setelah mengetahui cara untuk bermain <i>Black Jack</i>, ada istilah - istilah dalam <i>Black Jack</i> yang harus kamu ketahui.</p>
												<dl class="row">
													<dt class="col-sm-3 col-md-2">Black Jack</dt>
													<dd class="col-sm-9 col-md-10">
														Istilah lain dari total kartu 21 dengan jumlah dari hasil 2 kartu saja. Seperti yang dijelaskan untuk mendapatkan <i>Black Jack</i> maka harus mempunyai 2 kartu dengan nilai 10 dan AS. Apabila sudah ada pemain yang mendapatkan kartu <i>Black Jack</i> maka Bandar tidak akan membagikan kartu lagi dan menganggap permainan sudah berakhir. Kecuali ada pemain lain yang mendapatkan <i>Black Jack</i>, maka akan dianggap Seri.
													</dd>

													<dt class="col-sm-3 col-md-2">Bust</dt>
													<dd class="col-sm-9 col-md-10">Istilah yang digunakan apabila ada yang mendapatkan kartu dengan total lebih dari 21, maka pemain ataupun Bandar yang mendapatkan total nilai kartu lebih dari 21 akan dianggap kalah.</dd>

													<dt class="col-sm-3 col-md-2">Double</dt>
													<dd class="col-sm-9 col-md-10">Istilah untuk menggandakan taruhan awal menjadi 2x lipat dengan hanya menarik 1 buah kartu tambahan, untuk kasus ini maka pemain lain tidak diberikan kesempatan untuk menarik lebih dari 1 buah kartu tambahan. Pilihan <i>Double</i> akan muncul apabila jumlah total 2 kartu pemain adalah angka 11.</dd>

													<dt class="col-sm-3 col-md-2">Hit</dt>
													<dd class="col-sm-9 col-md-10">Istilah untuk menarik kartu tambahan.</dd>

													<dt class="col-sm-3 col-md-2">Stand</dt>
													<dd class="col-sm-9 col-md-10">Istilah untuk menahan kartu atau tidak minta kartu tambahan.</dd>

													<dt class="col-sm-3 col-md-2">Insurance</dt>
													<dd class="col-sm-9 col-md-10">Istilah untuk mengasuransikan taruhan dan hal ini berlaku jika kartu pertama Bandar adalah AS, maka kamu boleh membeli asuransi dengan nilai 50% taruhan kamu apabila kamu yakin Bandar akan mendapatkan <i>Black Jack</i>. Apabila Bandar mendapatkan <i>Black Jack</i>, maka kamu hanya akan kalah 50% taruhan kamu. Sebaliknya jika Bandar tidak mendapatkan <i>Black Jack</i>, maka kamu kehilangan nilai asuransi 50% tersebut dan permainan akan dilanjutkan.</dd>

													<dt class="col-sm-3 col-md-2">Split</dt>
													<dd class="col-sm-9 col-md-10">Istilah untuk memisahkan kartu menjadi 2 pasang taruhan, syaratnya adalah kartu tersebut harus memiliki nilai yang sama atau biasa disebut dengan kartu <i>Pair</i> atau kartu kembar. Pada <i>Split</i>, kartu AS hanya akan dihitung dengan nilai 10 saja sehingga kamu tidak bisa mendapatkan 2 pasang <i>Black Jack</i> murni dengan 2 kartu. Maksimal nilai kartu yang bisa kamu dapatkan adalah 20. Kemudian apabila kartu yang dipisahkan adalah kartu AS, maka kamu hanya bisa menambah 1 kartu untuk setiap kartu AS yang dipisahkan.</dd>

													<dt class="col-sm-3 col-md-2">Surrender</dt>
													<dd class="col-sm-9 col-md-10">Pada permainan <i>Black Jack</i>, kamu juga bisa menentukan untuk menyerah sebelum bermain atau apabila kamu yakin kartu Bandar akan keluar <i>Black Jack</i>. Apabila kamu menyerah, maka kamu akan kehilangan 50% dari nilai taruhan kamu. Untuk pilihan menyerah ini hanya dapat dilakukan jika kartu terbuka Bandar bernilai 9 sampai AS dan kamu hanya memiliki kartu terbuka 5 sampai 7 atau total 2 buah kartu 12 - 16. Saat Bandar mempunyai kartu terbuka AS, maka dia akan langsung membuka kartu tertutup nya untuk melihat apakah Bandar mendapatkan <i>Black Jack</i> atau tidak.</dd>
												</dl>
											</div>
										</div>

										<div class="tab-pane fade" id="nav-roullete" role="tabpanel" aria-labelledby="nav-roullete-tab">
											<div class="content-guide">
												<figure>
													<img src="assets/images/casino/roullete.jpg" alt="cara main roullete, banker, player, tie, belajar judi roullete, situs judi casino, casino online" title="Panduan Cara Main Roullete">
													<figcaption>Gambar 3.0 - Gambar Simulasi Contoh Meja Roullete</figcaption>
												</figure>
												<p><strong>Roullete</strong> merupakan salah satu jenis permainan Casino yang mengutamakan perhitungan dan intinya tidak terlepas dari keberuntungan anda. Permainan ini menggunakan sebuah bola besi kecil yang di putas dalam meja yang sudah memiliki tempat sandaran untuk bola tersebut berhenti yang terdiri dari warna dan angka.</p>
												<h3>Cara Main Casino Online Roullete</h3>
												<p>Cara untuk bermain roullete juga cukup mudah, kamu cukup menebak angka, warna, besar atau kecil, genap atau ganjil.</p>
												<dl class="row">
													<dt class="col-sm-3 col-md-2">Warna</dt>
													<dd class="col-sm-9 col-md-10">
														Untuk warna yang ada di putaran Roullete ada 3 yaitu Merah, Hitam dan Hijau. Untuk perhitungan pembayaran warna Merah dan Hitam adalah 1 : 1, sedangkan untuk pembayaran warna Hijau adalah 1 : 35.
													</dd>

													<dt class="col-sm-3 col-md-2">Angka</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak bola akan berada di angka yang ditebak. Untuk tebak angka sama seperti warna, bayaran nya 1 : 35 apabila menang.
													</dd>

													<dt class="col-sm-3 col-md-2">Odd <small class="text-muted">(Ganjil)</small></dt>
													<dd class="col-sm-9 col-md-10">Taruhan untuk menebak posisi bola akan berada di Angka Ganjil.<br />
														Angka Ganjil : 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35.
													</dd>

													<dt class="col-sm-3 col-md-2">Even <small class="text-muted">(Genap)</small></dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak posisi bola akan berada di Angka Genap.<br />
														Angka Genap : 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36.
													</dd>

													<dt class="col-sm-3 col-md-2">Kecil</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak posisi bola akan berada di Angka Kecil.<br />
														Angka Kecil : 1 - 18.
													</dd>

													<dt class="col-sm-3 col-md-2">Besar</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak posisi bola akan berada di Angka Besar.<br />
														Angka Kecil : 19 - 36.
													</dd>

													<dt class="col-sm-3 col-md-2">1st 12</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak posisi bola akan berada di antara angka 1 sampai 12.<br />
														Untuk 1st 12 akan mendapatkan bayaran 1 : 2 apabila menang.
													</dd>

													<dt class="col-sm-3 col-md-2">2nd 12</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak posisi bola akan berada di antara angka 13 sampai 24.<br />
														Untuk 2nd 12 akan mendapatkan bayaran 1 : 2 apabila menang.
													</dd>

													<dt class="col-sm-3 col-md-2">3rd 12</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak posisi bola akan berada di antara angka 25 sampai 36.<br />
														Untuk 3rd 12 akan mendapatkan bayaran 1 : 2 apabila menang.
													</dd>

													<dt class="col-sm-3 col-md-2">2 to 1</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan baris, di samping setiap baris terdapat 2 to 1 yang digunakan untuk menebak posisi bola akan berada di antara angka yang ada di baris tersebut. Untuk 2 to 1 akan mendapatkan bayaran 1 : 2 apabila menang.
													</dd>
												</dl>
											</div>
										</div>

										<div class="tab-pane fade show" id="nav-sicbo" role="tabpanel" aria-labelledby="nav-sicbo-tab">
											<div class="content-guide">
												<figure>
													<img src="assets/images/casino/sicbo.jpg" alt="cara main sicbo, banker, player, tie, belajar judi sicbo, situs judi casino, casino online" title="Panduan Cara Main Sicbo">
													<figcaption>Gambar 4.0 - Gambar Simulasi Contoh Dadu Sicbo</figcaption>
												</figure>
												<p><strong>Sicbo</strong> merupakan permainan <i>casino</i> yang menggunakan 3 buah dadu untuk bertaruh. Cara untuk bertaruh <i>Sicbo</i> ini hampir sama dengan <i>Roullete</i> yang membedakan hanya alat untuk bermain dan tidak ada warna dalam <i>Sicbo</i>.</p>
												<dl class="row">
													<dt class="col-sm-3 col-md-2">Ganjil / Genap</dt>
													<dd class="col-sm-9 col-md-10">
														Sesuai dengan taruhan lainnya, taruhan ini untuk menebak jumlah total 3 mata dadu apakah bernilai ganjil atau pun genap.
													</dd>

													<dt class="col-sm-3 col-md-2">Total 3 Dadu</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak jumlah nilai total 3 mata dadu ( Mulai dari 4 hingga 17 ).
													</dd>

													<dt class="col-sm-3 col-md-2">Tunggal</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak angka yang muncul pada mata dadu antara 1 sampai 6, jika angka yang dipilih tidak muncul di salah satu dari 3 dadu maka pemain di anggap kalah. Jika angka yang dipilih muncul, maka bayaran yang di dapat sesuai dengan seberapa sering angka yang dipilih muncul di mata dadu (1x maka 1:1, 2x maka 1:2 dan 3x maka 1:3).
													</dd>

													<dt class="col-sm-3 col-md-2">Ganda</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak minimal 2 buah dadu yang akan muncul memiliki nilai yang sama.<br />
														Misal Dadu : 1-1, 2-2, 3-3, 4-4, 5-5, 6-6
													</dd>

													<dt class="col-sm-3 col-md-2">Triple</dt>
													<dd class="col-sm-9 col-md-10">
														Taruhan untuk menebak 3 buah dadu atau semua dadu yang akan muncul memiliki nilai yang sama<br />
														Misal Dadu : 1-1-1, 2-2-2, 3-3-3, 4-4-4, 5-5-5, 6-6-6
													</dd>
												</dl>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection