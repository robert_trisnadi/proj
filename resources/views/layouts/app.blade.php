<!doctype html>
<html class="no-js" lang="id">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="description" content="@yield('contentdescription')">
		<meta name="keywords" content="t4sport.info, t4sportsbet, betting, agen betting, casino, promo, sportsbook, sabung ayam, cock fight, bola tangkas, agen judi, agen judi bola">
		<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-63117727-9"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-63117727-9');
		</script>
		<meta name="google-site-verification" content="72Saltoi9xPCr6TlxN1JF7sECoh2Dtc4RQaNUXFucWU" />
		<title>
			<?php
				if(Request::is('/'))
				{
					echo 'T4SPORT - Situs Judi Bola & Casino Online Terpercaya';
				}
				else if(Request::is('panduangame'))
				{
					echo "Panduan Main Judi Bola Online - T4SPORT";
				}
				else if(Request::is('panduanbola'))
				{
					echo "Panduan Main Judi Bola Online - T4SPORT";
				}
				else if(Request::is('panduankasino'))
				{
					echo "Panduan Main Judi Kasino Online - T4SPORT";
				}
				else if(Request::is('panduan'))
				{
					echo 'Panduan - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('faq'))
				{
					echo 'FAQ - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('peraturan'))
				{
					echo 'Peraturan - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('contact'))
				{					
					echo 'Hubungi Kami - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('forgot'))
				{
					echo 'Lupa Password - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('registration'))
				{
					echo 'Daftar Member - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('promo'))
				{
					echo 'Promo - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('jadwal'))
				{	
					echo 'Jadwal - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('livescore'))
				{
					echo 'Live Score - T4SPORT | Situs Judi Bola Online Terpercaya';
				}
				else if(Request::is('newss'))
				{
					echo 'Berita - T4SPORT | Situs Judi Bola Online Terpercaya';
				}				
				else if(Request::is('profil'))
				{
					echo 'Profile - T4SPORT';
				}
				else if(Request::is('daftargame'))
				{
					echo 'Daftar Game - T4SPORT';
				}
				else if(Request::is('deposit'))
				{
					echo 'Deposit - T4SPORT';					
				}
				else if(Request::is('withdraw'))
				{
					echo 'Withdraw - T4SPORT';
				}
				else if(Request::is('transfer'))
				{
					echo 'Transfer Dana - T4SPORT';
				}
				else if(Request::is('transaksi'))
				{
					echo 'Transaksi - T4SPORT';
				}
				else if(Request::is('referalbonus'))
				{
					echo 'Bonus Referral - T4SPORT';
				}
				else if(Request::is('firstdeposit'))
				{
					echo 'Bonus First Deposit - T4SPORT';
				}
				else if(Request::is('cashbackbonus'))
				{
					echo 'Bonus Cashback - T4SPORT';
				}

			?>


		</title>
		<link rel="author" href="humans.txt" />		
		<link rel="stylesheet" href={{ asset("assets/css/style.css") }}>		
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" rel="stylesheet">

		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<!-- <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"> -->
		<link rel="icon" type="image/png" sizes="16x16" href="/t4-favicon.png">
		<link rel="manifest" href="/site.webmanifest">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#28c73d">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">		
		<meta name="msapplication-TileColor" content="#28c73d">
		<meta name="theme-color" content="#ffffff">

		<script src="{{ URL::asset('assets/js/modernizr.js') }}"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script>if (!window.jQuery) { document.write('<script src="jquery.min.js" async><\/script>'); }</script>
		<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>	
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>		
		<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "BreadcrumbList",
		  "itemListElement": [{
		    "@type": "ListItem",
		    "position": 1,
		    "item": {
		      "@id": "http://t4sport.info/home",
		      "name": "Home",
		      "description":"Halaman awal dari T4Sport."
		    }
		  },{
		    "@type": "ListItem",
		    "position": 2,
		    "item": {
		      "@id": "http://t4sport.info/registration",
		      "name": "Registration",
		      "description":"Registrasi akun untuk T4Sport."
		    }
		  },{
		    "@type": "ListItem",
		    "position": 3,
		    "item": {
		      "@id": "http://t4sport.info/jadwal",
		      "name": "Jadwal",
		      "description":"Jadwal sepak bola di website T4Sport."
		    }
		  },{
		    "@type": "ListItem",
		    "position": 4,
		    "item": {
		      "@id": "http://t4sport.info/score",
		      "name": "Live Score",
		      "description":"Live score sepak bola di website T4Sport."
		    }
		  }]
		}
		</script>
	</head>

	<body>
	<?php 
	if(Request::session()->get('id','default')=='default'){
	?>
		@include('include.navbar')
	<?php 
	}
	else { 

		try {
				$msgpopup = DB::table("page")->where('MessagePopUpStatus',4)->first();
				if($msgpopup != null)
				{				
					$msgpopup = $msgpopup->MessagePopUp;
					if(!isset(Request::session()->get('id')->msgpopup))
					{
						//if(Request::session()->get('id')->Username == 'usertest1')
						//{
					?>
						<div id="bgmsgpopup" style="position:fixed;width:100%;height:100%;background-color:rgba(0,0,0,0.5);z-index:10;display:inline-block;">
							<div id="msgpopup" style="margin:0 auto;margin-top:10%;text-align:center;z-index:11;min-width:200px;max-width:800px;min-height:200px;border:10px solid #467dd6;border-radius:5px 5px 5px 5px;background-color:white;opacity:1.0;overflow:scroll;max-height:400px;">
								<div style="width:auto;text-align:right;padding:10px;">
								<span style="border:#4859a5 1px solid;cursor:pointer" onclick="closemsgpopup()">X</span>
								</div>
								<div style="color:black;"><?php echo $msgpopup;?></div>							
							</div>
						</div>
					<?php
						//}
					}
				}
			} catch (Exception $e) {
				echo '<script>console.log("'.$e->getMessage().'");</script>';
			}

		?>

		@include('include.navbar_login')		
	<?php } ?>
		<div id="site-container" class="site-container">
			<?php
			if(Request::session()->get('id','default')!='default'){
			?>
				@include('include.memo')
			<?php }
			$err = Request::session()->pull('errlogin', 'default');
			if($err != 'default')
			{        
				echo '<div class="alert" id="success-home">'.$err.'</div>';
			}
			?>
			<div class="contact-container hidden-xs">
				<ul class="contact-float">
					<li id="csWA"><a href="#">+639662697620</a></li>
					<li id="csBBM"><a href="#">E34A08E6</a></li>
					<li id="csLine"><a href="#">t4sport</a></li>
					<li id="csLC"><a href="#" onclick="parent.LC_API.open_chat_window({source:'minimized'})">LiveChat</a></li>
					<li id="csEmail"><a href="mailto:cst4dsport@gmail.com">cst4dsport@gmail.com</a></li>
				</ul>		
			</div>
			@yield('content')			
			<footer id="site-footer" class="site-footer" role="contentinfo">
				<div class="inner wrapper">
					<!-- Start of LiveChat (www.livechatinc.com) code -->
					<script type="text/javascript">
					window.__lc = window.__lc || {};
					window.__lc.license = 9835595;
					(function() {
					  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
					  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
					  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
					})();
					</script>
					<!-- End of LiveChat code -->
					<div class="site-footer-brand">
						<h4 class="title">Tentang Kami</h4>
						<ul class="benefits">
							<li class="benefit"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-bonus')}} />
							<title>Beragam Bonus Menarik</title>
							<description>Beragam bonus menarik di website <?php echo $_SERVER["HTTP_HOST"];?>.</description>
							</svg> Beragam Bonus Menarik</li>
							<li class="benefit"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-24')}} />
							<title>Customer Service 24 Jam</title>
							<description><?php echo $_SERVER["HTTP_HOST"];?> memberikan customer service selama 24 jam.</description>
							</svg> Customer Service 24 Jam</li>
							<li class="benefit"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-mobile')}} />
							<title>Akses Mobile</title>
							<description><?php echo $_SERVER["HTTP_HOST"];?> dapat dibuka melalui ponsel anda dengan user interface yang bersahabat.</description>
							</svg> Akses Mobile</li>
						</ul>
						<p class="about"><?php echo $_SERVER["HTTP_HOST"];?> adalah <b><a href="http://<?php echo $_SERVER["HTTP_HOST"];?>" target="_blank">Situs Agen Bola Terpercaya</a></b> yang menyediakan sarana terbaik dan terbesar untuk judi Bola, Casino, Poker, Sabung Ayam, Bola Tangkas dan Togel Online. Kami memiliki mitra yang sudah sangat terkenal di kawasan Asia Tenggara seperti SBOBET, MAXBET/IBCBET, 368BET, TOGEL4D dan lainnya dengan dukungan sistem pengamanan data kami yang menjamin keamanan dan kerahasiaan data member kami. Untuk anda yang sudah berusia lebih dari 18 tahun, Segeralah bergabung dengan <?php echo $_SERVER["HTTP_HOST"];?></p>
					</div>

					<div class="site-footer-addon">
						<ul class="badges">
							<li class="badge"><img src={{URL::asset('assets/images/badge-responsible.svg')}} alt="Responsible Gambling"></li>
							<li class="badge"><img src={{URL::asset('assets/images/badge-age.svg')}} alt="18+"></li>
						</ul>

						<p class="copyright">Copyright &copy; <script>document.write(new Date().getFullYear());</script> <?php echo $_SERVER["HTTP_HOST"];?>. All rights reserved.</p>
					</div>

				</div>
			</footer>
		</div>		
		<script src="{{ URL::asset('assets/js/vendor.js') }}"></script>
		<script src="{{ URL::asset('assets/js/bundle.js') }}"></script>
	</body>
</html>