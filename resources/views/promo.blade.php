@extends('layouts.app')
@section('contentdescription')
	<?php echo "Promo page di ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya. Silahkan melihat promo-promo betting di website ini. Promo mencakup bonus cashback, first deposit, referral system dan lain-lain.";?>
@endsection
@section('content')
<?php
	try {
		$promos = DB::table("promo")->orderBy('Urutan','ASC')->where('Status',4)->get();	
	} catch (Exception $e) {
		$promos = null;
	}	


	try {
		
	
?>
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Promo</h1>
				</header>

				<div class="page-single-content">
					<div class="promo-items promo-items-custom">

					<?php
					if($promos == null)
					{
						echo '<div class="no-promo">Tidak ada promo yang berlaku saat ini</div>';
					}
					else
					{	
					$counter = 1;					
					?>

						@foreach($promos as $promo)

							<div id="promo-item-<?php echo $counter;?>" class="promo-item promo-item-custom" >
								<div class="promo-item-teaser promo-item-teaser-custom">
									<a href="#promo-item-<?php echo $counter;?>-desc" class="toggle js-toggle" data-toggle-area="body">										
										<img alt="<?php if(isset($promo->AlternateImage))
											echo $promo->AlternateImage;?>" 
											title="<?php if(isset($promo->TitleImage))
												echo $promo->TitleImage;?>" class="promo-item-image" src="http://{{ $_SERVER['HTTP_HOST'].'/uploads/'.$promo->Image}}">
										<p class="note">Info Lengkap <svg class="icon" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-caret-down" /></svg></p>
									</a>
								
								</div>
								<div id="promo-item-<?php echo $counter;?>-desc" class="promo-item-desc promo-item-desc-custom">
									<h3 class="title">{{$promo->Description}}</h3>									
										<?php echo $promo->Content;?>										
									<div class="action">
										<a href="registration" class="button button-full">Join Now</a>
									</div>
								</div>
							</div>
							<?php $counter++;?>
						@endforeach
						<?php } 

						} catch (Exception $e) {
							Session::flash('errlogin',$e);
							Redirect::to('/')->send();
						}

						?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection