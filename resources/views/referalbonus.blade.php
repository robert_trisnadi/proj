@extends('layouts.app')
@section('contentdescription')
	<?php echo "Referral bonus page dari ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya.";?>
@endsection
<?php 
use App\SecurityQuestions;
use Illuminate\Support\Facades\Redirect;

if(!Session::has('id'))
{
   Redirect::to('/')->send();
}

$user = Request::session()->get('id','default');
$data_vp = DB::table('viewcustomerproductproduct')->where('CustomerID', 
		Request::session()->get('id')->CustomerID)->where('Status',4)
		->select('CustomerProductID','ProductName')
		->get();
$arrproducts = $data_vp->pluck('ProductName', 'CustomerProductID');

$countdownline = DB::table("uplinedownlineview")->where('UplineID',$user->CustomerID)->count("UplineID");

?>


@section('content')
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Member Area</h1>
				</header>

				<div class="page-single-content">
					<div class="user-dashboard">
						<div class="user-dashboard-aside">
							@include('include.usernav')
							<?php $bonus = Request::session()->get('bonusreferral', 0);?>
						</div>
						<div class="user-dashboard-body">
							<h2 class="user-dashboard-title">Referral Bonus</h2>
							@include('include.marq')
							<?php 
								$msg = Request::session()->pull('message', 'default');
								if($msg !='default')
									echo '<div class="success">'.$msg.'</div>';
								else
								{
									$msg = Request::session()->pull('err', 'default');
									if($msg !='default')
										echo '<div class="alert">'.$msg.'</div>';
								}
							?>	
							@if(count($errors)>0)	
								<div class="alert">
								@foreach($errors->all() as $error)		
									{{$error}}<?php echo '</br>';?>
								@endforeach
								</div>
							@endif
							<div class="user-dashboard-row">
								<div class="referral-card">
									<div class="referral-card-link referral-card-area">
										<p class="title">Link Referral</p>
										<a href="http://{{ $_SERVER['HTTP_HOST']}}/registration?ref=<?php echo $user->ReferralCode;?>" class="link">
										http://{{ $_SERVER['HTTP_HOST'] }}/registration?ref=<?php echo $user->ReferralCode;?>
										</a>
									</div>
									<div class="referral-card-areas">
										{!!Form::open(['url'=> '/referalbonus/claim'])!!}
										<div class="referral-card-total referral-card-area">
											<p class="title">Total Referral Bonus</p>
											<p class="value">Rp.&nbsp;<?php echo number_format($bonus,0,',','.');?></p>
											<?php if($bonus >= 50000 ){?>
											{{Form::submit('Claim',['class'=>"button button-alt"])}}
											{{Form::text('membertime','',['hidden'=>'true','id'=>'membertime'])}}
											{{Form::text('bonus',$bonus,['hidden'=>'true'])}}	
											{{Form::text('type','referral',['hidden'=>'true'])}}
											<?php } else echo 'note : Penarikan minimal Rp. 50.000 ,00';?>
										</div>
										{!!Form::close()!!}
										<div class="referral-card-transfer referral-card-area">
										{!!Form::open(['url'=> '/referalbonus/kirim'])!!}
											<p class="title">Transfer Bonus ke Game</p>
											<div class="select form-select">
												<label for="transfer-bonus" class="label">Transfer ke</label>
												{{Form::select('productname', $arrproducts,null,['id'=>"transfer-bonus",'class'=>'input'])}}
											</div>
											<?php if($bonus >= 50000){?>
											{{Form::submit('Transfer',['class'=>"button button-alt"])}}
											{{Form::text('membertime','',['hidden'=>'true','id'=>'membertime2'])}}
											{{Form::text('bonus',$bonus,['hidden'=>'true','id'=>'bonusreferral'])}}	
											{{Form::text('type','referral',['hidden'=>'true'])}}
											<?php } else echo 'note : Transfer minimal Rp. 50.000 ,00';?>
											{!!Form::close()!!}
										</div>
									</div>
								</div>

								<div class="table-history">
									<div class="table-history-head">
										<p class="title">Bonus Referral Anda</p>
										<p class="count">Jumlah Downline: <span class="value">
										<?php 
										if($countdownline!=null){
											?> 
											<a href="/downline" class="link">
											<?php echo $countdownline;?>
											</a>
										<?php
										}
										else
											echo 0;
										 ?></span></p>
									</div>
									<div class="table-history-body table-responsive">
									<table class="table" id="table-data">
										<thead>
											<tr>
												<th scope="col">#</th>
												<th scope="col">Tanggal</th>
												<th scope="col">Keterangan</th>
												<th scope="col">Jumlah</th>
												<th scope="col">Status</th>
											</tr>
										</thead>
										<tbody>
										@if($data!=null && count($data) == 0)
											<tr>
											<th scope="row">-</th>
											<td colspan="4" style="align:center">Tidak ada data</td>
											</tr>
										@endif		
										<?php $counter = 1;?>
										@foreach($data as $d)
											<tr>
												<th scope="row"><?php echo $counter++;?></th>
												<td>{{$d->RequestDate}}</td>												
												<td>{{$d->Keterangan}}</td>
												<td>Rp.&nbsp;{{number_format($d->Jumlah,0,',','.')}}</td>
												<?php
												if($d->Status == 3 || $d->Status == 1)//belum claim
													echo "<td class='success-font'>";
												else
													echo "<td>";
												
												echo (Request::session()->get('id')->statuss[$d->Status]->StatusName);
												?>
												</td>
											</tr>		
										@endforeach	
										</tbody>
										</table>									
									</div>
									<a class="table-history-more" 
										id="bonus-load-more-btn" 
										onclick="LoadMoreDataBonus({{$counter}},'table-data','referralbonus')">
										Load More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection