@extends('layouts.app')
@section('contentdescription')
	<?php echo "Deposit page dari ".$_SERVER["HTTP_HOST"].". Depositkan uang anda di website ini sebelum bermain.";?>
@endsection
<?php 
use App\SecurityQuestions;
try {
	if(!Session::has('id'))
	{
	   Redirect::to('/')->send();
	}
	$user = Request::session()->get('id');
	$data_vp = DB::table('viewcustomerproductproduct')->where('CustomerID', 
			Request::session()->get('id')->CustomerID)->where('Status',4)
			->select('CustomerProductID','ProductName')
			->get();
	$arrproducts = $data_vp->pluck('ProductName', 'CustomerProductID');

	$banks = DB::table('Bank')
			->join('BankAdmin','BankAdmin.BankID','Bank.BankID')
			->where(function($query)
			{
				$query->where('BankType','Deposit')->orWhere('BankType','Mix');
				return $query;
			})
			->where('BankAdmin.Status',4)
			->select('Bank.BankName','Bank.BankID')
			->orderBy('Bank.BankName', 'asc')
			->get();
	$banks = array_flip($banks->pluck('BankID', 'BankName')->all());	
} catch (Exception $e) {
	Request::session()->flash('errlogin', 'Halaman deposit error : '.$e);
	Redirect::to("/");
}

?>

@section('content')
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Member Area</h1>
				</header>

				<div class="page-single-content">
					<div class="user-dashboard">

						<div class="user-dashboard-aside">							
							@include('include.usernav')

						</div>

						<div class="user-dashboard-body">
							@include('include.marq')
							<h2 class="user-dashboard-title">Deposit</h2>
							<?php 
								$msg = Request::session()->pull('message', 'default');
								if($msg !='default')
									echo '<div class="success">'.$msg.'</div>';
								else
								{
									$msg = Request::session()->pull('err', 'default');
									if($msg !='default')
										echo '<div class="alert">'.$msg.'</div>';
								}
							?>	
							@if(count($errors)>0)	
								<div class="alert">
								@foreach($errors->all() as $error)		
									{{$error}}<?php echo '</br>';?>
								@endforeach
								</div>
							@endif
							<div class="user-dashboard-row">
							{!! Form::open(['url'=> '/deposit/kirim']) !!}
								<div class="form-text form-inline">
									<label for="deposit-bank-name" class="label">Bank</label>									
									{{Form::text('bankname',$user->BankName,['id'=>"deposit-bank-name",'disabled'=>'true','class'=>'input', 'placeholder'=>'BankName'])}}
		  							{{ Form::hidden('bankname',$user->BankName) }}
								</div>
								<div class="form-text form-inline">
									<label for="deposit-bank-account" class="label">No. Rekening</label>
									{{Form::text('norekbank',$user->BankAccountNumber,['id'=>"deposit-bank-account",'disabled'=>'true','class'=>'input', 'placeholder'=>'No. rekening bank'])}}
									{{ Form::hidden('norekbank',$user->BankAccountNumber) }}
								</div>
								<div class="form-text form-inline">
									<label for="deposit-bank-registrant" class="label">Nama Rekening</label>
									{{Form::text('pemilikbank',$user->BankAccountName,['id'=>"deposit-bank-registrant",'disabled'=>'true','class'=>'input', 'placeholder'=>'Nama pemilik bank'])}}
									{{ Form::hidden('pemilikbank',$user->BankAccountName) }}
								</div>
								<div class="form-select form-inline">
									<label for="deposit-game" class="label">Game</label>									
									{{Form::select('productname', $arrproducts,null,['id'=>"deposit-game",'class'=>'input'])}}
								</div>
								<div class="form-text form-inline">
									<label for="deposit-date" class="label">Tanggal Deposit</label>
									<input class='input' type="text" id="datepicker" size="30">									
									<input type='hidden' name='depositdate' id='dpd' value=''>
								</div>
								<div class="form-select form-inline">
									<label for="deposit-to" class="label">Bank Tujuan</label>									
									{{Form::select('banktujuan', $banks,null,['id'=>"deposit-to",'class'=>'input'])}}
								</div>
								<div class="form-text form-inline">
									<label for="deposit-amount" class="label">Jumlah Deposit</label>
									{{Form::text('jumlah','',['id'=>"deposit-amount",'class'=>'input', 'placeholder'=>'Jumlah'])}}
								</div>
								<div class="form-submit form-inline">
									{{Form::submit('Deposit',['class'=>"button button-full"])}}
								</div>
								{!! Form::close() !!}
							</div>

							<div class="user-dashboard-row">
								<div class="table-history">
									<div class="table-history-head">
										<p class="title">Data Deposit Anda</p>
									</div>
									<div class="table-history-body table-responsive">
										<table class="table" id="table-data">
											<thead>
												<tr>
													<th scope="col">#</th>
													<th scope="col">Tanggal Request</th>													
													<th scope="col">Game</th>
													<th scope="col">Jumlah</th>
													<th scope="col">Status</th>
												</tr>
											</thead>
											<tbody>
											@if(count($data) == 0)
												<tr>
												<th scope="row">-</th>
												<td colspan="4" style="align:center">Tidak ada data</td>
												</tr>
											@endif		
											<?php $counter = 1;?>
											@foreach($data as $d)
												<tr>
													<th scope="row"><?php echo $counter++; ?></th>
													<?php 
														$time = strtotime($d->RequestDate);
														$newformat = date('d F Y',$time);															
													?>
													<td>{{$d->RequestDate}}</td>
													<td>{{$d->ProductName}}</td>
													<td>Rp.&nbsp;{{number_format($d->Amount,0,',','.')}}</td>
													<?php
													if($d->Status == 3 || $d->Status == 1)//belum claim
														echo "<td class='success-font'>";
													else
														echo "<td>";
													
													echo (Request::session()->get('id')->statuss[$d->Status]->StatusName);
													if($d->Note != '-')
														echo ' - '.$d->Note;
													?>
													</td>
												</tr>		
											@endforeach	
											</tbody>
										</table>
									</div>									
									<a class="table-history-more" id="depo-load-more-btn" onclick="LoadMoreData({{$counter}},'table-data',
									<?php 
										if(count($data)>0)
											echo $data[count($data)-1]->DepositlogID;
									?>
									,'deposit')">Load More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection