@extends('layouts.app')
@section('contentdescription')
	<?php echo "Pendaftaran game page dari ".$_SERVER["HTTP_HOST"].". Daftarkan diri anda sebelum bermain. Permainan mencakup bola tangkas, casino, dan lain-lain";?>
@endsection
<?php 
use Illuminate\Support\Facades\Redirect;

if(!Session::has('id'))
{
   Redirect::to('/')->send();
}

$user = Request::session()->get('id','default');
$products = $products->pluck('ProductName','ProductID');
?>


@section('content')

<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Member Area</h1>
				</header>

				<div class="page-single-content">
					<div class="user-dashboard">

						<div class="user-dashboard-aside">							
							@include('include.usernav')
						</div>

						<div class="user-dashboard-body">
							@include('include.marq')
							<h2 class="user-dashboard-title">Daftar Game</h2>							
							<?php 
								$msg = Request::session()->pull('message', 'default');
								if($msg !='default')
									echo '<div class="success">'.$msg.'</div>';
								else
								{
									$msg = Request::session()->pull('err', 'default');
									if($msg !='default')
										echo '<div class="alert">'.$msg.'</div>';
								}
							?>	
							@if(count($errors)>0)	
								<div class="alert">
								@foreach($errors->all() as $error)		
									{{$error}}<?php echo '</br>';?>
								@endforeach
								</div>
							@endif
							<div class="user-dashboard-row">
								{!! Form::open(['url'=> '/daftargame/kirim']) !!}
								<div class="game-registration">
									<div class="form-select form-inline">
										<label for="game-register" class="label">Daftar Game</label>
										{{Form::select('productid', $products,null,['id'=>"game-register",'class'=>'input'])}}										
									</div>
									<div class="form-submit form-inline">
										{{Form::submit('Daftar Game',['class'=>'button button-full'])}}
									</div>
								</div>
								{!! Form::close() !!}
							</div>

							<div class="user-dashboard-row">
								<div class="table-history">
									<div class="table-history-head">
										<p class="title">Data Game Anda</p>
									</div>
									<div class="table-history-body table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th scope="col">#</th>
													<th scope="col">Game</th>
													<th scope="col">Username</th>
													<th scope="col">Password</th>
													<th scope="col">Status</th>
												</tr>
											</thead>
											<tbody>
											@if($data!=null && count($data) == 0)
												<tr>
												<th scope="row">-</th>
												<td colspan="4" style="align:center">Tidak ada data</td>
												</tr>
											@endif		
											<?php $counter = 1;?>
											@foreach($data as $d)
												<tr>
													<th scope="row"><?php echo $counter++;?></th>
													<td>{{$d->ProductName}}</td>
													<td>{{$d->Username}}</td>
													<td>{{$d->Password}}</td>
													<?php
													if($d->Status == 4 || $d->Status == 1)//belum claim
														echo "<td class='success-font'>";
													else
														echo "<td>";
													
													echo (Request::session()->get('id')->statuss[$d->Status]->StatusName);
													?>
													</td>
												</tr>		
											@endforeach	
											</tbody>
										</table>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection