@extends('layouts.app')
@section('contentdescription')
	<?php echo "Layanan kontak ".$_SERVER["HTTP_HOST"].". Anda punya komplain mengenain website dan seputar layanan ".$_SERVER["HTTP_HOST"]."? Silahkan ajukan di halaman ini.";?>
@endsection
@section('content')
<div id="site-content" class="site-content">	
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Bantuan</h1>
				</header>

				<div class="page-single-content">
					<div class="help-single">
						@include('include.sidebar')
						<div class="help-single-body">
							<h2 class="title">Hubungi Kami</h2>
							<div class="help-single-row">
								<div class="contact-space">
									<div class="contact-space-list">
										<div class="items">
											<p class="item"><span class="label">Whatsapp</span> <span class="value">+639662697620</span></p>
											<p class="item"><span class="label">BBM</span> <span class="value">E34A08E6</span></p>
											<p class="item"><span class="label">Line</span> <span class="value">t4sport</span></p>
											<p class="item"><span class="label">Email</span> <a href="mailto:cst4dsport@gmail.com" class="value">cst4dsport@gmail.com</a></p>
										</div>
									</div>
									<div class="contact-space-note">
										<p>Anda juga dapat menghubungi kami melalui fitur <a class="link" href="#" 
											onclick="parent.LC_API.open_chat_window({source:'minimized'})">Live Chat 24 jam.</a></p>
									</div>
								</div>
							</div>

							<div class="help-single-row">
								<div class="contact-complaint">
									<h3 class="contact-complaint-title">Complain Box</h3>
									<?php $errkomplain = Request::session()->get('errkomplain','default');
										$message = Request::session()->get('message','default');
										if($message != 'default')
										{
											echo '<div class="success">'.$message.'</div>';
										}
									?>
									@if($errkomplain != 'default')	
										<div class="alert">
											{{$errkomplain}}<?php echo '</br>';?>
										</div>
									@endif									
									<p class="contact-complaint-intro">Complain box merupakan upaya kami untuk berkomunikasi secara langsung kepada member yg mengalami masalah terhadap pelayanan staff kami, complain yang masuk akan langsung ditujukan pada divisi pengaduan.</p>
									<div class="contact-complaint-form">
									{!! Form::open(['url'=> '/komplain','files'=> true]) !!}
										<div class="form-text">
											<label for="contact-complaint-username" class="label">Username <span class="req">*</span></label>											
											{{Form::text('username','',['onChange'=>'checkUsername(this)','id'=>"contact-complaint-username",'class'=>'input', 'placeholder'=>'Enter username'])}}
											<span id="username_info" style="color:#006600"></span>
										</div>
										<div class="form-text">
											<label for="contact-complaint-phone" class="label">Handphone <span class="req">*</span></label>
											{{Form::text('handphone','',['id'=>"contact-complaint-phone",'class'=>'input', 'placeholder'=>'Enter valid phone number'])}}
										</div>
										<div class="form-text">
											<label for="contact-complaint-email" class="label">Email <span class="req">*</span></label>
											{{Form::email('emailkomplain','',['id'=>"contact-complaint-email",'class'=>'input', 'placeholder'=>'Email'])}}
										</div>
										<div class="form-text">
											<label for="contact-complaint-message" class="label">Pesan <span class="req">*</span></label>
											{{Form::textarea('komplain','',['id'=>"contact-complaint-message",'class'=>'input', 'placeholder'=>'Enter your message'])}}
										</div>										
										<div class="form-submit">
											{{Form::submit('Kirim',['id'=>'btn_submit_complain','disabled'=>'true','class'=>"button button-full"])}}
										</div>
										{!!Form::close()!!}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection