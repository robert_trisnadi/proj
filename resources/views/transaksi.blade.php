@extends('layouts.app')
@section('contentdescription')
	<?php echo "Halaman transaksi di T4Sportbet.com! Website agen betting terpercaya. Pengguna bisa melihat history transaksi di halaman ini.";?>
@endsection
<?php 
use App\SecurityQuestions;
use Illuminate\Support\Facades\Redirect;

if(!Session::has('id'))
{
   Redirect::to('/')->send();
}

$user = Request::session()->get('id','default');
?>


@section('content')

<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Member Area</h1>
				</header>

				<div class="page-single-content">
					<div class="user-dashboard">

						<div class="user-dashboard-aside">							
							@include('include.usernav')
						</div>

						<div class="user-dashboard-body">
							<h2 class="user-dashboard-title">Transaksi</h2>
							<?php 
								$msg = Request::session()->pull('message', 'default');
								if($msg !='default')
									echo '<div class="sucess">'.$msg.'</div>';
								else
								{
									$msg = Request::session()->pull('err', 'default');
									if($msg !='default')
										echo '<div class="alert">'.$msg.'</div>';
								}
							?>	
							@if(count($errors)>0)	
								<div class="alert">
								@foreach($errors->all() as $error)		
									{{$error}}<?php echo '</br>';?>
								@endforeach
								</div>
							@endif							
							<div class="user-dashboard-row">
								<div class="table-history">
									<div class="table-history-head">
										<p class="title">Data Transaksi Anda</p>
									</div>
									<div class="table-history-body table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th scope="col">#</th>
													<th scope="col">Tanggal</th>
													<th scope="col">Game</th>
													<th scope="col">Type</th>
													<th scope="col">Jumlah</th>
													<th scope="col">Status</th>
												</tr>
											</thead>
											<tbody>
											@if($data!=null && count($data) == 0)
												<tr>
												<th scope="row">-</th>
												<td colspan="4" style="align:center">Tidak ada data</td>
												</tr>
											@endif		
											<?php $counter = 1;?>
											@foreach($data as $d)
												<tr>
													<th scope="row"><?php echo $counter++;?></th>
													<td>{{$d->Tanggal}}</td>
													<td>{{$d->Game}}</td>
													<td>{{$d->Type}}</td>
													<td>{{$d->Jumlah}}</td>
													<td>{{$d->Status}}</td>
												</tr>		
											@endforeach	
											</tbody>
										</table>	
									</div>
									<!-- <a href="#" class="table-history-more">Load More</a> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection