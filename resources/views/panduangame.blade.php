@extends('layouts.app')
@section('contentdescription')
	<?php echo "Panduan game di ".$_SERVER['HTTP_HOST']."! Website agen betting terpercaya.";?>	
@endsection
@section('content')

<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Bantuan</h1>
				</header>

				<div class="page-single-content">
					<div class="help-single">
						@include('include.sidebar')
						<div class="help-single-body">
							<h2>Panduan Bermain Judi Bola, Casino di <a href="http://<?php echo $_SERVER['HTTP_HOST'];?>" title="Situs Judi Online" target="_blank"><strong>Situs Judi Bola</strong></a> <?php echo $_SERVER['HTTP_HOST'];?></h2>
							<div class="panduangame-posts">
								<nav>
									<div class="nav nav-tabs" id="nav-tab" role="tablist">
										<a class="nav-item nav-link active" id="nav-panduan-bola" data-toggle="tab" href="#panduan-bola" role="tab" aria-controls="panduan-bola" aria-selected="true">Cara Main Bola Online</a>
										<a class="nav-item nav-link" id="nav-panduan-casino" data-toggle="tab" href="#panduan-casino" role="tab" aria-controls="panduan-casino" aria-selected="false">Cara Main Casino Online</a>
									</div>
								</nav>
								<div class="tab-content" id="nav-tabContent">
									<div class="tab-pane fade show active" id="panduan-bola" role="tabpanel" aria-labelledby="nav-panduan-bola">
										<div class="row">
											<div class="col-3">
												<div class="nav flex-column nav-pills" id="side-panduan-bola" role="tablist" aria-orientation="vertical">
													<a class="nav-link active" id="panduan-bola-0" data-toggle="pill" href="#panduan-bola-penjelasan" role="tab" aria-controls="panduan-bola-penjelasan" aria-selected="false">Penjelasan Umum</a>
													<a class="nav-link" id="panduan-bola-1" data-toggle="pill" href="#panduan-bola-handicap" role="tab" aria-controls="panduan-bola-handicap" aria-selected="true">Handicap</a>
													<a class="nav-link" id="panduan-bola-2" data-toggle="pill" href="#panduan-bola-over-under" role="tab" aria-controls="panduan-bola-over-under" aria-selected="false">Over / Under</a>
													<a class="nav-link" id="panduan-bola-3" data-toggle="pill" href="#panduan-bola-1x2" role="tab" aria-controls="panduan-bola-1x2" aria-selected="false">1x2</a>
													<a class="nav-link" id="panduan-bola-4" data-toggle="pill" href="#panduan-bola-odd-even" role="tab" aria-controls="panduan-bola-odd-even" aria-selected="false">Odd / Even</a>
													<a class="nav-link" id="panduan-bola-5" data-toggle="pill" href="#panduan-bola-mix-parlay" role="tab" aria-controls="panduan-bola-mix-parlay" aria-selected="false">Mix Parlay</a>
													<a class="nav-link" id="panduan-bola-6" data-toggle="pill" href="#panduan-bola-correct-score" role="tab" aria-controls="panduan-bola-correct-score" aria-selected="false">Correct Score</a>
												</div>
											</div>
											<div class="col-9">
												<div class="tab-content" id="panduan-bola-tabContent">
													<div class="tab-pane fade show active" id="panduan-bola-penjelasan" role="tabpanel" aria-labelledby="panduan-bola-0">
														<h2>Penjelasan Mengenai Tabel di Situs Betting Bola</h2>
														<hr />
														<figure>
															<img src="/assets/images/bola/tabel-bola-1.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Tabel Judi Bola Online">
															<figcaption>Gambar 1.0 - Gambar Untuk Tabel Untuk Judi Bola Online</figcaption>
														</figure>
														<p>Untuk kamu yang belum pernah main bola sebelumnya dan belum mengerti mengenai singkatan - singkatan yang ada di tabel untuk pasang judi bola online, kami sarankan untuk membaca sedikit agar kamu tidak salah pasang ketika ingin bermain <a href="http://t4sport.info" target="_blank">judi online</a> di situs T4SPORT.info</p>
														<p>Mengikuti pada contoh Gambar 1.0 diatas pada pertandingan <strong>World Cup Russia 2018</strong>, berikut penjelasan nya.</p>
														<dl class="row">
															<dt class="col-2">Time</dt>
															<dd class="col-10">
																Waktu Pertandingan <small class="text-muted">(GMT +8)</small>, untuk jam WIB maka dikurangi 1 jam menjadi 17.00
																<br />
																Apabila ada keterangan <span class="text-red">Live</span>, maka taruhan untuk pertandingan tersebut sedang berlangsung.
															</dd>

															<dt class="col-2">Event</dt>
															<dd class="col-10">
																Tim yang akan bertanding, untuk tim warna <span class="text-red">Merah</span> maka tim tersebut yang memberikan Fur kepada lawan nya yang berwarna <span class="text-blue">Biru</span>.
															</dd>

															<dt class="col-2">Full Time</dt>
															<dd class="col-10">
																Memasang taruhan untuk pertandingan penuh, kemenangan akan dibayar setelah pertandingan babak pertama dan babak kedua selesai ( 2 x 45 menit+ ).
															</dd>

															<dt class="col-2">First Half</dt>
															<dd class="col-10">
																Memasang taruhan hanya untuk setengah babak pertandingan, kemenangan akan dibayar setelah pertandingan babak pertama selesai ( 45 menit+ ).
															</dd>

															<dt class="col-2">Pasaran</dt>
															<dd class="col-10">
																Nilai Fur-an yang ada pada HDP <small class="text-muted">(Handicap)</small>, OU <small class="text-muted">(Over / Under)</small>.
																<br />
																<div class="card">
																	<div class="card-body">
																		Contoh Pasaran di Gambar 1.0 :
																		<br />
																		- HDP Full Time yaitu <code>1.5-2</code> yang arti nya fur 1 &#190;. <br />
																		- OU <small class="text-muted">(Over / Under)</small> Full Time yaitu <code>2.5-3</code> yang artinya fur 2 &#190;<br />
																		<br />
																		Berikut ini keterangan nilai - nilai Pasaran :
																		<br />
																		<code>0-0.5</code> = Fur &#188;.
																		<br />
																		<code>0.5</code> = Fur &#189;.
																		<br />
																		<code>0.5-1</code> = Fur &#190;.
																		<br />
																		<code>1-1.5</code> = Fur 1 &#188;
																		<br />
																		dan seterusnya..
																	</div>
																</div>
															</dd>

															<dt class="col-2">Odds <br /><small class="text-muted">( Key / Uang Air )</small></dt>
															<dd class="col-10">
																Nilai Odds adalah bayaran untuk pertandingan yang sedang berlangsung, ada 2 warna pada Odds yaitu :
																<br />
																<dl>
																	<dt><span class="text-red">Merah</span></dt>
																	<dd>
																		<ul>
																			<li>Kalah : Bayar lebih banyak dari nilai taruhan <small class="text-muted">(tergantung besar Odds)</small></li>
																			<li>Menang : Dapat uang sesuai taruhan yang dipasang.</li>
																		</ul>
																	</dd>
																</dl>
																<dl>
																	<dt>Hitam</dt>
																	<dd>
																		<ul>
																			<li>Kalah : Bayar sesuai dengan taruhan yang dipasang.</li>
																			<li>Menang : Dapat uang lebih banyak dari taruhan kita <small class="text-muted">(tergantung besar Odds)</small>.</li>
																		</ul>
																	</dd>
																</dl>
																<div class="card">
																	<div class="card-body">
																		Contoh Odds pada <mark>HDP Full Time</mark> :<br />
																		<ul>
																			<li><span class="text-red">-1.13</span>, yaitu taruhan untuk pasang tim <span class="text-red">France</span></li>
																			<li>1.07, yaitu taruhan untuk pasang tim <span class="text-blue">Australia</span></li>
																		</ul>
																	</div>
																</div>
															</dd>

															<dt class="col-2">
																HDP <small class="text-muted">(Handicap)</small>
															</dt>
															<dd class="col-10">
																Taruhan Handicap, tim yang memberikan fur maka odds nya selalu berwarna <span class="text-red">Merah</span>.
															</dd>

															<dt class="col-2">
																OU <small class="text-muted">(Over / Under)</small>
															</dt>
															<dd class="col-10">
																Hasil pertandingan tim <span class="text-red">Merah</span> dan <span class="text-blue">Biru</span> apabila dijumlahkan nilainya Over <small class="text-muted">(lebih)</small> atau Under <small class="text-muted">(kurang)</small> nilai Pasaran pada OU <code>(2.5-3)</code>. Tergantung pada Odds yang anda pasang.
																<br />
																<div class="card">
																	<div class="card-body">
																		Contoh <mark>Pasang Over</mark> <small class="text-muted">(lebih)</small> dengan nilai Pasaran 2.5-3
																		<br />
																		<br />
																		Skor tim <span class="text-red">Merah</span> dan tim <span class="text-blue">Biru</span> dijumlahkan, apabila hasilnya :<br />
																		- 0 sampai 2 : kamu Kalah.<br />
																		- 3 : kamu menang taruhan setengah atau 50% dari taruhan kamu.<br />
																		- Lebih besar dari 3, kamu Menang.
																	</div>
																</div>
																<div class="card">
																	<div class="card-body">
																		Contoh <mark>Pasang Under</mark> <small class="text-muted">(kurang)</small> dengan nilai Pasaran 2.5-3
																		<br />
																		<br />
																		Skor tim <span class="text-red">Merah</span> dan tim <span class="text-blue">Biru</span> dijumlahkan, apabila hasilnya :<br />
																		- 0 sampai 2 : kamu Menang.<br />
																		- 3 : kamu menang taruhan setengah atau 50% dari taruhan kamu.<br />
																		- Lebih besar dari 3, kamu Kalah.
																	</div>
																</div>
															</dd>

															<dt class="col-2">1x2</dt>
															<dd class="col-10">
																Taruhan untuk menebak tim yang bertanding Seri atau Menang berapa pun nilai skor nya.<br />
																Cukup pilih tim yang dipasang dari Odds yang tersedia pada kolom 1x2.
																<div class="card">
																	<div class="card-body">
																		Contoh <mark>1x2 Full Time</mark> di Gambar 1.0
																		<br />
																		<br />
																		1 : Tuan Rumah, baris pertama (<span class="text-red">France</span> - Odds 1.24)
																		<br />
																		X : Draw atau Seri, baris ke 3 (Odds 7.00)
																		<br />
																		2 : Tim Tamu, baris kedua (<span class="text-blue">Australia</span> - Odds 10.00)
																	</div>
																</div>
															</dd>

															<dt class="col-2">OE <small class="text-muted">(Odd / Even)</small></dt>
															<dd class="col-10">
																Taruhan untuk menebak hasil pertandingan tim <span class="text-red">Merah</span> dan tim <span class="text-blue">Biru</span> apabila dijumlahkan maka hasilnya Ganjil <small class="text-muted">(odd)</small> atau Genap <small class="text-muted">(even)</small>.
																<div class="card">
																	<div class="card-body">
																		Contoh Odds <mark>Ganjil <small class="text-muted">(odd)</small> atau Genap <small class="text-muted">(even)</small> Full Time</mark> di Gambar 1.0
																		<br />
																		<br />
																		Ganjil <small class="text-muted">(odd)</small> : baris pertama (Odds 1.00)
																		<br />
																		Genap <small class="text-muted">(even)</small> : baris kedua (Odds <span class="text-red">-1.11</span>)
																	</div>
																</div>
															</dd>
														</dl>
														<p>Apabila kamu masih kurang mengerti mengenai yang sudah dijelaskan di atas, kamu bisa cek ke menu lain nya yang ada di sebelah kiri halaman ini.</p>
													</div>

													<!-- Handicap -->
													<div class="tab-pane fade" id="panduan-bola-handicap" role="tabpanel" aria-labelledby="panduan-bola-1">
														<h2>Panduan Cara Bermain Taruhan Judi Bola Online Handicap</h2>
														<hr />
														<figure>
															<img src="/assets/images/bola/tabel-bola-1-1.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Panduan Taruhan Bola Handicap">
															<figcaption>Gambar 1.1 - Gambar Untuk Penjelasan Taruhan Handicap</figcaption>
														</figure>
														<p>Untuk kamu yang belum mengerti tentang cara bermain atau bertaruh bola online Handicap, disini kami akan menjelaskan lebih lengkap mengenai cara bertaruh Handicap serta contoh perhitungan nya.</p>
														<p>Disini kami akan memakai contoh pada kolom Full Time saja, yang diberi kotak berwarna <span class="text-red">Merah</span>.</p>
														<h3>Cara Main Taruhan Judi Bola Online Handicap</h3>
														<p>Taruhan <b>Handicap</b> : Taruhan Handicap, tim yang memberikan fur maka odds nya selalu berwarna <span class="text-red">Merah</span>.<br />
														Pasaran <code>1.5-2</code> atau fur 1 &#190; yang ada di baris <span class="text-red">France</span>, yang ada disamping nilai Odds merupakan nilai Fur yang diberikan oleh <span class="text-red">France</span>.</p>
														<p>Taruhan <span class="text-red">France</span> akan dianggap Menang penuh apabila skor nya minimal 2-0.<br />Apabila skor nya <span class="text-red">France</span> 1-0 <span class="text-blue">Australia</span> maka hanya akan dianggap menang 50%, karena <span class="text-red">France</span> memberikan Fur 1 &#190;.</p>
														<p>Taruhan <span class="text-blue">Australia</span> akan dianggap Menang apabila skor 0-0 & 1-0.<br />Apabila skor nya <span class="text-red">France</span> 2-0 <span class="text-blue">Australia</span> maka tetap dianggap menang 50%, karena <span class="text-red">France</span> memberikan Fur 1 &#190;</p>
														<div class="card">
															<div class="card-body">
																<p>Ada 2 angka yang ada di kolom Handicap (Odds / Key / Uang Air) :</p>
																<ul class="list-unstyled">
																	<li><code>-1.13</code> : Untuk pasang taruhan kamu di <span class="text-red">France</span> apabila kamu yakin Perancis pasti menang.</li>
																	<li><code>1.07</code> : Untuk pasang taruhan kamu di <span class="text-blue">Australia</span> apabila kamu yakin Australia pasti menang.</li>
																</ul>
															</div>
														</div>
														<h3>Contoh Menghitung Taruhan Judi Bola Online Handicap</h3>
														<p>Misalkan kamu bertaruh 100 dan apabila kamu pasang di :</p>
														<div class="card">
															<div class="card-body">
																<dl>
																	<dt><span class="text-red">France</span> - Odds <span class="text-red">-1.13</span></dt>
																	<dd>
																		<ul>
																			<li>Menang : Kamu menang 1 x 100 = 100</li>
																			<li>Kalah : Kamu kalah 1.13 x 100 = 113</li>
																		</ul>
																	</dd>
																	<dt><span class="text-blue">Australia</span> - Odds 1.07</dt>
																	<dd>
																		<ul>
																			<li>Menang : Kamu menang 1.07 x 100 = 107</li>
																			<li>Kalah : Kamu kalah 1 x 100 = 100</li>
																		</ul>
																	</dd>
																</dl>
															</div>
														</div>
													</div>

													<!-- Over Under -->
													<div class="tab-pane fade" id="panduan-bola-over-under" role="tabpanel" aria-labelledby="panduan-bola-2">
														<h2>Panduan Cara Bermain Taruhan Judi Bola Online Over / Under  <small class="text-muted">(Lebih / Kurang)</small></h2>
														<hr />
														<figure>
															<img src="/assets/images/bola/tabel-bola-1-2.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Panduan Taruhan Bola Over / Under">
															<figcaption>Gambar 1.2 - Gambar Untuk Penjelasan Taruhan Over / Under</figcaption>
														</figure>
														<p>Untuk kamu yang belum mengerti tentang cara bermain atau bertaruh bola online Over / Under, disini kami akan menjelaskan lebih lengkap mengenai cara bertaruh Over / Under serta contoh perhitungan nya.</p>
														<p>Disini kami akan memakai contoh pada kolom Full Time saja, yang diberi kotak berwarna <span class="text-red">Merah</span>.</p>
														<h3>Cara Main Taruhan Judi Bola Online Over / Under</h3
			>											<p>Taruhan <b>Over / Under</b> : Taruhan hasil pertandingan tim <span class="text-red">France</span> dan <span class="text-blue">Australia</span> apabila dijumlahkan nilainya Over <small class="text-muted">(lebih)</small> atau Under <small class="text-muted">(kurang)</small> nilai Pasaran pada OU <code>(2.5-3)</code>, untuk taruhan Over / Under tidak ada pengaruh dari tim mana yang menang.</p>
														<p>Total Skor = Jumlah skor pertandingan <span class="text-red">France</span> dan <span class="text-blue">Australia</span> ketika ditambahkan</p>
														<div class="card">
															<div class="card-body">
																<p>Beberapa contoh taruhan <b>Over <small class="text-muted">(lebih)</small></b>.</p>
																<dl>
																	<dt>Pasaran : <code>1.0-1.5</code></dt>
																	<dd>
																		<ul>
																			<li>Total Skor 0 : Kalah</li>
																			<li>Total Skor 1 : Kalah 50%</li>
																			<li>Total Skor 2 atau lebih : Menang Full</li>
																		</ul>
																	</dd>
																	<dt>Pasaran : <code>1.5</code></dt>
																	<dd>
																		<ul>
																			<li>Total Skor 0 atau 1 : Kalah</li>
																			<li>Total Skor 2 atau lebih : Menang Full</li>
																		</ul>
																	</dd>
																	<dt>Pasaran : <code>2.5-3</code></dt>
																	<dd>
																		<ul>
																			<li>Total Skor 2 atau kurang : Kalah</li>
																			<li>Total Skor 3 : Menang 50%</li>
																			<li>Total Skor 4 atau lebih : Menang Full</li>
																		</ul>
																	</dd>
																</dl>
															</div>
														</div>
														<div class="card">
															<div class="card-body">
																<p>Beberapa contoh taruhan <b>Under <small class="text-muted">(kurang)</small></b>.</p>
																<dl>
																	<dt>Pasaran : <code>1.0-1.5</code></dt>
																	<dd>
																		<ul>
																			<li>Total Skor 0 : Menang Full</li>
																			<li>Total Skor 1 : Menang 50%</li>
																			<li>Total Skor 2 atau lebih : Kalah</li>
																		</ul>
																	</dd>
																	<dt>Pasaran : <code>1.5</code></dt>
																	<dd>
																		<ul>
																			<li>Total Skor 0 atau 1 : Menang Full</li>
																			<li>Total Skor 2 atau lebih : Kalah</li>
																		</ul>
																	</dd>
																	<dt>Pasaran : <code>2.5-3</code></dt>
																	<dd>
																		<ul>
																			<li>Total Skor 2 atau kurang : Menang Full</li>
																			<li>Total Skor 3 : Kalah 50%</li>
																			<li>Total Skor 4 atau lebih : Kalah</li>
																		</ul>
																	</dd>
																</dl>
															</div>
														</div>
														<h3>Contoh Menghitung Taruhan Judi Bola Online Over / Under</h3>
														<p>Misalkan kamu bertaruh 100 dan apabila kamu pasang di :</p>
														<div class="card">
															<div class="card-body">
																<dl>
																	<dt>Taruhan Over <small class="text-muted">(lebih)</small>, Odds <span class="text-red">-1.16</span></dt>
																	<dd>
																		<ul>
																			<li>Skor 2 atau kurang : Kalah 1.16 x 100 = 116</li>
																			<li>Skor 3 : Menang 50%, 0.5 x 100 = 50</li>
																			<li>Skor 4 atau lebih : Menang Full, 1 x 100 = 100</li>
																		</ul>
																	</dd>
																	<dt>Taruhan Under <small class="text-muted">(kurang)</small> - Odds 1.07</dt>
																	<dd>
																		<ul>
																			<li>Skor 2 atau kurang : Menang Full, 1.07 x 100 = 107</li>
																			<li>Skor 3 : Kalah 50%, 0.5 x 100 = 50</li>
																			<li>Skor 4 atau lebih : Kalah, 1 x 100 = 100</li>
																		</ul>
																	</dd>
																</dl>
															</div>
														</div>
													</div>

													<!-- 1x2 -->
													<div class="tab-pane fade" id="panduan-bola-1x2" role="tabpanel" aria-labelledby="panduan-bola-3">
														<h2>Panduan Cara Bermain Taruhan Judi Bola Online 1x2</h2>
														<hr />
														<figure>
															<img src="/assets/images/bola/tabel-bola-1-3.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Panduan Taruhan Bola 1x2">
															<figcaption>Gambar 1.3 - Gambar Untuk Penjelasan Taruhan 1x2</figcaption>
														</figure>
														<p>Untuk kamu yang belum mengerti tentang cara bermain atau bertaruh bola online 1x2, disini kami akan menjelaskan lebih lengkap mengenai cara bertaruh 1x2 serta contoh perhitungan nya.</p>
														<p>Disini kami akan memakai contoh pada kolom Full Time saja, yang diberi kotak berwarna <span class="text-red">Merah</span>.</p>
														<h3>Cara Main Taruhan Judi Bola Online 1x2</h3>
														<p>Taruhan <b>1x2</b> : Taruhan untuk menebak tim yang bertanding Seri atau Menang berapa pun nilai skor nya<br />Untuk cara pasangnya cukup klik pada angka - angka yang ada di kolom 1x2.</p>
														<p>Taruhan <span class="text-red">France</span> akan dianggap menang apabila skor minimal <span class="text-red">France</span> 1-0 <span class="text-blue">Australia</span></p>
														<p>Taruhan <span class="text-blue">Australia</span> akan dianggap menang apabila skor minimal <span class="text-red">France</span> 0-1 <span class="text-blue">Australia</span></p>
														<div class="card">
															<div class="card-body">
																<p>Ada 3 angka yang ada di kolom 1X2 (Odds / Key / Uang Air) :</p>
																<ul class="list-unstyled">
																	<li><code>1.24</code> : Untuk pasang taruhan kamu di <span class="text-red">France</span> apabila kamu yakin Perancis pasti menang.</li>
																	<li><code>10.00</code> : Untuk pasang taruhan kamu di <span class="text-blue">Australia</span> apabila kamu yakin Australia pasti menang.</li>
																	<li><code>7.00</code> : Untuk pasang taruhan kamu di Draw atau Seri apabila kamu yakin pertandingan ini pasti Seri.</li>
																</ul>
															</div>
														</div>
														<h3>Contoh Menghitung Taruhan Judi Bola Online 1x2</h3>
														<p>Untuk menghitung taruhan 1x2, apabila Menang nilai Odds akan dikurangi 1 terlebih dahulu sebelum dikali dengan nilai taruhan kamu.</p>
														<p>Misalkan kamu bertaruh 100 dan apabila kamu pasang di :</p>
														<div class="card">
															<div class="card-body">
																<dl>
																	<dt><span class="text-red">France</span> - Odds 1.24</dt>
																	<dd>
																		<ul>
																			<li>Menang : Kamu menang (1.24 - 1) x 100 = 24</li>
																			<li>Kalah : Kamu kalah 1 x 100 = 100</li>
																		</ul>
																	</dd>
																	<dt><span class="text-blue">Australia</span> - Odds 10.00</dt>
																	<dd>
																		<ul>
																			<li>Menang : Kamu menang (10.00 - 1) x 100 = 900</li>
																			<li>Kalah : Kamu kalah 1 x 100 = 100</li>
																		</ul>
																	</dd>
																	<dt>Draw - Odds 7.00</dt>
																	<dd>
																		<ul>
																			<li>Menang : Kamu menang (7.00 - 1) x 100 = 600</li>
																			<li>Kalah : Kamu kalah 1 x 100 = 100</li>
																		</ul>
																	</dd>
																</dl>
															</div>
														</div>
													</div>
													<div class="tab-pane fade" id="panduan-bola-odd-even" role="tabpanel" aria-labelledby="panduan-bola-4">
														<h2>Panduan Cara Bermain Taruhan Judi Bola Online Odd / Even <small class="text-muted">(Ganjil / Genap)</small></h2>
														<hr />
														<figure>
															<img src="/assets/images/bola/tabel-bola-1-4.png" alt="cara main judi bola, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Panduan Taruhan Bola Odd / Even atau Ganjil / Genap">
															<figcaption>Gambar 1.4 - Gambar Untuk Penjelasan Taruhan Odd / Even</figcaption>
														</figure>
														<p>Untuk kamu yang belum mengerti tentang cara bermain atau bertaruh bola online Odd / Even, disini kami akan menjelaskan lebih lengkap mengenai cara bertaruh Odd / Even serta contoh perhitungan nya.</p>
														<p>Disini kami akan memakai contoh pada kolom Full Time saja, yang diberi kotak berwarna <span class="text-red">Merah</span>.</p>
														<h3>Cara Main Taruhan Judi Bola Online Odd / Even</h3>
														<p>Taruhan <b>Odd / Even</b> : Taruhan tebak Jumlah hasil pertandingan <span class="text-red">France</span> dan <span class="text-blue">Australia</span> hasilnya Odd <small class="text-muted">(Ganjil)</small> atau Even <small class="text-muted">(Genap)</small>, untuk taruhan Odd / Even tidak ada pengaruh dari tim mana yang menang.</p>
														<p>Angka Odd <small class="text-muted">(Ganjil)</small> : 1, 3, 5, 7, 9, dan seterusnya<br />
														Angka Even <small class="text-muted">(Genap)</small> : 0, 2, 4, 6, 8, 10, dan seterusnya</p>
														<div class="card">
															<div class="card-body">
																<ul>
																	<li><span class="text-red">France</span> 0 - 0 <span class="text-blue">Australia</span> : Genap (Total Skor : 0)</li>
																	<li><span class="text-red">France</span> 1 - 0 <span class="text-blue">Australia</span> : Ganjil (Total Skor : 1)</li>
																	<li><span class="text-red">France</span> 0 - 1 <span class="text-blue">Australia</span> : Ganjil (Total Skor : 1)</li>
																	<li><span class="text-red">France</span> 1 - 1 <span class="text-blue">Australia</span> : Genap (Total Skor : 2)</li>
																	<li><span class="text-red">France</span> 2 - 1 <span class="text-blue">Australia</span> : Ganjil (Total Skor : 3)</li>
																	<li><span class="text-red">France</span> 1 - 2 <span class="text-blue">Australia</span> : Ganjil (Total Skor : 3)</li>
																	<li><span class="text-red">France</span> 2 - 2 <span class="text-blue">Australia</span> : Genap (Total Skor : 4)</li>
																	<li>dan seterusnya..</li>
																</ul>
															</div>
														</div>
														<p>Apabila kamu pasang Odd, maka skor untuk Menang harus Angka Ganjil.</p>
														<p>Apabila kamu pasang Even, maka skor untuk Menang harus Angka Genap.</p>
														<h3>Cara Menghitung Taruhan Judi Bola Online Odd / Even</h3>
														<p>Misalkan kamu bertaruh 100 dan apabila kamu pasang di :</p>
														<div class="card">
															<div class="card-body">
																<dl>
																	<dt>Odd <small class="text-muted">(Ganjil)</small> - Odds 1.00</dt>
																	<dd>
																		<ul>
																			<li>Total Skor 0 : Kamu kalah 1 x 100 = 100</li>
																			<li>Total Skor 1 : Kamu menang 1 x 100 = 100</li>
																			<li>Total Skor 2 : Kamu kalah 1 x 100 = 100</li>
																			<li>Total Skor 3 : Kamu menang 1 x 100 = 100</li>
																			<li>dan seterusnya..</li>
																		</ul>
																	</dd>
																	<dt>Even <small class="text-muted">(Genap)</small> - Odds <span class="text-red">-1.11</span></dt>
																	<dd>
																		<ul>
																			<li>Total Skor 0 : Kamu menang 1 x 100 = 100</li>
																			<li>Total Skor 1 : Kamu kalah 1.11 x 100 = 111</li>
																			<li>Total Skor 2 : Kamu menang 1 x 100 = 100</li>
																			<li>Total Skor 3 : Kamu kalah 1.11 x 100 = 111</li>
																			<li>dan seterusnya..</li>
																		</ul>
																	</dd>
																</dl>
															</div>
														</div>
													</div>
													<div class="tab-pane fade" id="panduan-bola-mix-parlay" role="tabpanel" aria-labelledby="panduan-bola-5">
														<h2>Panduan Cara Bermain Taruhan Judi Bola Online Mix Parlay <small class="text-muted">(Taruhan Paketan)</small></h2>
														<hr />
														<figure>
															<img src="/assets/images/bola/parlay-1.png" alt="cara main judi bola, mix parlay, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Panduan Taruhan Bola Mix Parlay">
															<figcaption>Gambar 2.0 - Gambar Untuk Penjelasan Taruhan Mix Parlay</figcaption>
														</figure>
														<p><small class="text-muted">Catatan Gambar : Untuk Mix Parlay tetap ada untuk taruhan First Half. Untuk contoh, kita hanya menampilkan yang Full Time.</small></p>
														<p>Untuk kamu yang belum mengerti tentang cara bermain atau bertaruh bola online <strong>Mix Parlay</strong>, disini kami akan menjelaskan lebih lengkap mengenai cara bertaruh Mix Parlay serta contoh perhitungan nya.</p>
														<p>Perhatikan contoh pada gambar, untuk yang diberikan kotak <span class="text-red">Merah</span> adalah keterangan </p>
														<h3>Cara Main Taruhan Judi Bola Online Mix Parlay</h3>
														<p>Taruhan <b>Mix Parlay</b> : Taruhan untuk beberapa pertandingan sekaligus untuk menjadi 1 paket taruhan. Keuntungan nya adalah nilai Odds menang akan lebih tinggi daripada taruhan normal. Untuk memainkan Mix Parlay ini harus memilih minimal 2 sampai 3 tim <small class="text-muted">(Tergantung Provider)</small>.</p>
														<p>Untuk Mix Parlay kamu hanya bisa pilih 1 taruhan untuk setiap tim.<br />
														Misal, untuk <span class="text-red">Sweden</span> vs <span class="text-blue">Korean Republic</span> kamu memilih HDP, apabila kamu coba klik untuk menambah taruhan <span class="text-red">Sweden</span> vs <span class="text-blue">Korean Republic</span> skor Over, maka taruhan HDP kamu akan dibatalkan dan berubah menjadi taruhan Over. Termasuk untuk taruhan lainnya.</p>
														<p>Apabila ada 1 tim kalah dalam paket Mix Parlay yang kamu mainkan, maka sudah dianggap kalah. Untuk menang penuh Mix Parlay, semua tim tebakan kamu harus Menang atau Betul.</p>
														<p>Apabila ada tim yang menang atau kalah 50%, maka pasaran tersebut memiliki perhitungan tersendiri yang akan di jelaskan di bawah.</p>
														<h3>Cara Menghitung Taruhan Mix Parlay Menang Full</h3>
														<p>Contoh untuk menghitung Mix Parlay pada Gambar 2.0, ada 3 tim yang dipilih pada Mix Parlay</p>
														<p>Misalkan kamu bertaruh 100 <small class="text-muted">(dalam ribuan rupiah)</small> dan semua tim yang kamu pilih Menang, maka berikut ini cara untuk menghitungnya<br /><small class="text-muted">Catatan : Salah 1 tim kalah, maka Mix Parlay kamu langsung dianggap kalah.</small></p>
														<div class="card">
															<div class="card-body">
																<p>Tim yang dipasang :</p>
																<ul class="list-unstyled">
																	<li><b>Partai 1</b> : <span class="text-red">Sweden</span> vs <span class="text-blue">Korea Republic</span> - HDP 0.50 <span class="text-red">Sweden</span> - Odds 2.09</li>
																	<li><b>Partai 2</b> : <span class="text-red">Belgium</span> vs <span class="text-blue">Panama</span> - Over <code>2.5-3</code> - Odds 1.89</li>
																	<li><b>Partai 3</b> : <span class="text-red">England</span> vs <span class="text-blue">Tunisia</span> - 1 <span class="text-red">England</span> - Odds 1.44</li>
																</ul>
																<br />
																<p>Total kali Odds dari 3 tim akan dikurangi 1 sebelum dikalikan dengan nilai taruhan.</p>
																<ul class="list-unstyled">
																	<li><code>( ( <b>Odds Partai 1</b> x <b>Odds Partai 2</b> x <b>Odds Partai 3</b> ) - 1 ) x <b>Nilai Taruhan</b></code></li>
																	<li>= ( <code>( 2.09 x 1.89 x 1.44 )</code> - 1 ) x 100</li>
																	<li>= <code>( 5.688 - 1 )</code> x 100</li>
																	<li>= 4.688 x 100</li>
																	<li>Kamu menang taruhan sebesar 468.8 <small class="text-muted">(dalam ribuan rupiah)</small> atau sekitar Rp 468.800</li>
																</ul>
																<br />
																<span class="text-muted">Catatan : Angka yang merah dihitung terlebih dahulu.</span>
															</div>
														</div>
														<h3>Cara Menghitung Taruhan Mix Parlay Menang atau Kalah Setengah (&frac12;)</h3>
														<dl>
															<dt>Perhitungan Mix Parlay Menang Setengah (&frac12;)</dt>
															<dd>
																<p>Apabila dalam partai yang kita ambil ada Menang Setengah (&frac12;), Odds Mix Parlay akan dihitung ulang dengan mengurangi Odds pada tim yang kalah tersebut dengan rumus :
																<br />
																<code>Odds Awal kurang 1, kemudian hasil dibagi 2 lalu ditambahkan 1 sebelum dikalikan kembali dengan Odds Partai lainnya.</code></p>
																<p>Contoh Rumus : <code>( ( ( (( <b>Odds Partai Kalah</b> - 1 ) / 2 ) + 1 ) x <b>Odds Partai Menang</b> ) - 1 ) x <b>Nilai Taruhan</b></code></p>
															</dd>
															<div class="card">
																<div class="card-body">
																	<p>Contoh perhitungan apabila yang Menang Setengah (&frac12;) adalah tim <b>Partai 1</b> : <span class="text-red">Sweden</span> vs <span class="text-blue">Korea Republic</span> - Odds 2.09</p>
																	<ul class="list-unstyled">
																		<li><code>( ( ((( <b>Odds Partai 1</b> - 1 ) / 2 ) + 1 ) x <b>Odds Partai 2</b> x <b>Odds Partai 3</b> ) - 1 ) x <b>Nilai Taruhan</b></code></li>
																		<li>= ( ( ( ( <code>( 2.09 - 1 )</code> / 2) + 1) x 1.89 x 1.44 ) - 1 ) x 100</li>
																		<li>= ( ( ( <code>( 1.09 / 2 )</code> + 1) x 1.89 x 1.44 ) - 1 ) x 100</li>
																		<li>= ( ( <code>( 0.545 + 1 )</code> x 1.89 x 1.44 ) - 1 ) x 100</li>
																		<li>= ( <code>( 1.545 x 1.89 x 1.44 )</code> - 1 ) x 100</li>
																		<li>= <code>( 4.204 - 1 )</code> x 100</li>
																		<li>= 3.204 x 100</li>
																		<li>Kamu menang taruhan sebesar 320.4 <small class="text-muted">(dalam ribuan rupiah)</small> atau sekitar Rp 320.400</li>
																	</ul>
																	<br />
																	<span class="text-muted">Catatan : Angka yang merah dihitung terlebih dahulu. Semakin banyak partai yang dimainkan, taruhan yang akan kamu menangkan bisa berlipat2.</span>
																</div>
															</div>
														</dl>
														<dl>
															<dt>Perhitungan Mix Parlay Kalah Setengah (&frac12;)</dt>
															<dd>
																Apabila dalam partai yang kita ambil ada Kalah Setengah (½), Odds Mix Parlay akan dihitung ulang dengan mengurangi Odds pada tim yang kalah tersebut dengan rumus :
																<br />
																<code>Total perkalian partai yang menang akan dibagi dengan partai yang kalah setelah partai yang kalah dikali dengan 2, kemudian hasil pembagian tersebut akan dikurangi 1 sebelum dikalikan kembali dengan Nilai Taruhan kamu.</code></p>
																<p>Contoh Rumus : <code>( <b>Odds Partai Menang</b> / ( <b>Odds Partai Kalah</b> x 2 ) ) - 1 ) x <b>Nilai Taruhan</b></code></p>
															</dd>
															<div class="card">
																<div class="card-body">
																	<p>Contoh perhitungan apabila yang Kalah Setengah (&frac12;) adalah tim <b>Partai 3</b> : <span class="text-red">England</span> vs <span class="text-blue">Tunisia</span> - 1 <span class="text-red">England</span> - Odds 1.44</p>
																	<ul class="list-unstyled">
																		<li><code>( ( <b>Odds Partai 1</b> x <b>Odds Partai 2</b> ) / ( <b>Odds Partai 3</b> x 2 ) ) - 1 ) x <b>Nilai Taruhan</b></code></li>
																		<li>= ( ( <code>( 2.09 x 1.89 )</code> / <code>( 1.44 x 2 )</code> ) - 1 ) x 100</li>
																		<li>= ( <code>( 3.95 / 2.88 )</code> - 1 ) x 100</li>
																		<li>= <code>( 1.37 - 1 )</code> x 100</li>
																		<li>= 0.37 x 100</li>
																		<li>Kamu menang taruhan sebesar 37 <small class="text-muted">(dalam ribuan rupiah)</small> atau sekitar Rp 37.000</li>
																	</ul>
																	<br />
																	<span class="text-muted">Catatan : Angka yang merah dihitung terlebih dahulu.
																	Semakin banyak partai yang dimainkan, taruhan yang akan kamu menangkan bisa berlipat2.</span>
																</div>
															</div>
														</dl>
													</div>
													<div class="tab-pane fade" id="panduan-bola-correct-score" role="tabpanel" aria-labelledby="panduan-bola-6">
														<h2>Panduan Cara Bermain Taruhan Judi Bola Online Correct Score <small class="text-muted">(Skor Pertandingan)</small></h2>
														<hr />
														<figure>
															<img src="/assets/images/bola/panduan-bola-correct-score.png" alt="cara main judi bola, mix parlay, judi bola online, panduan bola, belajar judi bola, situs judi bola" title="Panduan Taruhan Bola Correct Score">
															<figcaption>Gambar 3.0 - Gambar Untuk Penjelasan Taruhan Correct Score</figcaption>
														</figure>
														<p>Taruhan <strong>Correct Score</strong> : Taruhan untuk menebak skor pertandingan dengan tepat hingga pertandingan berakhir, tidak ada pengaruh pasaran atau fur - an.<br />Untuk FH <span class="text-muted">(First Half)</span> Correct Score hanya berlaku untuk pertandingan babak pertama saja ( 1 x 45 menit+ ).</p>
														<h3>Cara Bermain Taruhan Judi Bola Online Correct Score & First Half Correct Score</h3>
														<p>Contoh pertandingan : <span class="text-red">Belgium</span> vs <span class="text-blue">Panama</span><br />
														Untuk skor tidak bisa di bolak - balik, sesuai dengan tim yang anda pasang seperti contoh</p>
														<div class="card">
															<div class="card-body">
																<p>Contoh Tebakan Correct Score Full Time, lihat gambar dengan kotak warna <span class="text-red">Merah</span>.</p>
																<ul>
																	<li>Skor <span class="text-red">Belgium</span> 1 : 0 <span class="text-blue">Panama</span>, Odds 4.7</li>
																	<li>Skor <span class="text-red">Belgium</span> 3 : 2 <span class="text-blue">Panama</span>, Odds 46</li>
																	<li>Skor <span class="text-red">Belgium</span> 4 : 3 <span class="text-blue">Panama</span>, Odds 220</li>
																</ul>
																<br />
																<p>Contoh Tebakan First Half Correct Score, lihat gambar dengan kotak warna <span class="text-blue">Biru</span>.</p>
																<ul>
																	<li>Skor <span class="text-red">Belgium</span> 0 : 2 <span class="text-blue">Panama</span>, Odds 60</li>
																	<li>Skor <span class="text-red">Belgium</span> 3 : 0 <span class="text-blue">Panama</span>, Odds 40</li>
																	<li>Skor <span class="text-red">Belgium</span> 3 : 2 <span class="text-blue">Panama</span>, Odds 225</li>
																</ul>
															</div>
														</div>
														<h3>Cara Menghitung Taruhan Judi Bola Online Correct Score & First Half Correct Score</h3>
														<p>Cara menghitung Correct Score adalah nilai Odds dikurangi 1 lalu dikalikan dengan nilai taruhan kamu.
															<br/ >
															Rumus : <code>(Odds - 1) x Nilai Taruhan</code>
														</p>
														<p>Contoh pertandingan : <span class="text-red">Belgium</span> vs <span class="text-blue">Panama</span><br />
															Nilai Taruhan Kamu : 100 <small class="text-muted">(dalam ribuan rupiah)</small></p>
														<div class="card">
															<div class="card-body">
																<p>Contoh Tebakan Correct Score Full Time, lihat gambar dengan kotak warna <span class="text-red">Merah</span>.</p>
																<ul>
																	<li>Skor <span class="text-red">Belgium</span> 1 : 0 <span class="text-blue">Panama</span>, Odds 4.7<br />
																	Skor Tebakan Benar ( Menang )<br />
																	<code>= ( 4.7 - 1 ) x 100</code><br />
																	<code>= 3.7 x 100</code><br />
																	Kamu Menang 370 <small class="text-muted">(dalam ribuan rupiah)</small> atau Rp 370.000</li>
																	<br />
																	<li>Skor <span class="text-red">Belgium</span> 3 : 2 <span class="text-blue">Panama</span>, Odds 46<br />
																		Skor Tebakan Benar ( Menang )<br />
																	<code>= ( 46 - 1 ) x 100</code><br />
																	<code>= 45 x 100</code><br />
																	Kamu Menang 4.500 <small class="text-muted">(dalam ribuan rupiah)</small> atau Rp 4.500.000</li>
																	<br />
																	<li>Skor <span class="text-red">Belgium</span> 4 : 3 <span class="text-blue">Panama</span>, Odds 220<br />
																	Skor Tebakan Benar ( Menang )<br />
																	<code>= ( 220 - 1 ) x 100</code><br />
																	<code>= 219 x 100</code><br />
																	Kamu Menang 21.900 <small class="text-muted">(dalam ribuan rupiah)</small> atau Rp 21.900.000</li>
																</ul>
																<br />
																<p>Contoh Tebakan First Half Correct Score, lihat gambar dengan kotak warna <span class="text-blue">Biru</span>.</p>
																<ul>
																	<li>Skor <span class="text-red">Belgium</span> 0 : 2 <span class="text-blue">Panama</span>, Odds 60<br />
																	Skor Tebakan Benar ( Menang )<br />
																	<code>= ( 60 - 1 ) x 100</code><br />
																	<code>= 59 x 100</code><br />
																	Kamu Menang 5.900 <small class="text-muted">(dalam ribuan rupiah)</small> atau Rp 5.900.000</li>
																	<br />
																	<li>Skor <span class="text-red">Belgium</span> 3 : 0 <span class="text-blue">Panama</span>, Odds 40<br />
																	Skor Tebakan Benar ( Menang )<br />
																	<code>= ( 40 - 1 ) x 100</code><br />
																	<code>= 39 x 100</code><br />
																	Kamu Menang 3.900 <small class="text-muted">(dalam ribuan rupiah)</small> atau Rp 3.900.000</li>
																	<br />
																	<li>Skor <span class="text-red">Belgium</span> 3 : 2 <span class="text-blue">Panama</span>, Odds 225<br />
																	Skor Tebakan Benar ( Menang )<br />
																	<code>= ( 225 - 1 ) x 100</code><br />
																	<code>= 224 x 100</code><br />
																	Kamu Menang 22.400 <small class="text-muted">(dalam ribuan rupiah)</small> atau Rp 22.400.000</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane fade show" id="panduan-casino" role="tabpanel" aria-labelledby="nav-panduan-casino">
										<div class="row">
											<div class="col-3">
												<div class="nav flex-column nav-pills" id="side-panduan-baccarat" role="tablist" aria-orientation="vertical">
													<a class="nav-link active" id="panduan-baccarat" data-toggle="pill" href="#panduan-cara-main-baccarat" role="tab" aria-controls="panduan-cara-main-baccarat" aria-selected="true">Cara Main Baccarat</a>
													<a class="nav-link" id="panduan-blackjack" data-toggle="pill" href="#panduan-cara-main-blackjack" role="tab" aria-controls="panduan-cara-main-blackjack" aria-selected="false">Cara Main Black Jack</a>
													<a class="nav-link" id="panduan-roullete" data-toggle="pill" href="#panduan-cara-main-roullete" role="tab" aria-controls="panduan-cara-main-roullete" aria-selected="false">Cara Main Roullete</a>
													<a class="nav-link" id="panduan-sicbo" data-toggle="pill" href="#panduan-cara-main-sicbo" role="tab" aria-controls="panduan-cara-main-sicbo" aria-selected="false">Cara Main Sicbo</a>
												</div>
											</div>
											<div class="col-9">
												<div class="tab-content" id="panduan-bola-tabContent">
													<div class="tab-pane fade show active" id="panduan-cara-main-baccarat" role="tabpanel" aria-labelledby="panduan-baccarat">
														<h2>Cara Bermain Baccarat</h2>
														<hr />
														<figure>
															<img src="/assets/images/casino/baccarat.jpg" alt="cara main baccarat, banker, player, tie, belajar judi baccarat, situs judi bola, casino online" title="Panduan Cara Main Baccarat">
															<figcaption>Gambar 4.0 - Gambar Contoh Meja Baccarat, Untuk Setiap Casino Berbeda</figcaption>
														</figure>
														<p><strong>Baccarat</strong> adalah permainan kartu dimana kamu akan membandingkan nilai kartu antara sisi Banker dan sisi Player. Tujuan nya adalah untuk menebak sisi mana yang akan mempunyai kartu yang lebih besar, kamu juga bisa memilih untuk taruhan Tie atau Seri.</p>
														<p>Kamu akan diberikan waktu untuk memilih sebelum kartu dibagikan, setelah kamu atau pemain lain memilih maka kartu akan dibagikan untuk Banker dan Player masing - masing 2 buah kartu secara tertutup hingga maksimal 3 kartu setelah 2 kartu utama dibuka. Pemain dengan nilai taruhan terbesar di setiap sisi bisa diberikan kesempatan untuk membuka kartu sesuai dengan sisi pilihan nya.</p>
														<figure>
															<img src="/assets/images/casino/kartu-baccarat.png" alt="cara main baccarat, banker, player, tie, belajar judi baccarat, situs judi bola, casino online" title="Panduan Cara Main Baccarat">
															<figcaption>Gambar 4.1 - Gambar Untuk Penjelasan Nilai Kartu Baccarat</figcaption>
														</figure>
														<p>Untuk nilai kartu paling kecil adalah 1 dan paling besar adalah 9, Berikut ini beberapa nilai2 kartu yang ada di Baccarat :</p>
														<div class="card">
															<div class="card-body">
																<ul class="list-unstyled">
																	<li><code>2 - 9</code> : Sesuai dengan angka pada kartu.</li>
																	<li><code>10, J, Q, K</code> : Nilai nya 0</li>
																	<li><code>AS <small class="text-muted">(Ace)</small></code> : Nilai nya 1</li>
																</ul>
																<br />
																<p>Untuk penjumlahan 2 atau 3 buah kartu nilai terbesar adalah 9 dan apabila nilai kartu mencapai 10 atau lebih maka akan dikurangi 10, contoh :</p>
																<ul>
																	<li><code>9</code> + <code>1</code> = <code>10</code> ( Nilai Kartu = 0 )</li>
																	<li><code>K</code> + <code>J</code> = <code>0</code> ( Nilai Kartu = 0 )</li>
																	<li><code>9</code> + <code>7</code> = <code>17</code> ( Nilai Kartu = 7 )</li>
																	<li><code>9</code> + <code>7</code> + <code>3</code> = <code>19</code> ( Nilai Kartu = 9 )</li>
																	<li><code>9</code> + <code>K</code> = <code>9</code> ( Nilai Kartu = 9 )</li>
																</ul>
															</div>
														</div>
														<br />
														<p>Dalam permainan Baccarat apabila total nilai 2 buah kartu (Murni) milik Banker atau Player dijumlahkan angka nya mencapai 8 atau 9, maka tidak ada penambahan kartu ketiga karena biasanya disebut dengan <b>Natural Win</b> atau <b>Menang Murni</b>. Apabila Banker dan Player memiliki kartu yang sama, maka akan dianggap Tie atau Seri.</p>
														<p>Berikut ini peraturan untuk penambahan kartu ketiga.</p>
														<dl>
															<dt>1. Peraturan Penambahan Kartu Ketiga Untuk Player :</dt>
															<p>Untuk penambahan kartu ketiga Player hanya ada 1 peraturan lain yang perlu diperhatikan.</p>
															<dd>
																<div class="card">
																	<div class="card-body">
																		<ol>
																			<li>Jumlah 2 kartu Player sama dengan 5 atau kurang dari 5.</li>
																		</ol>
																	</div>
																</div>
															</dd>
														</dl>
														<dl>
															<dt>2. Peraturan Penambahan Kartu Ketiga Untuk Banker :</dt>
															<br />
															<figure>
																<img src="/assets/images/casino/tabel-baccarat.png" alt="cara main baccarat, banker, player, tie, belajar judi baccarat, situs judi bola, casino online" title="Panduan Cara Main Baccarat">
																<figcaption>Gambar 4.2 - Gambar Peraturan Kartu Ketiga untuk Banker. H = Hit, S = Stand</figcaption>
															</figure>
															<p>Untuk penambahan kartu ketiga Banker, ada huruf H <small class="text-muted">(Hit)</small> untuk mengambil kartu ketiga dan S <small class="text-muted">(Stand)</small> untuk tidak mengambil kartu, ada beberapa peraturan yang perlu diperhatikan.</p>
															<dd>
																<div class="card">
																	<div class="card-body">
																		<ol>
																			<li>Jumlah 2 kartu Banker hanya mencapai 0 sampai 2</li>
																			<li>Jumlah 2 kartu Banker adalah 3 dan jumlah nilai kartu ketiga Player bukan merupakan angka 8.</li>
																			<li>Jumlah 2 kartu Banker adalah 4 dan jumlah nilai kartu ketiga Player bukan merupakan angka 0, 1 , 8, 9.</li>
																			<li>Jumlah 2 kartu Banker adalah 5 dan jumlah nilai kartu ketiga Player bukan merupakan angka 4, 5 , 6, 7.</li>
																			<li>Jumlah 2 kartu Banker adalah 6 dan jumlah nilai kartu ketiga Player bukan merupakan angka 6 atau 7.</li>
																		</ol>	
																	</div>
																</div>
															</dd>
														</dl>
														<p>Diluar peraturan diatas, Banker maupun Player tidak akan mengambil tambahan kartu ketiga.</p>
													</div>

													<div class="tab-pane fade" id="panduan-cara-main-blackjack" role="tabpanel" aria-labelledby="panduan-blackjack">
														<h2>Cara Bermain Black Jack</h2>
														<hr />
														<figure>
															<img src="/assets/images/casino/blackjack.jpg" alt="cara main blackjack, banker, player, tie, belajar judi blackjack, situs judi casino, casino online" title="Panduan Cara Main Black Jack">
															<figcaption>Gambar 4.3 - Gambar Simulasi Contoh Meja Black Jack</figcaption>
														</figure>
														<p><strong>Black Jack</strong> merupakan permainan kartu untuk melawan Bandar dengan jumlah nilai kartu yang kamu pegang harus mendekati atau mencapai angka 21, untuk menang jumlah nilai kartu kamu harus lebih besar dari Bandar apabila nilai kartu kita lebih dari angka 21 maka akan dianggap Busted atau kalah.</p>
														<p>Apabila kamu mendapatkan 2 buah kartu pertama dengan total nilai 21 ( <code>AS</code> dengan nilai 11 dan <code>10</code> ) atau biasa disebut dengan <b>Black Jack</b>, maka kamu tidak bisa kalah dan nilai kemenangan kamu akan dikali 1.5 dari taruhan kamu. Tapi apabila Bandar juga memiliki 2 kartu pertama dengan total nilai 21 maka akan dianggap Push atau seri.</p>
														<p>Nilai - nilai kartu yang ada di Black Jack :</p>
														<div class="card">
															<div class="card-body">
																<ul class="list-unstyled">
																	<li><code>2 - 10</code> : Sesuai angka pada kartu.</li>
																	<li><code>J, Q, K</code> : Nilai kartu 10</li>
																	<li>Untuk kartu <code>AS <small class="text-muted">(Ace)</small></code> merupakan kartu special di Black Jack dengan dua nilai yaitu 1 atau 11. Contoh : Apabila total 2 kartu kamu nilai nya 15 dan setelah kartu ke 3 diambil mendapat AS, total nilai kartu kamu bisa dihitung menjadi 16 saja.</li>
																</ul>
															</div>
														</div>
														<h3>Istilah Dalam Permainan Black Jack</h3>
														<p>Setelah mengetahui tentang cara main Black Jack, ada istilah - istilah dalam Black Jack yang perlu kamu ketahui.</p>
														<dl class="row">
															<dt class="col-2">Black Jack</dt>
															<dd class="col-10">Istilah lain dari pemain yang mendapatkan kartu 21 hanya dengan 2 kartu. Ketika seorang pemain mendapatkan Black Jack, maka dealer tidak akan membagikan kartu lagi dan langsung menganggap pemain tersebut sebagai pemenang. Terkecuali, ada pemain lain yang mendapatkan Black Jack maka dianggap Push atau Seri.</dd>

															<dt class="col-2">Bust</dt>
															<dd class="col-10">Istilah yang digunakan apabila mendapatkan kartu dengan total nilai lebih dari 21 ( baik untuk pemain maupun bandar ). Bagi yang mendapatkan kartu dengan nilai lebih dari 21 atau Bust maka akan dianggap kalah.</dd>

															<dt class="col-2">Double</dt>
															<dd class="col-10">Sesuai dengan istilahnya, double digunakan untuk menggandakan taruhan awal menjadi 2x lipat dengan hanya menarik 1 kartu tambahan ( Pemain lain tidak diizinkan untuk menarik lebih dari 1 kartu tambahan ). Pilihan untuk double ini akan muncul saat total nilai 2 kartu pemain pertama bernilai 11.</dd>

															<dt class="col-2">Hit</dt>
															<dd class="col-10">Hit digunakan untuk menarik kartu tambahan.</dd>

															<dt class="col-2">Stand</dt>
															<dd class="col-10">Tidak minta kartu tambahan.</dd>

															<dt class="col-2">Asuransi</dt>
															<dd class="col-10">Jika kartu pertama yang di dapat oleh Bandar adalah AS, maka kamu boleh membeli asuransi yang nilai nya 50% dari taruhan kamu, kamu bisa membeli asuransi apabila kamu yakin kartu bandar yang tertutup bernilai 10. Apabila kamu kalah, maka kamu hanya perlu bayar 50% dari taruhan kamu. Apabila bandar tidak mendapat Black Jack maka permainan akan dilanjutkan dan asuransi kamu akan hilang.</dd>

															<dt class="col-2">Split</dt>
															<dd class="col-10">Kamu bisa meminta untuk membagi kartu menjadi 2 pasang taruhan dan menggandakan taruhan kamu. Kedua kartu yang ingin dipisahkan harus bernilai sama. Pada kondisi ini, kartu AS akan dihitung dengan nilai 10 yang arti nya kamu tidak bisa mendapatkan Black Jack murni 2 kartu atau Bandar tidak mau bayar 3 banding 2. Namun kamu masih bisa mengalahkan pemain lain dengan kartu dibawah 20. Apabila pemain memisahkan kartu AS dan AS maka pemain hanya akan menambah satu kartu untuk setiap kartu AS.</dd>

															<dt class="col-2">Surrender</dt>
															<dd class="col-10">Pada banyak casino, kamu bisa menentukan untuk menyerah sebelum bermain atau setelah menebak kartu dealer memiliki Black Jack atau tidak. Apabila kamu menyerah, maka kamu akan kehilangan setengah dari taruhan kamu. Untuk opsi menyerah ini dapat dilakukan apabila dealer menunjukkan kartu 9 sampai AS dan kamu memiliki kartu 5 - 7 atau kartu 12 - 16. <br />
															Saat Bandar memiliki kartu AS, maka dia akan langsung membuka kartu berikutnya untuk melihat apakah dia mendapatkan Black Jack atau bukan.</dd>
														</dl>
													</div>

													<div class="tab-pane fade" id="panduan-cara-main-roullete" role="tabpanel" aria-labelledby="panduan-roullete">
														<h2>Cara Bermain Roullete</h2>
														<hr />
														<figure>
															<img src="/assets/images/casino/roullete.jpg" alt="cara main roullete, banker, player, tie, belajar judi roullete, situs judi bola, casino online" title="Panduan Cara Main Roullete">
															<figcaption>Gambar 4.4 - Gambar Simulasi Contoh Meja Roullete</figcaption>
														</figure>
														<p><strong>Roullete</strong> merupakan salah satu jenis permainan Casino yang mengutamakan perhitungan dan intinya tidak terlepas dari keberuntungan anda. Permainan ini menggunakan sebuah bola besi kecil yang di putas dalam meja yang sudah memiliki tempat sandaran untuk bola tersebut berhenti yang terdiri dari warna dan angka.</p>
														<h3>Cara Main dan Hitung Taruhan Judi Online Roullete</h3>
														<p>Cara untuk bermain roullete juga cukup mudah, kamu cukup menebak angka, warna, besar atau kecil, genap atau ganjil.</p>
														<dl class="row">
															<dt class="col-2">Warna</dt>
															<dd class="col-10">Untuk warna yang ada di putaran Roullete ada 3 yaitu Merah, Hitam dan Hijau. Untuk perhitungan pembayaran warna Merah dan Hitam adalah 1 : 1, sedangkan untuk pembayaran warna Hijau adalah 1 : 35.</dd>

															<dt class="col-2">Angka</dt>
															<dd class="col-10">Taruhan untuk menebak bola akan berada di angka yang ditebak. <br />
															Untuk tebak angka sama seperti warna, bayaran nya 1 : 35 apabila menang.</dd>

															<dt class="col-2">Odd <small class="text-muted">(Ganjil)</small></dt>
															<dd class="col-10">Taruhan untuk menebak posisi bola akan berada di Angka Ganjil<br />
															Angka Ganjil : 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35. </dd>

															<dt class="col-2">Even <small class="text-muted">(Genap)</small></dt>
															<dd class="col-10">Taruhan untuk menebak posisi bola akan berada di Angka Genap<br />
															Angka Genap : 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36.</dd>

															<dt class="col-2">Kecil <small class="text-muted">(1-19)</small></dt>
															<dd class="col-10">Taruhan untuk menebak posisi bola akan berada di Angka Kecil.<br />
															Angka Kecil : 1 - 19.</dd>

															<dt class="col-2">Besar <small class="text-muted">(19-36)</small></dt>
															<dd class="col-10">Taruhan untuk menebak posisi bola akan berada di Angka Besar.<br />
															Angka Besar : 19 - 36.</dd>

															<dt class="col-2">1st 12</dt>
															<dd class="col-10">Taruhan untuk menebak posisi bola akan berada di antara angka 1 sampai 12.<br />
															Untuk 1st 12 akan mendapatkan bayaran 1 : 2 apabila menang.</dd>

															<dt class="col-2">2nd 12</dt>
															<dd class="col-10">Taruhan untuk menebak posisi bola akan berada di antara angka 13 sampai 24.<br />
															Untuk 1st 12 akan mendapatkan bayaran 1 : 2 apabila menang.</dd>

															<dt class="col-2">3rd 12</dt>
															<dd class="col-10">Taruhan untuk menebak posisi bola akan berada di antara angka 25 sampai 36.<br />
															Untuk 1st 12 akan mendapatkan bayaran 1 : 2 apabila menang.</dd>

															<dt class="col-2">2 to 1</dt>
															<dd class="col-10">Taruhan baris, di samping setiap baris terdapat 2 to 1 yang digunakan untuk menebak posisi bola akan berada di antara angka yang ada di baris tersebut. <br />
															Untuk 2 to 1 akan mendapatkan bayaran 1 : 2 apabila menang.</dd>

														</dl>
														</dl>
													</div>

													<div class="tab-pane fade" id="panduan-cara-main-sicbo" role="tabpanel" aria-labelledby="panduan-sicbo">
														<h2>Cara Bermain Sicbo atau Dadu</h2>
														<hr />
														<figure>
															<img src="/assets/images/casino/sicbo.jpg" alt="cara main roullete, banker, player, tie, belajar judi roullete, situs judi bola, casino online" title="Panduan Cara Main Roullete">
															<figcaption>Gambar 4.5 - Gambar Simulasi Contoh Dadu Sicbo</figcaption>
														</figure>
														<p><strong>Sicbo</strong> merupakan permainan casino yang menggunakan 3 buah dadu untuk bertaruh. Cara untuk bertaruh Sicbo ini hampir sama dengan Roullete yang membedakan hanya alat untuk bermain dan tidak ada warna dalam Sicbo.</p>
														<dl class="row">
															<dt class="col-3">Ganjil / Genap</dt>
															<dd class="col-9">Sesuai dengan taruhan lainnya, taruhan ini untuk menebak jumlah total 3 mata dadu apakah bernilai ganjil atau pun genap.</dd>

															<dt class="col-3">Total Tiga Dadu</dt>
															<dd class="col-9">Taruhan untuk menebak jumlah nilai total 3 mata dadu ( Mulai dari 4 hingga 17 ).</dd>

															<dt class="col-3">Angka Tunggal</dt>
															<dd class="col-9">Taruhan untuk menebak angka yang muncul pada mata dadu antara 1 sampai 6, jika angka yang dipilih tidak muncul di salah satu dari 3 dadu maka pemain di anggap kalah. Jika angka yang dipilih muncul, maka bayaran yang di dapat sesuai dengan seberapa sering angka yang dipilih muncul di mata dadu (1x maka 1:1, 2x maka 1:2 dan 3x maka 1:3).</dd>

															<dt class="col-3">Taruhan Ganda</dt>
															<dd class="col-9">Taruhan untuk menebak minimal 2 buah dadu yang akan muncul memiliki nilai yang sama<br />
															Misal Dadu : 1-1, 2-2, 3-3, 4-4, 5-5, 6-6</dd>

															<dt class="col-3">Taruhan Triple</dt>
															<dd class="col-9">Taruhan untuk menebak 3 buah dadu atau semua dadu yang akan muncul memiliki nilai yang sama<br />
															Misal Dadu : 1-1-1, 2-2-2, 3-3-3, 4-4-4, 5-5-5, 6-6-6</dd>
														</dl>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection