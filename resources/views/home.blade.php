﻿@extends('layouts.app')
@section('contentdescription')
	<?php echo "T4SPORT - Situs Judi Bola & Casino Online Terbesar dan Terpercaya.";?>
@endsection
@section('content')
<?php
if(Session::has('id'))
{
   Redirect::to('profil')->send();
}
?>
<div id="site-content" class="site-content">
	<?php $value = Request::session()->get('id', 'default'); 
		$msg = Request::session()->pull('msg', 'default');
		if($msg != 'default')
		{        
			echo '<div class="success" id="success-home">'.$msg.'</div>';
		}      
    ?>
	<div class="inner inner-zero wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="home-page-container">
				<div class="home-page-content">
					<section class="section unwrapper js-unwrapper" id="web-slider">
						<div class="slider-heros js-slider-heros is-hidden">							
							<div class="slider-hero">
								<div class="slider-hero-image">
									<img class="image" title="T4SPORT - World Cup 2018" alt="world cup 2018, piala dunia 2018, piala dunia russia, tim 16 besar, 16 besar piala dunia, final piala dunia, semi final world cup 2018, pemenang piala dunia, prediksi piala dunia" src="/uploads/slider/tim-world-cup-russia-2018.jpg">
								</div>
							</div>							
							<div class="slider-hero">
								<div class="slider-hero-image">
									<img class="image" title="T4SPORT - Promo Bonus Deposit 20%" alt="bonus deposit, gratis chip, freebet, bonus tambahan deposit, taruhan bola online, gratis taruhan, bonus deposit terbesar" src="/uploads/slider/bonus-deposit-terbesar-20-persen.jpg">
								</div>
							</div>
							<div class="slider-hero">
								<div class="slider-hero-image">
									<img class="image" title="T4SPORT - Promo Komisi Sportsbook 1%" alt="situs judi bola, judi bola online, judi bola, taruhan bola, bonus deposit 20%, bonus depo bola, gratis kredit bola, freebet" src="/uploads/slider/komisi-turnover-judi-bola-terbesar-tertinggi.jpg">
								</div>
							</div>
							<div class="slider-hero">
								<div class="slider-hero-image">
									<img class="image" title="T4SPORT - Promo Komisi Rollingan Casino 1%" alt="rollingan casino, casino, turnover terbesar, turn over casino, cashback casino, komisi casino, cara main casino" src="/uploads/slider/komisi-turnover-casino-terbesar-tertinggi.jpg">
								</div>
							</div>
							<div class="slider-hero">
								<div class="slider-hero-image">
									<img class="image" title="T4SPORT - Promo Cashback Turnover Poker 20%" alt="cashback poker, poker online, judi poker, agen poker, bandar poker, situs poker, situs judi poker, cara main poker" src="/uploads/slider/cashback-poker-terbesar-tertinggi.jpg">
								</div>
							</div>
						</div>
					</section>

					<section class="section" id="web-games">
						<h2 class="heading">Games</h2>
						<div class="game-overviews">
							<div class="game-overview">
								<div class="game-overview-art">
									<img src="/uploads/games/agen-judi-bola-sbobet-maxbet-mr8.jpg" alt="agen bola, situs judi bola, situs bola, bandar bola, taruhan bola, tempat taruhan bola, cara main judi bola online, sbobet, maxbet, mr8asia" title="T4SPORT - Judi Bola Online" class="image">
									<h3 class="name">Sportsbook</h3>
								</div>
								<div class="game-overview-product">
									<ul class="products">
										<li class="product"><a href="http://www.jimwater.com" target="_blank">SBOBET</a></li>
										<li class="product"><a href="http://www.maxbet.com" target="_blank">MAXBET</a></li>
										<li class="product"><a href="http://www.mr8asia.com" target="_blank">MR8</a></li>
									</ul>
								</div>
							</div>
							<div class="game-overview">
								<div class="game-overview-art">
									<img src="/uploads/games/agen-casino-online-sbobet-maxbet.jpg" alt="casino, agen casino, bandar casino, cara main casino online, situs casino, judi casino, sbobet, maxbet" title="T4SPORT - Kasino Online" class="image">
									<h3 class="name">Casino</h3>
								</div>
								<div class="game-overview-product">
									<ul class="products">
										<li class="product"><a href="http://www.jimwater.com" target="_blank">SBOBET-CASINO</a></li>
										<li class="product"><a href="http://www.maxbet.com" target="_blank">MAXBET</a></li>
									</ul>
								</div>
							</div>
							<div class="game-overview">
								<div class="game-overview-art">
									<img src="/uploads/games/agen-poker-online-terbesar-terpercaya.jpg" alt="agen poker, poker online, poker, situs judi poker online, main poker, website poker, taruhan poker" title="T4SPORT - Poker Online" class="image">
									<h3 class="name">Poker</h3>
								</div>
								<div class="game-overview-product">
									<ul class="products">
										<li class="product"><a href="http://8mpoker.com" target="_blank">8MPOKER</a></li>
									</ul>
								</div>
							</div>
							<div class="game-overview">
								<div class="game-overview-art">
									<img src="/uploads/games/agen-cock-fight-sabung-ayam-online.jpg" alt="sabung ayam, cock fight, judi ayam, taruhan ayam, situs pasang taruhan ayam" title="T4SPORT - Cock Fight Online ( Sabung Ayam )" class="image">
									<h3 class="name">Cock Fight</h3>
								</div>
								<div class="game-overview-product">
									<ul class="products">
										<li class="product"><a href="http://sv388.com" target="_blank">SV388</a></li>
									</ul>
								</div>
							</div>
							<div class="game-overview">
								<div class="game-overview-art">
									<img src="/uploads/games/agen-togel-online-terbesar-terpercaya.jpg" alt="togel, taruhan togel, lottery, judi togel, tebak 4 angka, 3 angka, 2 angka, prediksi togel, 4D, 3D, 2D, agen togel, bandar togel" title="T4SPORT - Togel Online" class="image">
									<h3 class="name">Lottery</h3>
								</div>
								<div class="game-overview-product">
									<ul class="products">
										<li class="product"><a href="#">Coming Soon</a></li>
									</ul>
								</div>
							</div>
							<div class="game-overview">
								<div class="game-overview-art">
									<img src="/uploads/games/agen-bola-tangkas-online-terpercaya.jpg" alt="agen bola tangkas, taruhan tangkas, mickey mouse, bola tangkas online, website tangkas, situs taruhan tangkas, tempat main bola tangkas, website bola tangkas" title="T4SPORT - Bola Tangkas Online" class="image">
									<h3 class="name">Bola Tangkas</h3>
								</div>
								<div class="game-overview-product">
									<ul class="products">
										<li class="product"><a href="http://tangkas365.com" target="_blank">Tangkas365</a></li>		
									</ul>
								</div>
							</div>
						</div>
					</section>

					<section class="section section-alt unwrapper js-unwrapper" id="web-banks">
						<div class="wrapper">
							<h2 class="heading">Metode Transaksi</h2>
							<div class="payment-banks">
								<div class="payment-bank">
									<div class="payment-bank-logo">
										<a class="bank-link" href="https://ibank.klikbca.com/"><button><img class="logo" src="/assets/images/logo-bank-bca.png" alt="BCA"></button></a>
									</div>
									<div class="payment-bank-schedule">
										<p class="legend">Jadwal Bank Offline</p>
										<p class="time">
											<span class="day">Senin – Jumat</span>
											<span class="hour">21:00 – 00:00 WIB</span>
										</p>
										<p class="time">
											<span class="day">Sabtu</span>
											<span class="hour">22:00 – 23:30 WIB</span>
										</p>
										<p class="time">
											<span class="day">Minggu</span>
											<span class="hour">01:30 – 05:00 WIB</span>
										</p>
									</div>
								</div>
								<div class="payment-bank">
									<div class="payment-bank-logo">
										<a class="bank-link" href="https://ib.bankmandiri.co.id/"><button><img class="logo" src="/assets/images/logo-bank-mandiri.png" alt="Mandiri"></button></a>
									</div>
									<div class="payment-bank-schedule">
										<p class="legend">Jadwal Bank Offline</p>
										<p class="time">
											<span class="day">Senin – Minggu</span>
											<span class="hour">23:00 – 05:00 WIB</span>
										</p>
									</div>
								</div>
								<div class="payment-bank">
									<div class="payment-bank-logo">
										<a class="bank-link" href="https://ibank.bni.co.id/"><button><img class="logo" src="/assets/images/logo-bank-bni.png" alt="BNI"></button></a>
									</div>
									<div class="payment-bank-schedule">
										<p class="legend">Jadwal Bank Offline</p>
										<p class="note">Tidak ada offline</p>
									</div>
								</div>
								<div class="payment-bank">
									<div class="payment-bank-logo">
										<a class="bank-link" href="https://ib.bri.co.id/"><button><img class="logo" src="/assets/images/logo-bank-bri.png" alt="BRI"></button></a>
									</div>
									<div class="payment-bank-schedule">
										<p class="legend">Jadwal Bank Offline</p>
										<p class="note">Hubungi Customer Service</p>
									</div>
								</div>
								<div class="payment-bank">
									<div class="payment-bank-logo">
										<a class="bank-link" href="https://www.danamonline.com/"><button><img class="logo" src="/assets/images/logo-bank-danamon.png" alt="DANAMON"></button></a>
									</div>
									<div class="payment-bank-schedule">
										<p class="legend">Jadwal Bank Offline</p>
										<p class="note">Tidak ada offline</p>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection