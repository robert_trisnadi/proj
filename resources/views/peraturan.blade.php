@extends('layouts.app')
@section('contentdescription')
	<?php echo "Halaman peraturan di ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya.";?>
@endsection
@section('content')
<?php
try {

	$peraturan = DB::table("page")->first()->PeraturanPage;	
} catch (Exception $e) {
	$peraturan = null;
}	
?>
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Bantuan</h1>
				</header>

				<div class="page-single-content">
					<div class="help-single">
						@include('include.sidebar')
						<div class="help-single-body">
							<h2 class="title">Peraturan</h2>
							<div class="article-post">
								<?php echo $peraturan;?>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection