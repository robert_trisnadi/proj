@extends('layouts.app')
@section('contentdescription')
	<?php echo "Transfer page dari ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya. Pengguna bisa melakukan transfer antar game yang sudah terdaftar melalui halaman ini.";?>
@endsection
<?php 
use App\SecurityQuestions;
if(!Session::has('id'))
{
   Redirect::to('/')->send();
}
$user = Request::session()->get('id');
$data_vp = DB::table('viewcustomerproductproduct')->where('CustomerID', 
		Request::session()->get('id')->CustomerID)->where('Status',4)
		->select('CustomerProductID','ProductName')
		->get();
$arrproducts = $data_vp->pluck('ProductName', 'CustomerProductID');
?>


@section('content')
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Member Area</h1>
				</header>

				<div class="page-single-content">
					<div class="user-dashboard">

						<div class="user-dashboard-aside">							
							@include('include.usernav')
						</div>						
						<div class="user-dashboard-body">
							@include('include.marq')
							<h2 class="user-dashboard-title">Transfer</h2>							
							<?php 
								$msg = Request::session()->pull('message', 'default');
								if($msg !='default')
									echo '<div class="success">'.$msg.'</div>';
								else
								{
									$msg = Request::session()->pull('err', 'default');
									if($msg !='default')
										echo '<div class="alert">'.$msg.'</div>';
								}
							?>	
							@if(count($errors)>0)	
								<div class="alert">
								@foreach($errors->all() as $error)		
									{{$error}}<?php echo '</br>';?>
								@endforeach
								</div>
							@endif
							<div class="user-dashboard-row">
								{!! Form::open(['url'=> '/transfer/transfer']) !!}
								<div class="form-select form-inline">
									<label for="transfer-from" class="label">Transfer Dari</label>
									{{Form::select('transferdari', $arrproducts,null,['id'=>"transfer-from",'class'=>'input'])}}
								</div>
								<div class="form-select form-inline">
									<label for="transfer-to" class="label">Transfer Ke</label>
									{{Form::select('transferke', $arrproducts,null,['id'=>"transfer-to",'class'=>'input'])}}
								</div>
								<div class="form-text form-inline">
									<label for="transfer-amount" class="label">Jumlah</label>
									{{Form::text('jumlah','',['id'=>"transfer-amount",'class'=>'input', 'placeholder'=>'Jumlah'])}}
								</div>
								<div class="form-submit form-inline">
									{{Form::submit('Transfer Dana',['class'=>"button button-full"])}}
									{{Form::text('membertime','',['hidden'=>'true','id'=>'membertime'])}}
								</div>
								{!! Form::close() !!}
							</div>
							<div class="user-dashboard-row">
								<div class="table-history">
									<div class="table-history-head">
										<p class="title">Data Transfer Anda</p>
									</div>
									<div class="table-history-body table-responsive">
										<table class="table" id="table-data">
										<thead>
											<tr>
												<th scope="col">#</th>				
												<th scope="col">Tanggal Request</th>
												<th scope="col">Dari akun</th>
												<th scope="col">Ke akun</th>
												<th scope="col">Jumlah</th>
												<th scope="col">Status</th>
											</tr>
										</thead>
										<tbody>
										@if(count($data) == 0)
											<tr>
											<th scope="row">-</th>
											<td colspan="4" style="align:center">Tidak ada data</td>
											</tr>
										@endif		
										<?php $counter = 1;?>
										@foreach($data as $d)
											<tr>
												<th scope="row"><?php echo $counter++; ?></th>
												<td>{{$d->RequestDate}}</td>
												<td>{{$d->ProductNameFrom}}</td>
												<td>{{$d->ProductNameTo}}</td>
												<td>Rp.&nbsp;{{number_format($d->Amount,0,',','.')}}</td>
												<?php
												if($d->Status == 3 || $d->Status == 1)//belum claim
													echo "<td class='success-font'>";
												else
													echo "<td>";
												
												echo (Request::session()->get('id')->statuss[$d->Status]->StatusName);
												if($d->Note != '-')
														echo ' - '.$d->Note;
												?>
												</td>
											</tr>		
										@endforeach	
										</tbody>
									</table>
									</div>
									<a class="table-history-more" id="depo-load-more-btn" onclick="LoadMoreData({{$counter}},'table-data',
									<?php 
										if(count($data)>0)
											echo $data[count($data)-1]->TransferlogID;
									?>
									,'transfer')">Load More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection