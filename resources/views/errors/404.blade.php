<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="description" content="@yield('contentdescription')"></meta>
		<meta name="keywords" content="t4sportsbet.com, t4sportsbet, betting, agen betting, casino, promo, sportsbook, sabung ayam, cock fight, bola tangkas, agen judi, agen judi bola">		
		<title>
			404 - Page not found!

		</title>
		<link rel="author" href="humans.txt" />
		<!-- <link rel="stylesheet" href="assets/css/style.css">		 -->
		<link rel="stylesheet" href={{ asset("assets/css/style.css") }}>
		<!-- Custom - Ganti Warna - digabung sama style.css ya-->
		<!-- <link rel="stylesheet" href={{ asset("assets/css/custom.css") }}> -->

		<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" rel="stylesheet">

		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/site.webmanifest">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#28c73d">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">				
		<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>		-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<meta name="msapplication-TileColor" content="#28c73d">
		<meta name="theme-color" content="#ffffff">

		<script src="{{ URL::asset('assets/js/modernizr.js') }}"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script>if (!window.jQuery) { document.write('<script src="jquery.min.js" async><\/script>'); }</script>
		<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>	
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="application/ld+json">
		{
		  "@context": "http://schema.org",
		  "@type": "BreadcrumbList",
		  "itemListElement": [{
		    "@type": "ListItem",
		    "position": 1,
		    "item": {
		      "@id": "http://t4sport.com/home",
		      "name": "Home",
		      "description":"Halaman awal dari T4Sportbet."
		    }
		  },{
		    "@type": "ListItem",
		    "position": 2,
		    "item": {
		      "@id": "http://t4sport.com/registration",
		      "name": "Registration",
		      "description":"Registrasi akun untuk T4Sportbet."
		    }
		  },{
		    "@type": "ListItem",
		    "position": 3,
		    "item": {
		      "@id": "http://t4sport.com/jadwal",
		      "name": "Jadwal",
		      "description":"Jadwal sepak bola di website T4Sportbet."
		    }
		  },{
		    "@type": "ListItem",
		    "position": 4,
		    "item": {
		      "@id": "http://t4sport.com/score",
		      "name": "Live Score",
		      "description":"Live score sepak bola di website T4Sportbet."
		    }
		  }]
		}
		</script>
	</head>

	<body>
		<div id="site-container" class="site-container">
			<header id="site-header" class="site-header" role="banner">
			  <div class="inner">
			    <div class="site-header-brand">
					<div class="wrapper">
						<div class="logo" style="display:block;margin:auto">
							<a href="/" class="logo" rel="nofollow"><img style="display:block;margin:auto" src={{URL::asset('assets/images/t4sport-logo.png')}} alt="Logo T4SPORT.com" title="Logo T4SPORT.com"></a>
						</div>						
					</div>
				</div>
			  </div>
			</header>
			<div style="text-align:center;margin-top:50px;margin-bottom:20px">
				<h2>Halaman tidak ditemukan!</h2>
				<h4><a href="/">Kembali ke halaman utama</a></h4>
			</div>
			<footer id="site-footer" class="site-footer" role="contentinfo" style="margin-top:100px">
				<div class="inner wrapper">					
					<div class="site-footer-brand">
						<ul class="benefits">
							<li class="benefit"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-bonus')}} />
							<title>Beragam Bonus Menarik</title>
							<description>Beragam bonus menarik di website T4sportbet.com.</description>
							</svg> Beragam Bonus Menarik</li>
							<li class="benefit"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-24')}} />
							<title>Customer Service 24 Jam</title>
							<description>T4sportbet.com memberikan customer service selama 24 jam.</description>
							</svg> Customer Service 24 Jam</li>
							<li class="benefit"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-mobile')}} />
							<title>Akses Mobile</title>
							<description>T4sportbet.com dapat dibuka melalui ponsel anda dengan user interface yang bersahabat.</description>
							</svg> Akses Mobile</li>
						</ul>
						<p class="about">T4SPORT.com adalah <b><a href="http://t4sport.com" target="_blank">Situs Agen Bola Terpercaya</a></b> yang menyediakan sarana terbaik dan terbesar untuk judi Bola, Casino, Poker, Sabung Ayam, Bola Tangkas dan Togel Online. Kami memiliki mitra yang sudah sangat terkenal di kawasan Asia Tenggara seperti SBOBET, MAXBET/IBCBET, 368BET, TOGEL4D dan lainnya dengan dukungan sistem pengamanan data kami yang menjamin keamanan dan kerahasiaan data member kami. Untuk anda yang sudah berusia lebih dari 18 tahun, Segeralah bergabung dengan T4SPORT.com</p>
					</div>

					<div class="site-footer-addon">
						<ul class="badges">
							<li class="badge"><img src={{URL::asset('assets/images/badge-responsible.svg')}} alt="Responsible Gambling"></li>
							<li class="badge"><img src={{URL::asset('assets/images/badge-age.svg')}} alt="18+"></li>
						</ul>

						<p class="copyright">Copyright &copy; <script>document.write(new Date().getFullYear());</script> T4Sport.com. All rights reserved.</p>
					</div>

				</div>
			</footer>
		</div>
		<!-- #site-container -->
		<script src="{{ URL::asset('assets/js/vendor.js') }}"></script>
		<script src="{{ URL::asset('assets/js/bundle.js') }}"></script>
	</body>
</html>