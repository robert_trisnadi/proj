@extends('layouts.app')
@section('contentdescription')
	<?php echo "Berita seputar FIFA world cup dari ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya.";?>
@endsection
@section('content')
<?php 
	if($data == null)
	{		
		Redirect::to('/')->send();
	}

$articles = DB::table('category')->where('Status',4)->get();
$subc = DB::table('ArticleCategorySubView')
		->where('Status',4)
		->where(function($q)
		{
			$q->where('StatusCategory',4)
			  ->orWhere('StatusCategory',NULL);
		})
		->where(function($q)
		{
			$q->where('StatusSub',4)
			  ->orWhere('StatusSub',NULL);
		})
		->groupBy("SubcategoryID")
		->select("SubcategoryName","subcategoryid as SubcategoryID","CategoryID","StatusSub as Status")->get();
echo '
		<script>
			document.title = "'.$data->Title.' - Berita | T4SPORT";
		</script>
	';

?>
<div id="page-detail-site-content" class="site-content">
	<div class="inner wrapper" id="page-detail-inner-wrapper">
		<div class="main-content" id="page-detail-main-content" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
			<div class="page-single-container" id="page-detail-single-container">
				<header class="page-single-header">
					<p class="heading">{{$data->category}}&nbsp;/&nbsp;{{isset($data->subcategory)?$data->subcategory:'-'}}</p>
				</header>

				<div class="page-single-content" id="page-detail-single-content">
					<div class="news-post">
						<div class="news-post-head">
							<h1 class="title">{{$data->Title}}</h1>
							<div class="hero">								
								<img class="hero-news-image" title="{{$data->TitleImage}}"  alt="{{$data->AlternateImage}}" src="http://{{ $_SERVER['HTTP_HOST'].'/uploads/' . "{$data->Image}"}}"/>
							</div>
							<p class="meta">{{$data->PublishDate}}</p>
						</div>
						<div class="news-post-body">
							<div class="article-post">
								<?php echo $data->Content;?>
							</div>
						</div>
					</div>
				</div>


				<?php
					$news_bacajuga = DB::table('ArticleCategorySubView')
						->where('Status',4)
						->where(function($q)
						{
							$q->where('StatusCategory',4)
							  ->orWhere('StatusCategory',NULL);
						})
						->where(function($q)
						{
							$q->where('StatusSub',4)
							  ->orWhere('StatusSub',NULL);
						})
						->where('ArticleID','<>',$data->ArticleID)->get();
					$cmax = count($news_bacajuga);
					if($cmax == 0)
						$arr = array();
					else if($cmax < 2)
						$arr = array(0);
					else if($cmax < 3)
						$arr = array(0,1);
					else if($cmax < 4)
						$arr = array(0,1,2);
					else if($cmax >= 4)
					{
						$cmax--;
						$p2 = rand(1,$cmax-1);
						$p1 = rand(0,$p2-1);
						$p3 = rand($p2+1,$cmax);
						$arr = array($p1,$p2,$p3);
					}

					$number = 1;
				?>

				<footer class="page-single-footer" id="page-detail-footer-container">
					<section class="section">
						<h4 class="heading">Baca Juga</h4>
						<div class="news-relates">
						@foreach ($arr as $a)
							<div class="news-relate news-relate-custom" id="page-detail-footer-content-<?php echo $number++;?>">
								<a href="{{route('detailberita', ['idd'=>str_replace(" ","-",$news_bacajuga[$a]->Title)])}}" class="news-relate-link">
									<div class="news-relate-thumbnail">
										<img  title="{{$news_bacajuga[$a]->TitleImage}}"  alt="{{$news_bacajuga[$a]->AlternateImage}}" src="http://{{ $_SERVER['HTTP_HOST'].'/uploads/' . "{$news_bacajuga[$a]->Image}"}}"/>
									</div>
									<div class="news-relate-detail">
										<p class="meta">{{$news_bacajuga[$a]->PublishDate}}</p>
										<p class="categories"><span class="category">{{$news_bacajuga[$a]->CategoryName}}</span> / <span class="category">{{isset($news_bacajuga[$a]->SubcategoryName)?$news_bacajuga[$a]->SubcategoryName:'-'}}</span></p>
										<h2 class="title">{{$news_bacajuga[$a]->Title}}</h2>
									</div>
								</a>
							</div>
						@endforeach
						</div>
					</section>
				</footer>
			</div>
		</div>
		@include('include.newsfeed')
		<!-- </aside> -->

	</div>
</div>
@endsection