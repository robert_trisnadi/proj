@extends('layouts.app')
@section('contentdescription')
	<?php echo "Registration page dari ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya. Daftarkan diri anda melalui halaman ini. Bisa menggunakan referral untuk mendapatkan bonus-bonus tertentu.";?>
@endsection
<?php 
	if(Session::has('id'))
	{
	   Redirect::to('/profil')->send();
	}
	use App\SecurityQuestions;
	use App\Bank;
	$SecurityQuestions = SecurityQuestions::where('Status',1)->get();
	$SecurityQuestions = $SecurityQuestions->pluck('Question','ForgotQuestionsID');

	$bankdata = Bank::where('BankType','Customer')->where('Status',4)->orderBy('BankName', 'asc')->get();
	$bankdata = $bankdata->pluck('BankName','BankID');
?>

@section('content')
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">

			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Daftar Akun Baru</h1>
				</header>		
				
				<div class="page-single-content">
				{!! Form::open(['url'=> '/registration/submit']) !!}
					<div class="user-form">
					<?php 
						$msg = Request::session()->pull('message', 'default');
						if($msg !='default')
							echo '<div class="success">'.$msg.'</div>';
						else
						{
							$msg = Request::session()->pull('err', 'default');
							if($msg !='default')
								echo '<div class="alert">'.$msg.'</div>';
						}
					?>	
					@if(count($errors)>0)	
						<div class="alert">
						@foreach($errors->all() as $error)		
							{{$error}}<?php echo '</br>';?>
						@endforeach
						</div>
					@endif
						<div class="form-text form-inline">						
							<label for="user-register-username" class="label">Username</label>
							{{Form::text('username','',['required'=>'required','id'=>"user-register-username" ,'class'=>'input', 'placeholder'=>'Username'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-register-password" class="label">Password</label>
							{{Form::password('password',['required'=>'required',"id"=>"user-register-password",'class'=>'input', 'placeholder'=>'Password'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-register-password-2" class="label">Konfirmasi Password</label>
							{{Form::password('kpassword',['required'=>'required','id'=>"user-register-password-2" ,'class'=>'input', 'placeholder'=>'Konfirmasi password'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-register-email" class="label">Email</label>
							{{Form::email('email','',['required'=>'required',"id"=>"user-register-email",'class'=>'input','aria-descrebedby'=>'emailHelp', 'placeholder'=>'alamat email'])}} 
						</div>
						<div class="form-text form-inline">
							<label for="user-register-forgot-question" class="label">Pertanyaan Lupa Password</label>
							{{Form::select('plupapassword',$SecurityQuestions,null,['required'=>'required','class'=>'input',"id"=>"user-register-forgot-question"])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-register-forgot-answer" class="label">Jawaban Lupa Password</label>
							{{Form::text('jlupapassword','',['required'=>'required','class'=>'input', "id"=>"user-register-forgot-answer" ,'placeholder'=>'Jawaban lupa password'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-register-bank" class="label">Bank</label>
							{{Form::select('bankname', 
							  	$bankdata,null,['required'=>'required','class'=>'input'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-register-bank-account" class="label">No. Rekening</label>
							{{Form::text('norekbank','',['required'=>'required','class'=>'input', 'id'=>"user-register-bank-account",'placeholder'=>'No. rekening bank'])}}
						</div>
						<div class="form-text form-inline">
							<label for="user-register-bank-name" class="label">Nama Rekening</label>
							{{Form::text('pemilikbank','',['required'=>'required','class'=>'input','id'=>"user-register-bank-name",'placeholder'=>'Nama pemilik bank'])}}
						</div>
						<div class="form-checkbox form-inline">
							{{Form::checkbox('cbkonfirmasi', '1',false,['required'=>'required','id'=>"user-register-agreement",'class'=>'form-check-input'])}}
							<label for="user-register-agreement" class="label">Saya sudah berusia lebih dari 18 tahun dan menyetujui untuk mengikuti peraturan yang dipublikasikan ini.</label>
						</div>
						{{ csrf_field() }}
						<div class="form-submit form-inline">						
							{{Form::text('upline','',['hidden'=>'true','id'=>'upline'])}}						
							<button class="button button-full">Daftar Sekarang</button>						
						</div>					
					</div>
					{!!Form::close()!!}
				</div>				
			</div>
		</div>
	</div>
</div>
@endsection