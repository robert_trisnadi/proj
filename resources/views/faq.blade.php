@extends('layouts.app')
@section('contentdescription')
	<?php echo "FAQ page dari ".$_SERVER["HTTP_HOST"].". Punya pertanyaan mengenai ".$_SERVER["HTTP_HOST"]."? Silahkan melihat halaman FAQ terlebih dahulu sebelum mengontak.";?>
@endsection
@section('content')

<?php
try {

	$faq = DB::table("page")->first()->FaqPage;	
} catch (Exception $e) {
	echo '<script>console.log("'.$e.'")</script>';
	$faq = null;
}	
?>
<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Bantuan</h1>
				</header>

				<div class="page-single-content">
					<div class="help-single">
						@include('include.sidebar')
						<div class="help-single-body">
							<h2 class="title">FAQ</h2>
							<div class="faq-posts">
								<?php echo $faq;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection