<div class="site-header-brand">
	<div class="wrapper">

		<a href="#mobile-nav" class="mobile-nav-toggle js-toggle" data-toggle-animation="manual" data-toggle-area="#mobile-nav"><svg class="icon" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-hamburger')}} /></svg></a>

		<div class="logo">
			<h1>
			<a href="<?php 
			if(Request::session()->get('id','default') == 'default')
				echo '/';
			else 
				echo '/profil';
			?>" class="logo" rel="nofollow"><img src={{URL::asset('assets/images/t4sport-logo.png')}} alt="Logo T4SPORT.info" title="Logo T4SPORT.info"></a>
			</h1>
		</div>

		<nav class="nav-site only-desktop" role="navigation">
			<ul class="items">
				<li class="item {{ Request::is('/') ? 'item-current' : '' }}">
					<a href="{{ (Request::session()->get('id','default') == 'default') ? '/' : '/profil' }}" class="link">Home</a>
				</li>
				<li class="item {{ Request::is('promo') ? 'item-current' : '' }}"><a href="/promo" class="link">Promo</a></li>
				<li class="item {{ Request::is('jadwal') ? 'item-current' : '' }}"><a href="/jadwal" class="link">Jadwal</a></li>
				<li class="item {{ Request::is('livescore') ? 'item-current' : '' }}"><a href="/livescore" class="link">Live Score</a></li>
				<li class="item {{ Route::currentRouteName() == 'berita'? 'item-current' : '' }}"><a href="/berita/10/-/-" class="link">Berita</a></li>
			</ul>
		</nav>
	</div>
</div>