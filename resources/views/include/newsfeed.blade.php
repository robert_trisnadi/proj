<aside id="main-aside" class="main-aside" role="complementary">
	<div class="widget-single" id="news-category-container">
		<div class="widget-single-head">
			<h4 class="title">Kategori</h4>
		</div>
		<div class="widget-single-body">
			<div class="news-category">
				<ul class="items">
					<li class="item"><a 
					<?php 
						if(isset($categoryid) && isset($subcategoryid))
							if('-' == $categoryid && '-'==$subcategoryid)
								echo 'style="color:#288BF8;font-weight:bold"';
					?>
					class="link" href="{{route('berita', ['imax' => 10,'categoryid'=>'-','subcategoryid'=>'-'])}}">Semua Berita</a></li>
					<?php
					foreach ($articles as $a) {
						# code...
						if($a->Status == 4)
						?>
						<li class="item"><a 
							<?php 
								if(isset($categoryid))
									if($a->CategoryName == $categoryid)
										echo 'style="color:#288BF8;font-weight:bold"';
							?>
						class="link" href="{{route('berita', ['imax' => 10,'categoryid'=>str_replace(" ","-",$a->CategoryName),'subcategoryid'=>'-'])}}">{{$a->CategoryName}}</a>
						<ul class="items items-children" >
						<?php 
							foreach ($subc as $sub) 
							{
								if($sub->CategoryID == $a->CategoryID && $sub->Status == 4)
								{		
						?>
									<li class="item"
									<?php 
										if(isset($subcategoryid))
											if(isset($sub->SubcategoryName))
												if($sub->SubcategoryName == $subcategoryid)
													echo 'style="color:#288BF8;font-weight:bold"';
									?> 
									><a class="link" href="{{route('berita', ['imax' => 10,'categoryid'=>str_replace(" ","-",$a->CategoryName),'subcategoryid'=>str_replace(" ","-",$sub->SubcategoryName)])}}"><?php echo $sub->SubcategoryName;?></a></li>
						<?php
								}
							}
						?>
						</ul>
						</li>
					<?php
					}
					?>
				</ul>
			</div>
		</div>
	</div>

<?php
	$news_populer = DB::table('ArticleCategorySubView')
	->where('Status',4)
	->where(function($q)
	{
		$q->where('StatusCategory',4)
		  ->orWhere('StatusCategory',NULL);
	})
	->where(function($q)
	{
		$q->where('StatusSub',4)
		  ->orWhere('StatusSub',NULL);
	})
	->orderBy('ViewCount', 'desc')->take(5)->get();
?>
	<div class="widget-single" id="news-berita-populer-container">
		<div class="widget-single-head">
			<h4 class="title">Berita Populer</h4>
		</div>
		<div class="widget-single-body">
			<div class="news-minis">			
			@foreach ($news_populer as $a)
				<div class="news-mini">
					<a href="{{route('detailberita', ['idd'=>str_replace(" ","-",$a->Title)])}}" class="news-mini-link">
						<div class="news-mini-thumbnail">
							<img  title="{{$a->TitleImage}}"  alt="{{$a->AlternateImage}}" src="http://{{ $_SERVER['HTTP_HOST'].'/uploads/' . "{$a->Image}"}}"/>							
						</div>
						<div class="news-mini-detail">
							<p class="categories"><span class="category">{{$a->CategoryName}}</span> / <span class="category">{{$a->SubcategoryName}}</span></p>
							<h4 class="title">{{$a->Title}}</h4>
						</div>
					</a>
				</div>
			@endforeach
			</div>
		</div>
	</div>
</aside>