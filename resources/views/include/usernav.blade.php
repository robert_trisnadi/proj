<?php 
	$data2 = DB::table('cashbackbonusview')
		->where('CustomerID',Request::session()->get('id')->CustomerID)
		->where('Status' , 1)
		->get();

	$totalcb = 0;
	foreach ($data2 as $var) {
		if($var->status == 1)
		{		
			$totalcb+=floatval($var->BonusCashback);
		}
	}
	Request::session()->put('bonuscashback', round($totalcb,2));

	$data = DB::table('ReferralBonusView')
		->where('UplineID',Request::session()->get('id')->CustomerID)
		->where('Status' , 1)
		->get();	
	$total = 0;
	foreach ($data as $var) {
		if($var->Status == 1)
		{
	
			$total+=floatval($var->ReferralBonus);
		}
	}
	Request::session()->put('bonusreferral', round($total,2));

	$totalfd = 0;
	$data3 = DB::table('CustomerProduct')
		->where('CustomerID', Request::session()->get('id')->CustomerID)
		->where('firstdepositbonus.Status',1)
		->join('firstdepositbonus','firstdepositbonus.customerproductid','=','customerproduct.CustomerProductID')
		->select(DB::raw('firstdepositbonus.TimeInput as Tanggal'),DB::raw("'bonus first deposit' as Keterangan" ),DB::raw('(VarFirstDepo * Transaction / 100) as Jumlah'),'firstdepositbonus.Status')
		->get();
	foreach ($data3 as $var) {
		if($var->Status == 1)
		{
			$totalfd+=floatval($var->Jumlah);
		}
	}
	Request::session()->put('bonusfirstdeposit', round($totalfd,2));
?>

<div class="user-card">
	<p class="name"><?php echo $user->Username;?></p>
	<p class="bonus bonus-referral" title="Bonus Referral"><a class="link" href="/referalbonus"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-referral" />
	</svg> Rp {{number_format($total,0,',','.')}}</a></p>
	<p class="bonus bonus-cashback" title="Bonus Cashback"><a class="link" href="/cashbackbonus"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-cashback" />
	</svg> Rp {{number_format($totalcb,0,',','.')}}</a></p>
</div>
<nav class="user-nav">
	<ul class="items">
		<li class="item {{ Request::is('profil') ? 'item-current' : '' }}"><a href="profil" class="link"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-profile" /></svg> Profile</a></li>
		<li class="item {{ Request::is('daftargame') ? 'item-current' : '' }}"><a href="/daftargame" class="link"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-game" /></svg> Daftar Game</a></li>
		<li class="item {{ Request::is('deposit') ? 'item-current' : '' }}"><a href="/deposit" class="link"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-deposit" /></svg> Deposit</a></li>
		<li class="item {{ Request::is('withdraw') ? 'item-current' : '' }}"><a href="/withdraw" class="link"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-withdraw" /></svg> Withdraw</a></li>
		<li class="item {{ Request::is('transfer') ? 'item-current' : '' }}"><a href="/transfer" class="link"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-transfer" /></svg> Transfer</a></li>		
		<li class="item {{ Request::is('referalbonus') ? 'item-current' : Request::is('firstdeposit') ? 'item-current' : Request::is('cashbackbonus') ? 'item-current' : ''}}">
			<a href="#bonus-children" class="link toggle js-toggle" data-toggle-area="body"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-bonus" /></svg> Bonus</a>
			<ul id="bonus-children" class="items items-children">
				<li class="item {{ Request::is('referalbonus') ? 'item-current' : '' }}"><a href="/referalbonus" class="link">Referral Bonus</a></li>
				<li class="item {{ Request::is('firstdeposit') ? 'item-current' : '' }}"><a href="/firstdeposit" class="link">First Deposit</a></li>
				<li class="item {{ Request::is('cashbackbonus') ? 'item-current' : '' }}"><a href="/cashbackbonus" class="link">Cashback Bonus</a>
				</li>
			</ul>
		</li>
		<li class="item {{ Request::is('logout') ? 'item-current' : '' }}"><a href="/logout" class="link"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-logout" /></svg> Logout</a></li>
	</ul>
</nav>