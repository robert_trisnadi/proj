<div class="container">
	<span id="komplainbox" style="
		position: fixed;
		margin-top: 100px;
		margin-right: 60px;
		right:0%;
		width: auto;
		height: auto;
		z-index: 2;
		cursor: pointer;
	">
		<div class="container" id="komplainicon" style="
			font-family: inherit;
			position: absolute;			
			width: 60px;
			padding:5px;		
			background-color: #fff;
			border: 1px solid #333;
			border-radius:10px;
			text-align: middle;
			display: inline-block;
			font-size:3em;
			color:black;
		">
			<i class="fa fa-bullhorn"></i>		
		</div>
		<div style="
			position: absolute;
			left: 60px;
			width: 400px;
			height: 500px;
			background-color: white;
			border: 1px solid #333;
			border-radius:10px;
		">

		<div class="container">
			<h1>Komplain</h1>
			<?php $errkomplain = Request::session()->get('errkomplain','default');?>
			@if($errkomplain != 'default')	
				<div class="alert alert-danger">
					<?php echo '<script>

					$("#komplainbox").css("right","400px");
					</script>';?>
					{{$errkomplain}}<?php echo '</br>';?>				
				</div>
			@endif
			{!! Form::open(['url'=> '/komplain','files'=> true]) !!}
			<div class="form-row">
				{{Form::label('handphone','Handphone')}}
				{{Form::text('handphone','',['class'=>'form-control', 'placeholder'=>'Handphone'])}}
			</div>
			<div class="form-row">				
				{{Form::label('emailkomplain','Email')}}
				{{Form::email('emailkomplain','',['class'=>'form-control', 'placeholder'=>'Email'])}}
			</div>
			<div class="form-row">
				{{Form::label('komplain','Komplain')}}
				{{Form::textarea('komplain','',['class'=>'form-control', 'placeholder'=>'Komplain','rows' => 2,'margin-bottom'=>'10'])}}
			</div>
			<div class="form-row">				
				{{Form::label('uploadfile','Upload')}}
				{{Form::file('uploadfile',['class'=>'form-control'])}}
			</div>
			<div class="form-row" style="margin-top:10px;">
				{{Form::submit('Kirim')}}
			</div>
			{!! Form::close()!!}
		</div>


		</div>
	</span>

</div>