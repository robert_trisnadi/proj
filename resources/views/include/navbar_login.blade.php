<?php $value = Request::session()->get('id', 'default');
?>

<nav id="mobile-nav" class="mobile-nav" role="navigation">
  <div class="mobile-nav-bloc">
    <nav class="nav-user" role="navigation">
        <ul class="items">
          <li class="item item-current"><a href="/profile" class="link link-member"><svg class="icon icon-inline" role="img"><use xlink:href="assets/images/svg-symbols.svg#icon-profile" /></svg> Member Area</a></li>
        </ul>
      </nav>
  </div>

  <div class="mobile-nav-bloc">
    <ul class="items">
      <li class="item item-current"><a href="{{ (Request::session()->get('id','default') == 'default') ? '/' : '/profil' }}" class="link">Home</a></li>
      <li class="item {{ Request::is('promo') ? 'item-current' : '' }}"><a href="/promo" class="link">Promo</a></li>
      <li class="item {{ Request::is('berita') ? 'item-current' : '' }}"><a href="/berita/10/-/-" class="link">Berita</a></li>
    </ul>
  </div>

  <div class="mobile-nav-bloc">
    <p class="legend">Bantuan</p>
    <ul class="items">
      <li class="item {{ Request::is('panduan') ? 'item-current' : '' }}"><a href="/panduan" class="link">Panduan</a></li>
      <li class="item {{ Request::is('panduankasino') ? 'item-current' : '' }}"><a href="/panduankasino" class="link">Panduan kasino</a></li>
    <li class="item {{ Request::is('panduanbola') ? 'item-current' : '' }}"><a href="/panduanbola" class="link">Panduan Bola</a></li>
      <li class="item {{ Request::is('faq') ? 'item-current' : '' }}"><a href="/faq" class="link">FAQ</a></li>
      <li class="item {{ Request::is('peraturan') ? 'item-current' : '' }}"><a href="/peraturan" class="link">Peraturan</a></li>
      <li class="item {{ Request::is('contact') ? 'item-current' : '' }}"><a href="/contact" class="link">Hubungi Kami</a></li>
    </ul>
  </div>
  <div class="mobile-nav-bloc">
    <p class="legend">Member Area</p>
    <ul class="items">
      <li class="item {{ Request::is('profil') ? 'item-current' : '' }}"><a href="/profil" class="link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-profile')}} /></svg> Profile</a></li>
      <li class="item {{ Request::is('daftargame') ? 'item-current' : '' }}"><a href="/daftargame" class="link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-game')}} /></svg> Daftar Game</a></li>
      <li class="item {{ Request::is('deposit') ? 'item-current' : '' }}"><a href="/deposit" class="link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-deposit')}} /></svg> Deposit</a></li>
      <li class="item {{ Request::is('withdraw') ? 'item-current' : '' }}"><a href="/withdraw" class="link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-withdraw')}} /></svg> Withdraw</a></li>
      <li class="item {{ Request::is('transfer') ? 'item-current' : '' }}"><a href="/transfer" class="link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-transfer')}} /></svg> Transfer</a></li>      
      <li class="item {{ Request::is('referalbonus') ? 'item-current' : Request::is('firstdeposit') ? 'item-current' : Request::is('cashbackbonus') ? 'item-current' : ''}}">
        <a href="#bonus-children-mobile" class="link toggle js-toggle" data-toggle-area="body"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-bonus')}} /></svg> Bonus</a>
        <ul id="bonus-children-mobile" class="items items-children">
          <li class="item {{ Request::is('referalbonus') ? 'item-current' : '' }}"><a href="/referalbonus" class="link">Referral Bonus</a></li>
          <li class="item {{ Request::is('firstdeposit') ? 'item-current' : '' }}"><a href="/firstdeposit" class="link">First Deposit</a></li>
          <li class="item {{ Request::is('cashbackbonus') ? 'item-current' : '' }}"><a href="/cashbackbonus" class="link">Cashback Bonus</a></li>
        </ul>
      </li>
      <li class="item"><a href="/logout" class="link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-logout')}} /></svg> Logout</a></li>
    </ul>
</div>
</nav>
<!-- baru ditambahin nih, cek -->

<header id="site-header" class="site-header" role="banner">
  <div class="inner">

    <div class="site-header-top">
      <div class="wrapper">
        <nav class="nav-help" role="navigation">
          <ul class="items">
            <li class="item {{ Request::is('panduan') ? 'item-current' : '' }}"><a href="/panduan" class="link">Panduan</a></li>
            <li class="item {{ Request::is('panduankasino') ? 'item-current' : '' }}"><a href="/panduankasino" class="link">Panduan kasino</a></li>
            <li class="item {{ Request::is('panduanbola') ? 'item-current' : '' }}"><a href="/panduanbola" class="link">Panduan Bola</a></li>
            <li class="item {{ Request::is('faq') ? 'item-current' : '' }}"><a href="/faq" class="link">FAQ</a></li>
            <li class="item {{ Request::is('peraturan') ? 'item-current' : '' }}"><a href="/peraturan" class="link">Peraturan</a></li>
            <li class="item {{ Request::is('contact') ? 'item-current' : '' }}"><a href="/contact" class="link">Hubungi Kami</a></li>
          </ul>
        </nav>

        <nav class="nav-user" role="navigation">
          <ul class="items">
            <li class="item item-current"><a href="/profil" class="link link-member"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-profile')}} /></svg> <?php echo 'Welcome, <b>'.$value->Username.'</b> &nbsp;! &nbsp;&nbsp;';?></a>

            </li>
            <li class="item"><a href="/logout" class="link link-logout"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-logout')}} /></svg> Logout</a></li>
          </ul>
        </nav>

        <div id="popup-login" class="popup-login popup-inline mfp-hide">
          <div class="popup-inline-header">
            <h4 class="heading"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-login')}} /></svg> Login</h4>
          </div>
          <div class="popup-inline-content">
            <div class="popup-login-form">
              <div class="text form-text form-inline">
                <label for="user-register-username" class="label"><strong>Username</strong></label>
                <input type="text" id="user-register-username" class="input input-large">
              </div>
              <div class="text form-text form-inline">
                <label for="user-register-password" class="label"><strong>Password</strong></label>
                <input type="password" id="user-register-password" class="input input-large">
              </div>
              <div class="submit form-submit form-inline">
                <button class="button button-full">Masuk</button>
              </div>
              <p class="popup-login-forgot"><a href="/forgot">Lupa password?</a></p>
            </div>
          </div>
          <div class="popup-inline-footer">
            <p>Belum punya id? <a href="/registration"><strong>Daftar sekarang!</strong></a></p>
          </div>
        </div>
      </div>
    </div>

   @include('include.topsidebar')
  </div>
</header>