<div class="help-single-nav">
	<ul class="items">
		<li class="item {{ Request::is('panduan') ? 'item-current' : '' }}"><a href="/panduan" class="link">Panduan</a></li>
		<li class="item {{ Request::is('panduankasino') ? 'item-current' : '' }}"><a href="/panduankasino" class="link">Panduan Kasino</a></li>
		<li class="item {{ Request::is('panduanbola') ? 'item-current' : '' }}"><a href="/panduanbola" class="link">Panduan Bola</a></li>
		<li class="item {{ Request::is('faq') ? 'item-current' : '' }}"><a href="/faq" class="link">FAQ</a></li>
		<li class="item {{ Request::is('peraturan') ? 'item-current' : '' }}"><a href="/peraturan" class="link">Peraturan</a></li>
		<li class="item {{ Request::is('contact') ? 'item-current' : '' }}"><a href="/contact" class="link">Hubungi Kami</a></li>
	</ul>
</div>