<nav id="mobile-nav" class="mobile-nav" role="navigation">
  <div class="mobile-nav-bloc">
    <ul class="items">
      <li class="item"><a href="#popup-login" class="link js-popup-link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-login')}} /></svg> Login</a></li>
      <li class="item"><a href="/registration" class="link link-register"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-register')}} /></svg> Daftar</a></li>
    </ul>
  </div>

  <div class="mobile-nav-bloc">
    <ul class="items">
      <li class="item {{ Request::is('/') ? 'item-current' : '' }}"><a href="{{ (Request::session()->get('id','default') == 'default') ? '/' : '/profil' }}" class="link">Home</a></li>
      <li class="item {{ Request::is('promo') ? 'item-current' : '' }}"><a href="/promo" class="link">Promo</a></li>
      <li class="item {{ Route::currentRouteName() == 'berita'? 'item-current' : '' }}"><a href="/berita/10/-/-" class="link">
      Berita</a></li>
    </ul>
  </div>

  <div class="mobile-nav-bloc">
    <p class="legend">Bantuan</p>
    <ul class="items">
      <li class="item {{ Request::is('panduan') ? 'item-current' : '' }}"><a href="/panduan" class="link">Panduan</a></li>
      <li class="item {{ Request::is('panduankasino') ? 'item-current' : '' }}"><a href="/panduankasino" class="link">Panduan kasino</a></li>
    <li class="item {{ Request::is('panduanbola') ? 'item-current' : '' }}"><a href="/panduanbola" class="link">Panduan Bola</a></li>
      <li class="item {{ Request::is('faq') ? 'item-current' : '' }}"><a href="/faq" class="link">FAQ</a></li>
      <li class="item {{ Request::is('peraturan') ? 'item-current' : '' }}"><a href="/peraturan" class="link">Peraturan</a></li>
      <li class="item {{ Request::is('contact') ? 'item-current' : '' }}"><a href="/contact" class="link">Hubungi Kami</a></li>
    </ul>
  </div>
</nav>

<header id="site-header" class="site-header" role="banner">
  <div class="inner">

    <div class="site-header-top">
      <div class="wrapper">
        <nav class="nav-help" role="navigation">
          <ul class="items">
            <li class="item {{ Request::is('panduan') ? 'item-current' : '' }}"><a href="/panduan" class="link">Panduan</a></li>
            <li class="item {{ Request::is('panduankasino') ? 'item-current' : '' }}"><a href="/panduankasino" class="link">Panduan kasino</a></li>
    <li class="item {{ Request::is('panduanbola') ? 'item-current' : '' }}"><a href="/panduanbola" class="link">Panduan Bola</a></li>
            <li class="item {{ Request::is('faq') ? 'item-current' : '' }}"><a href="/faq" class="link">FAQ</a></li>
            <li class="item {{ Request::is('peraturan') ? 'item-current' : '' }}"><a href="/peraturan" class="link">Peraturan</a></li>
            <li class="item {{ Request::is('contact') ? 'item-current' : '' }}"><a href="/contact" class="link">Hubungi Kami</a></li>
          </ul>
        </nav>

        <nav class="nav-user" role="navigation">
          <ul class="items">
            <li class="item"><a href="#popup-login" class="link js-popup-link"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-login')}} /></svg> Login</a></li>
            <li class="item"><a href="/registration" class="link link-register"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-register')}} /></svg> Daftar</a></li>
          </ul>
        </nav>

        <div id="popup-login" class="popup-login popup-inline mfp-hide">
          <div class="popup-inline-header">
            <h4 class="heading"><svg class="icon icon-inline" role="img"><use xlink:href={{URL::asset('assets/images/svg-symbols.svg#icon-login')}} /></svg> Login</h4>
          </div>
          <div class="popup-inline-content">
            <div class="popup-login-form">
            {!! Form::open(['url'=> '/login1']) !!}
              <div class="text form-text form-inline">
                <label for="user-register-username" class="label"><strong>Username</strong></label>                
                {{Form::text('username','',['id'=>"user-register-username",'class'=>'input input-large', 'placeholder'=>'Enter Username'])}}
              </div>
              <div class="text form-text form-inline">
                <label for="user-register-password" class="label"><strong>Password</strong></label>                
                {{Form::password('password',['id'=>"user-register-password",'class'=>'input input-large', 'placeholder'=>'Enter Password'])}}
              </div>
              <div class="submit form-submit form-inline">                
                {{Form::submit('Masuk',['class'=>'button button-full'])}}
                {{csrf_field() }}
              </div>
              <p class="popup-login-forgot"><a href="/forgot">Lupa password?</a></p>
              {!! Form::close() !!}
            </div>
          </div>
          <div class="popup-inline-footer">
            <p>Belum punya id? <a href="/registration"><strong>Daftar sekarang!</strong></a></p>
          </div>
        </div>
      </div>
    </div>
    @include('include.topsidebar')
  </div>
</header>