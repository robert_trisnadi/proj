<?php
    try {
        $ses = Request::session()->get('id','default');
        $mem = DB::table("memo")->where('CustomerID',$ses->CustomerID)->first();
        if($mem != null)
        {


            if(!isset($ses->isMinimized))
                $ses->isMinimized = 0;
            if(!isset($ses->isClosed))
                $ses->isClosed = 0;
?>
    <div id="memo" style="<?php
        if($ses->isClosed == 1)
            echo 'opacity:0;';
        else if($ses->isMinimized == 1)
            echo 'height:35px;';
    ?>">
        <b>Memo</b>
        <span id="minimize-memo" onclick="memoOperation(0)">-</span>
        <span id="close-memo" onclick="memoOperation(1)">x</span><br>
        <?php echo $mem->Content; ?>
    </div>
<?php
    }
    } catch (Exception $e) {
        echo '<script>console.log("'.$e.'")</script>';
    }    
?>