@extends('layouts.app')
@section('contentdescription')
	<?php echo "Profil diri anda di ".$_SERVER["HTTP_HOST"].". Website agen betting terpercaya.";?>
@endsection
<?php 
if(!Session::has('id'))
{
   Redirect::to('/')->send();
}

$user = Request::session()->get('id','default');
?>

@section('content')

<div id="site-content" class="site-content">
	<div class="inner wrapper">
		<div class="main-content" role="main" itemscope itemprop="mainContentOfPage">		
			<div class="page-single-container">
				<header class="page-single-header">
					<h1 class="heading">Member Area</h1>
				</header>

				<div class="page-single-content">
					<div class="user-dashboard">

						<div class="user-dashboard-aside">
							@include('include.usernav')
						</div>

						<div class="user-dashboard-body">
							@include('include.marq')
							<h2 class="user-dashboard-title">Profile</h2>							
							<?php 
								$msg = Request::session()->pull('message', 'default');
								if($msg !='default')
									echo '<div class="success">'.$msg.'</div>';
								else
								{
									$msg = Request::session()->pull('err', 'default');
									if($msg !='default')
										echo '<div class="alert">'.$msg.'</div>';
								}
							?>	
							@if(count($errors)>0)	
								<div class="alert">
								@foreach($errors->all() as $error)		
									{{$error}}<?php echo '</br>';?>
								@endforeach
								</div>
							@endif
							{!! Form::open(['url'=> '/profil/gantipassword']) !!}
							<div class="user-profile">
								<div class="user-profile-basic">
									<div class="form-text form-inline">
										<label for="user-username" class="label">Username</label>
										{{Form::text('username',$user->Username,['id'=>"user-username",'disabled'=>'true','class'=>'input', 'placeholder'=>'Username'])}}
									</div>
									<div class="form-text form-inline">
										<label for="user-email" class="label">Email</label>
										{{Form::text('email',$user->Email,['id'=>"user-email",'disabled'=>'true','class'=>'input', 'placeholder'=>'Email'])}}
									</div>
									<div class="form-text form-inline">
										<label for="user-bank" class="label">Bank</label>
										{{Form::text('bankname',$user->BankName,['id'=>"user-bank",'disabled'=>'true','class'=>'input', 'placeholder'=>'BankName'])}}
									</div>
									<div class="form-text form-inline">
										<label for="user-bank-account" class="label">No. Rekening</label>
										{{Form::text('norekbank',$user->BankAccountNumber,['id'=>"user-bank-account",'disabled'=>'true','class'=>'input', 'placeholder'=>'No. rekening bank'])}}
									</div>
									<div class="form-text form-inline">
										<label for="user-bank-name" class="label">Nama Rekening</label>
										{{Form::text('pemilikbank',$user->BankAccountName,['id'=>"user-bank-name",'disabled'=>'true','class'=>'input', 'placeholder'=>'Nama pemilik bank'])}}
									</div>
								</div>

								<div class="user-profile-password">								
									<h3 class="title">Ubah Password</h3>
									<div class="form-text form-inline">
										<label for="user-password-old" class="label">Password Lama</label>
										{{Form::password('password',['id'=>"user-password-old",'class'=>'input', 'placeholder'=>'Password lama'])}}
									</div>
									<div class="form-text form-inline">
										<label for="user-password-new" class="label">Password Baru</label>
										{{Form::password('passwordbaru',['id'=>"user-password-new",'class'=>'input', 'placeholder'=>'Password Baru'])}}
									</div>
									<div class="form-text form-inline">
										<label for="user-password-new-2" class="label">Konfirmasi Password Baru</label>
										{{Form::password('kpassword',['id'=>"user-password-new-2",'class'=>'input', 'placeholder'=>'Konfirmasi password'])}}
									</div>
								</div>

								<div class="user-profile-save">
									<div class="form-submit form-inline">
										{{Form::submit('Kirim',['class'=>'button button-full'])}}
										{{Form::text('membertime','',['hidden'=>'true','id'=>'membertime'])}}
									</div>
								</div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection